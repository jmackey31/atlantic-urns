let todays = new Date();

let User          = require('../../models/user'),
    SiteMaster    = require('../../models/sitemaster'),
    LoginReport   = require('../../models/login-reports');

// ------------------------------------------------
module.exports.deleteUserAccount = function(req, res) {
    // console.log(req.body);
    // console.log('req.body deleteuseraccount');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                let userToDelete = String(req.body.id_to_delete_on);
                console.log(user);
                console.log('User that wants to delete this user '+ userToDelete);

                // return res.json({
                //     success: true,
                //     message: 'User that wants to delete this user '+ userToDelete
                // });

                User.remove({ '_id': userToDelete })
                    .exec(function(err, deleteuser) {
                        if (err) {
                            return res.json({
                                success: false,
                                message: 'User could not removed!'
                            });
                        } else {
                            return res.json({
                                success: true,
                                message: 'User has been successfully deleted!'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.updateUserAccountStatus = function(req, res) {
    // console.log(req.body);
    // console.log('req.body updateuseraccountstatus');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to update this user '+ req.body.id_to_update_on);

                let userIdForUpdate = String(req.body.id_to_update_on);
                // console.log(userIdForUpdate);
                // console.log('userIdForUpdate');

                User.findById({ '_id': userIdForUpdate })
                    .exec(function(err, founduser) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not update this user\'s status.  Something went wrong.'
                            });
                        } else if (founduser) {
                            // console.log(founduser);
                            // console.log('Found user listed above');

                            if (req.body.account_status) founduser.account_status = req.body.account_status;

                            founduser.updated = todays;
                            // console.log('Updated user to save.');
                            // console.log('----------------------------------');
                            // console.log(founduser);

                            // return res.json({
                            //     success: true,
                            //     message: 'Returning all updated user account.',
                            //     founduser: founduser
                            // });

                            // save the user
                            founduser.save(function(err) {
                                if (err) {
                                    console.log('save not complete:');
                                    console.log(err);
                                    return res.json({
                                        success: false,
                                        message: 'Could not update this user\'s status.  Something went wrong.'
                                    });
                                } else {
                                    console.log('User account status updated!');
                                    return res.json({
                                        success: true,
                                        message: 'User account status updated!'
                                    });
                                }
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not update this user\'s status.  Something went wrong.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.updateUserAccount = function(req, res) {
    console.log(req.body);
    console.log('req.body updateUserAccount');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    let userIdForUpdate = String(req.params.user_id);
    // console.log(userIdForUpdate);
    // console.log('userIdForUpdate');

    User.findById({ '_id': userIdForUpdate })
        .exec(function(err, founduser) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not update this user\'s status.  Something went wrong.'
                });
            } else if (founduser) {
                // console.log(founduser);
                // console.log('Found user listed above');

                if (req.body.usersObj.account_type) founduser.account_type = req.body.usersObj.account_type;
                if (req.body.usersObj.fac_name) founduser.fac_name = req.body.usersObj.fac_name;
                if (req.body.usersObj.contact_person) founduser.contact_person = req.body.usersObj.contact_person;
                if (req.body.usersObj.user_code) founduser.user_code = req.body.usersObj.user_code;
                if (req.body.usersObj.access_level) founduser.access_level = req.body.usersObj.access_level;
                if (req.body.usersObj.username) founduser.username = req.body.usersObj.username;
                if (req.body.usersObj.password) founduser.password = req.body.usersObj.password;
                if (req.body.usersObj.email) founduser.email = req.body.usersObj.email;
                if (req.body.usersObj.phone) founduser.phone = req.body.usersObj.phone;
                if (req.body.usersObj.fax) founduser.fax = req.body.usersObj.fax;
                if (req.body.usersObj.firstname) founduser.firstname = req.body.usersObj.firstname;
                if (req.body.usersObj.lastname) founduser.lastname = req.body.usersObj.lastname;
                if (req.body.usersObj.address_1) founduser.address_1 = req.body.usersObj.address_1;
                if (req.body.usersObj.address_2) founduser.address_2 = req.body.usersObj.address_2;
                if (req.body.usersObj.city) founduser.city = req.body.usersObj.city;
                if (req.body.usersObj.state) founduser.state = req.body.usersObj.state;
                if (req.body.usersObj.zip) founduser.zip = req.body.usersObj.zip;
                if (req.body.usersObj.web) founduser.web = req.body.usersObj.web;
                if (req.body.usersObj.price_rounding) founduser.price_rounding = req.body.usersObj.price_rounding;
                if (req.body.usersObj.account_status) founduser.account_status = req.body.usersObj.account_status;


                founduser.updated = todays;
                console.log(founduser);
                console.log('Updated user to save.');
                console.log('----------------------------------');

                // return res.json({
                //     success: true,
                //     message: 'Returning all updated user account.',
                //     founduser: founduser
                // });

                // save the user
                founduser.save(function(err) {
                    if (err) {
                        console.log('save not complete:');
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Could not update this user\'s status.  Something went wrong.'
                        });
                    } else {
                        console.log('User account status updated!');
                        return res.json({
                            success: true,
                            message: 'User account status updated!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not update this user\'s status.  Something went wrong.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.changeMyPassword = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.params.user_id
    };

    User.findById(query)
        .exec(function(err, user) {
            if (err) {
                console.log('Could not find user in database.');
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not find user in database.'
                });
            }
            // current timestamp
            let todays = new Date();
            if (req.body.newPassword) user.password = req.body.newPassword;

            user.updated = todays;
            // console.log('Updated user to save.');
            // console.log('----------------------------------');
            // console.log(user);

            // save the user
            user.save(function(err) {
                if (err) {
                    console.log('save not complete:');
                    console.log(err);
                    return res.json({
                        success: false,
                        error: err,
                        message: 'User could not be saved.  Something went wrong.'
                    });
                } else {
                    console.log('User updated!');
                    return res.json({
                        success: true,
                        message: 'User password updated!',
                        userObj: user
                    });
                }
        });
    });
};

// ------------------------------------------------
module.exports.getUserPricePoint = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        'user_code': req.params.user_code
    };
    let projection = {
        '_id': 0
        // 'formattedDate': 0,
        // 'updated': 0,
        // '__v': 0,
        // 'created': 0
    };

    User.findOne(query, projection)
        .select('account_type ' +
                'fac_name ' +
                'contact_person ' +
                'price_rounding '
        )
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User obj above:');

                // Get the site master object
                let querySiteMaster = {};
                let projectionSiteMaster = {'_id': 0};
                SiteMaster.findOne(querySiteMaster, projectionSiteMaster)
                    .select('master_product_price_markup')
                    .exec(function(err, sitemaster) {
                        if (err) {
                            console.log(err);
                            console.log('Ther was an error getting the site master object but continue anyway.');
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userPriceObj: user
                            });
                        } else if (sitemaster) {
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userPriceObj: user,
                                siteMasterObj: sitemaster
                            });
                        } else {
                            console.log('Ther was an error getting the site master object but continue anyway.');
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userPriceObj: user
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getUserInfo = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.body.user_id
    };
    let projection = {
        'password': 1
    };

    User.findById(query, projection)
        .select('username ' +
                'password ' +
                'firstname ' +
                'lastname ' +
                'account_type ' +
                'fac_name ' +
                'contact_person ' +
                'user_code ' +
                'parent_account.user_code ' +
                'parent_account.contact_person ' +
                'email ' +
                'phone ' +
                'fax ' +
                'address_1 ' +
                'address_2 ' +
                'city ' +
                'state ' +
                'zip ' +
                'web ' +
                'price_rounding ' +
                'account_status ' +
                'opt_out_base_price_increases ' +
                'access_level'
        )
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view all users.');

                // Get the site master object
                let querySiteMaster = {};
                let projectionSiteMaster = {'_id': 0};
                SiteMaster.findOne(querySiteMaster, projectionSiteMaster)
                    .select('master_product_price_markup')
                    .exec(function(err, sitemaster) {
                        if (err) {
                            console.log(err);
                            console.log('Ther was an error getting the site master object but continue anyway.');
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userObj: user
                            });
                        } else if (sitemaster) {
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userObj: user,
                                siteMasterObj: sitemaster
                            });
                        } else {
                            console.log('Ther was an error getting the site master object but continue anyway.');
                            return res.json({
                                success: true,
                                message: 'All good!',
                                userObj: user
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getAllUsers = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.body.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view all users.');
                // return res.json({
                //     success: true,
                //     message: 'All good!'
                // });
                User.find()
                    .sort({ username: 'asc', test: -1 })
                    .select('username ' +
                            'password ' +
                            'access_level')
                    .exec(function(err, users) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any users.  Something went wrong.'
                            });
                        } else if (users) {
                            // console.log(users);
                            // console.log('Users list above');
                            return res.json({
                                success: true,
                                message: 'All good!',
                                allusers: users
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find any users.  Something went wrong.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.singleLoginHistoryReports = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view single report.');

                let userToFindById = String(req.body.id_to_report);
                // console.log(userToFindById);
                // console.log('userToFindById');
                LoginReport.findOne({ 'user_id': userToFindById })
                    .exec(function(err, report) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any login reports.  Something went wrong.'
                            });
                        } else if (report) {
                            // console.log(report);
                            // console.log('Report listed above');
                            return res.json({
                                success: true,
                                message: 'Returning single report.',
                                singlereport: report
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'User has not yet logged in.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getLoginHistoryReports = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view all users.');

                LoginReport.find()
                    .sort({ username: 'asc', test: -1 })
                    .exec(function(err, reports) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any reports.  Something went wrong.'
                            });
                        } else if (reports) {
                            // console.log(reports);
                            // console.log('Reports list above');
                            return res.json({
                                success: true,
                                message: 'Returning all reports.',
                                allreports: reports
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find any reports.  Something went wrong.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getUserAccounts = function(req, res) {
    // console.log(req.body);
    // console.log('req.body');

    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view all users.');

                User.find()
                    .sort({ username: 'asc', test: -1 })
                    .exec(function(err, alluseraccounts) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any user accounts.  Something went wrong.'
                            });
                        } else if (alluseraccounts) {
                            // console.log(alluseraccounts);
                            // console.log('All user accounts list above');
                            return res.json({
                                success: true,
                                message: 'Returning all user accounts.',
                                allUserAccounts: alluseraccounts
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find any user accounts.  Something went wrong.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};
// ------------------------------------------------

module.exports.searchUserAccounts = function(req, res) {
    console.log(req.body);
    console.log('req.body for searching products');

    let adminQuery = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(adminQuery)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants to view all users.');
                let query = {};
                let projection = {
                    '_id': 0,
                    'formattedDate': 0,
                    'updated': 0,
                    'created': 0
                };

                // Add items to the query if they exist.
                if (req.body.usersObj.access_level) {
                    let accessLevelNumber = Number(req.body.usersObj.access_level);
                    query.access_level = accessLevelNumber;
                }
                if (req.body.usersObj.account_status) {
                    let accountStatusNumber = Number(req.body.usersObj.account_status);
                    query.account_status = accountStatusNumber;
                }
                if (req.body.usersObj.username) {
                    let accountUsername = req.body.usersObj.username.toLowerCase();
                    query.username = { $regex: accountUsername };
                }
                console.log(query);
                console.log('query object: ');

                // return res.json({
                //     success: false,
                //     message: 'No worries.  We are, Testing testing!',
                //     query: query,
                //     projection: projection
                // });

                User.find(query, projection)
                    .sort({ username: 'asc', test: -1 })
                    .exec(function(err, alluseraccounts) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any user accounts.  Something went wrong.'
                            });
                        } else if (alluseraccounts.length > 0) {
                            // console.log(alluseraccounts);
                            // console.log('All user accounts list above');
                            return res.json({
                                success: true,
                                message: 'Returning all user accounts.',
                                allUserAccounts: alluseraccounts
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find any user accounts.  Something went wrong.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            }
        });
};
// ------------------------------------------------

module.exports.newUser = function(req, res) {
    // console.log(req.body);
    // console.log('req.body above:');
    let usernameCheck = req.body.usernameCheck;
    // set the user object to a general all purpose one
    let newUserObject, determined_access_level;
    if (req.body.account_type === 'funeral_home') {
        newUserObject = req.body.funeralHomeObj;
        determined_access_level = 2;
    } else if (req.body.account_type === 'salesrep') {
        newUserObject = req.body.salesRepObj;
        determined_access_level = 4;
    }
    //
    // return res.json({
    //     success: false,
    //     message: 'No worries.  We are, Testing testingâ€¦',
    //     userType: req.body.funeralHomeObj
    // });
    // ---------------------------------------------------
    // Check if email exists in user's collection
    User.findOne({ 'username': usernameCheck })
        .exec(function(err, existinguser) {
            // console.log('-------------------');
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Something went wrong!  Could not create this user.'
                });
            } else if (existinguser) {
                console.log('This username has already been reserved.  Please try another one.');
                return res.json({
                    success: false,
                    err: 'duplicate-email',
                    message: 'The username "'+existinguser.username+'" has already been reserved.  Please select another one.'
                });
            } else {
                // ---------------------------------------------------
                // create a new instance of the User model
                let user = new User();

                // set the users information (comes from the request)
                if (newUserObject.username) user.username                               = newUserObject.username.toLowerCase();
                if (newUserObject.contact_person) user.contact_person                   = newUserObject.contact_person;
                if (newUserObject.fac_name) user.fac_name                               = newUserObject.fac_name;
                if (req.body.account_type) user.account_type                            = req.body.account_type;
                if (newUserObject.password) user.password                               = newUserObject.password;
                if (newUserObject.parent_account) {
                    if (newUserObject.parent_account.user_code) user.parent_account.user_code = newUserObject.parent_account.user_code;
                    if (newUserObject.parent_account.contact_person) user.parent_account.contact_person = newUserObject.parent_account.contact_person;
                }
                if (newUserObject.email) user.email                                     = newUserObject.email;
                if (newUserObject.phone) user.phone                                     = newUserObject.phone;
                if (newUserObject.fax) user.fax                                         = newUserObject.fax;
                if (newUserObject.firstname) user.firstname                             = newUserObject.firstname;
                if (newUserObject.lastname) user.lastname                               = newUserObject.lastname;
                if (newUserObject.address_1) user.address_1                             = newUserObject.address_1;
                if (newUserObject.address_2) user.address_2                             = newUserObject.address_2;
                if (newUserObject.city) user.city                                       = newUserObject.city;
                if (newUserObject.state) user.state                                     = newUserObject.state;
                if (newUserObject.zip) user.zip                                         = newUserObject.zip;
                if (newUserObject.web) user.web                                         = newUserObject.web;
                if (newUserObject.price_rounding) user.price_rounding                   = newUserObject.price_rounding;
                if (req.body.todaysFormattedDate) user.formattedDate                    = req.body.todaysFormattedDate;
                user.access_level                                                       = determined_access_level;
                user.created = todays;

                // Programatically create a user code
                function createUserCode() {
                    let getTimeNow = Date.parse(new Date());
                    let randNum = Math.random().toString();
                    let slicedNum = randNum.slice(3,9);

                    let newUserCode = 'user_'+getTimeNow+slicedNum;
                    return newUserCode;
                }
                user.user_code = createUserCode();

                // console.log('New constructed user.');
                // console.log(user);

                // return res.json({
                //     success: false,
                //     message: 'No worries.  We are, Testing testingâ€¦',
                //     userType: req.body.account_type,
                //     userToSave: user
                // });

                // save the user and check for errors
                user.save(function(err) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Something went wrong while saving this account.'
                        });
                    } else {
                        // sendSignupEmailMessage(user.email, specialMessage);
                        return res.json({
                            success: true,
                            message: 'Account created successfully!'
                        });
                    }
                });
            }
        });
};

// ------------------------------------------------
module.exports.newPublicUser = function(req, res) {
    console.log(req.body);
    console.log('req.body for new public user ------------- above ------');

    let newUserObject = req.body.publicUser;
    let query = {
        '_id': req.body.user_id,
        'access_level': 2
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not create this user.  Something went wrong.'
                });
            } else if (user) {
                // ---------------------------------------------------
                // create a new instance of the User model
                let newpublicuser = new User();

                // Add the parent user's code and name to this record.
                if (user.user_code) newpublicuser.parent_account.user_code                       = user.user_code;
                if (user.contact_person) newpublicuser.parent_account.contact_person             = user.contact_person;

                if (newUserObject.username) newpublicuser.username                               = newUserObject.username.toLowerCase();
                if (newUserObject.contact_person) newpublicuser.contact_person                   = newUserObject.contact_person;
                if (req.body.account_type) newpublicuser.account_type                            = req.body.account_type;
                if (newUserObject.password) newpublicuser.password                               = newUserObject.password;
                if (newUserObject.email) newpublicuser.email                                     = newUserObject.email;
                if (req.body.todaysFormattedDate) newpublicuser.formattedDate                    = req.body.todaysFormattedDate;
                newpublicuser.access_level                                                       = 5;
                newpublicuser.created = todays;

                // Programatically create a user code
                function createUserCode() {
                    let getTimeNow = Date.parse(new Date());
                    let randNum = Math.random().toString();
                    let slicedNum = randNum.slice(3,9);

                    let newUserCode = 'user_'+getTimeNow+slicedNum;
                    return newUserCode;
                }
                newpublicuser.user_code = createUserCode();

                console.log('New constructed public user.');
                console.log(newpublicuser);

                // return res.json({
                //     success: false,
                //     message: 'No worries.  We are, Testing testingâ€¦',
                //     newpublicuser: newpublicuser
                // });

                // save the user and check for errors
                newpublicuser.save(function(err) {
                    if (err) {
                        console.log(err);
                        if (err.code === 11000) {
                            return res.json({
                                success: false,
                                message: 'This username has already been selected.  Please select another one.'
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Something went wrong while saving this account.'
                            });
                        }
                    } else {
                        // sendSignupEmailMessage(user.email, specialMessage);
                        return res.json({
                            success: true,
                            message: 'Account created successfully!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not create this user.  Something went wrong.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.updatePriceMarkup = function(req, res) {
    // console.log(req.body);
    // console.log('req.body updateuseraccountstatus');

    let query = {
        '_id': req.params.user_id,
        'access_level': 2
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not update this user\'s price markup.  Something went wrong.'
                });
            } else if (user) {
                if (req.body.price_rounding) user.price_rounding = parseInt(req.body.price_rounding);
                if (req.body.opt_out_base_price_increases) {
                    user.opt_out_base_price_increases = true;
                } else {
                    user.opt_out_base_price_increases = false;
                }
                user.updated = todays;
                // console.log('Updated user to save.');
                // console.log('----------------------------------');
                // console.log(user);
                //
                // return res.json({
                //     success: false,
                //     message: 'Testing this endpoint.',
                //     user: user
                // });

                // save the user
                user.save(function(err) {
                    if (err) {
                        console.log('Save not completed:');
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Could not update this user\'s price markup.  Something went wrong.'
                        });
                    } else {
                        // console.log('User account status updated!');
                        return res.json({
                            success: true,
                            message: 'Price markup updated!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not update this user\'s price markup.  Something went wrong.'
                });
            }
        });
};

// ------------------------------------------------

// module.exports.someFunctionName = function(req, res) {
//
// };

// ------------------------------------------------

// module.exports.someFunctionName = function(req, res) {
//
// };

// ------------------------------------------------

// module.exports.someFunctionName = function(req, res) {
//
// };

// ------------------------------------------------
