let serverValidation    = require('./server-validation'),
    User                = require('../models/user'),
    Media               = require('../models/media'),
    EMedia              = require('../models/emedia'),
    LoginReport         = require('../models/login-reports'),
    Category            = require('../models/category'),
    Product             = require('../models/product'),
    SiteMaster          = require('../models/sitemaster'),
    MailingList         = require('../models/mailinglist'),
    jwt                 = require('jsonwebtoken'),
    config              = require('../../config'),
    fs                  = require('fs'),
    superSecret         = config.secret;

let todays = new Date();

module.exports.useMiddlewareWithToken = function(req, res, next) {
    // console.log('Using the middleware from it\'s module!!!!!');

    // check header or url parameters or post parameters for token
    let token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, superSecret, function(err, decoded) {
            if (err) {
                return res.status(403).json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No token provided.'
        });
    }
};

module.exports.authenticateEndpoints = function(req, res) {
    // console.log('req.body authentication object: ');
    // console.log(req.body);

    // --------------------------------------------------------
    let userLoginQuery = {
        'username': req.body.username.toLowerCase(),
        'account_status': 777
    };
    User.findOne(userLoginQuery)
        .select('_id ' +
            'user_code ' +
            'access_level ' +
            'account_type ' +
            'username ' +
            'password ' +
            'email ' +
            'city ' +
            'state'
        )
        .exec(function(err, user) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not log user in.  Something went wrong.'
                });
            } else if (!user) {
                // no user with that username was found
                return res.json({
                    success: false,
                    message: 'Authentication failed. User not found or does not have access.'
                });
            } else {
                // console.log(user);
                // check if password matches
                if (user.password !== req.body.password) {
                    return res.json({
                        success: false,
                        message: 'Authentication failed.  Wrong password.'
                    });
                } else {
                    // if user is found and password is right
                    // create a token
                    let token = jwt.sign({
                        username: user.username
                    }, superSecret, {
                        expiresIn: '24h' // expires in 24 hours
                    });

                    // console.log('Authenticated user');
                    // console.log(user);

                    // final login repsonse function
                    function finalLoginResponse() {
                        // return the information including token as JSON
                        return res.json({
                            success: true,
                            token: token,
                            user_id: user._id,
                            user_code: user.user_code,
                            access_level: user.access_level,
                            username: user.username,
                            city: user.city,
                            state: user.state,
                            email: user.email
                        });
                    }

                    // ------------------------------
                    // Log the user to the login reports collection

                    // 1. Find out if the user has ever logged in before.
                    let checkLoginReportQuery = { 'user_id': user._id };
                    LoginReport.findOne(checkLoginReportQuery)
                        .exec(function(err, reportfound) {
                            if (err) {
                                console.log(err);
                                // Continue with the login even if this throws an error.
                                // Dont worry about creating a new record or finding it at this point.
                                finalLoginResponse();
                            } else if (reportfound) {
                                // console.log('-------------------------------');
                                // console.log(reportfound);
                                // console.log('reportfound above');
                                // console.log('-------------------------------');
                                // Update the existing login report record
                                let currentCount = parseInt(reportfound.number_of_logins);
                                let numberOfLoginsCount = currentCount + 1;
                                reportfound.number_of_logins                                      = numberOfLoginsCount;
                                if (req.body.formattedDate) reportfound.formattedLastLoginDate    = req.body.formattedDate;
                                if (todays) reportfound.last_login                                = todays;

                                // console.log('-------------------------------');
                                // console.log('New report to update: ');
                                // console.log(reportfound);

                                // return res.json({
                                //    success: true,
                                //    message: 'Always return a value with these'
                                // });

                                // save the media and check for errors
                                reportfound.save(function(err) {
                                    if (err) {
                                        console.log(err);
                                        // Dont worry about creating a new record or finding it at this point.
                                        finalLoginResponse();
                                    } else {
                                        // console.log('-----------______######');
                                        // console.log('Everything is all good.  Updated login report.');
                                        // return the finalLoginResponse()
                                        finalLoginResponse();
                                    }
                                });
                            } else {
                                // console.log('-------------------------------');
                                // console.log('Create a new login report.......');
                                // console.log('-------------------------------');
                                // No report was found so create a new one and store the values
                                // create a new login report record
                                let loginreport = new LoginReport();

                                // set the users information (comes from the request)
                                if (user._id) loginreport.user_id                               = user._id;
                                if (user.username) loginreport.username                         = user.username;
                                if (user.user_code) loginreport.user_code                       = user.user_code;
                                if (user.access_level) loginreport.access_level                 = user.access_level;
                                if (user.account_type) loginreport.account_type                 = user.account_type;
                                // Set the initial value to 1 for this newly created record.
                                loginreport.number_of_logins                                    = 1;
                                if (req.body.formattedDate) loginreport.formattedLastLoginDate  = req.body.formattedDate;
                                loginreport.last_login                                          = todays;

                                // console.log('New constructed loginreport.');
                                // console.log(loginreport);

                                // save the media and check for errors
                                loginreport.save(function(err) {
                                    if (err) {
                                        console.log(err);
                                        // Dont worry about creating a new record or finding it at this point.
                                        finalLoginResponse();
                                    } else {
                                        // console.log('-----------______######');
                                        // console.log('Everything is all good.  Updated login report.');
                                        // return the finalLoginResponse()
                                        finalLoginResponse();
                                    }
                                });
                            }
                        });
                }
            }
        });
};

module.exports.contactTa = function(req, res) {
    // console.log('Contact Ta req.body:');
    // console.log(req.body);
    return res.json({
       success: true,
       message: 'Always return a value with these'
    });
};

module.exports.uploadToMediaLibrary = function(req, res) {
    // console.log(req.body);
    // console.log('req.body:');

    if (req.body.imageFile) {
        let postedimage = req.body.imageFile;
        // console.log(postedimage);
        // console.log('postedimage');
        function decodeBase64Image(dataString) {
            let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
                response = {};
            if (matches.length !== 3) {
                return new Error('Invalid input string');
            }
            response.type = matches[1];
            response.data = new Buffer(matches[2], 'base64');
            return response;
        }

        let imageBuffer = decodeBase64Image(postedimage);
        // console.log(imageBuffer);
        // console.log('imageBuffer above');
        let taccImageFile = 'tacc-photo-' + Date.now() + '.jpg';

        let fileImagePath = './public/media_library/'+taccImageFile;

        // save the file
        // fs.writeFile('.media_library/'+taccImageFile, imageBuffer.data, function(err) {
        // fs.writeFile('.public/media_library/'+taccImageFile, imageBuffer.data, function(err) {
        // fs.writeFile(__dirname + '/public/media_library/'+taccImageFile, imageBuffer.data, function(err) {
        // fs.writeFile(__dirname + './public/media_library/'+taccImageFile, imageBuffer.data, function(err) {
        // fs.writeFile('../public/media_library/'+taccImageFile, imageBuffer.data, function(err) {
        fs.writeFile(fileImagePath, imageBuffer.data, function(err) {
            // console.log('Local image is in right right.');
        });

        // console.log(taccImageFile);
        // console.log('taccImageFile to save.');
        // console.log(fileImagePath);
        // console.log('fileImagePath above');
        // console.log('--------------------------------');

        // Create a new media record
        // let media = new EMedia();
        let media = new Media();
        // let intApprovalStatus = parseInt(req.body.media.approval_status);

        if (req.body.imageObj.image_name) media.image_name = req.body.imageObj.image_name;
        if (req.body.imageObj.image_caption) media.image_caption = req.body.imageObj.image_caption;
        if (taccImageFile) media.file_name = taccImageFile;
        media.updated = todays;

        // console.log('-------------------------------');
        // console.log('New media to save: ');
        // console.log(media);

        // return res.json({
        //    success: true,
        //    message: 'Always return a value with these',
        //    media: media
        // });

        // save the media and check for errors
        media.save(function(err) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not save this media.'
                });
            } else {
                return res.json({
                    success: true,
                    message: 'Image saved!'
                });
            }
        });
    }
};

// ------------------------------------------------
module.exports.updateImageMedia = function(req, res) {
    // console.log(req.body);
    // console.log('req.body:');

    // Find the media image
    Media.findById(req.params.image_id)
        .exec(function(err, media) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not update your image'
                });
            } else if (media) {
                if (req.body.image_name) media.image_name = req.body.image_name;
                if (req.body.image_caption) media.image_caption = req.body.image_caption;
                media.updated = todays;

                // return res.json({
                //     success: true,
                //     message: 'Always return a value with these',
                //     media: media
                // });

                // save the media and check for errors
                media.save(function(err) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Could not save this media.'
                        });
                    } else {
                        return res.json({
                            success: true,
                            message: 'Image saved!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not update your image'
                });
            }
        });
};

// ------------------------------------------------
module.exports.deleteImageMedia = function(req, res) {
    // console.log(req.params.image_id);
    // console.log('req.params.image_id:');

    // return res.json({
    //     success: true,
    //     message: 'Always return a value with these',
    //     media_id: req.params.image_id
    // });

    Media.remove({
        '_id': req.params.image_id
    }, function(err, media) {
        if (err) {
            console.log(err);
            return res.json({
                success: false,
                message: 'Could not remove this image.  Something went wrong!'
            });
        } else if (media) {
            return res.json({
                success: true,
                message: 'Image successfully removed!'
            });
        } else {
            return res.json({
                success: false,
                message: 'Could not remove this image.  Something went wrong!'
            });
        }
    });
};

// ------------------------------------------------
module.exports.getAdminCategories = function(req, res) {
    // console.log(req.body);

    let categoryObj = req.body.categoryObj;
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');
                let adminCatQuery = {

                };
                let adminCatProjection = {
                    // '_id': 1,
                    'formattedDate': 0,
                    'updated': 0,
                    '__v': 0,
                    'created': 0
                };

                Category.find(adminCatQuery, adminCatProjection)
                    .sort({ category_description: 'asc', test: -1 })
                    .exec(function(err, categories) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find any categories.'
                            });
                        } else if (categories.length > 0) {
                            // console.log(categories);
                            // console.log('User that wants to view all categories.');
                            return res.json({
                                success: true,
                                message: 'All categories are in.',
                                adminCategories: categories
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find any categories.'
                            });
                        }
                    });

            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while creating this category.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getAllCategories = function(req, res) {
    let query = {

    };
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        '__v': 0,
        'created': 0
    };

    Category.find(query, projection)
        .sort({ category_description: 'asc', test: -1 })
        .exec(function(err, categories) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not find any categories.'
                });
            } else if (categories.length > 0) {
                // console.log(categories);
                // console.log('User that wants to view all categories.');
                return res.json({
                    success: true,
                    message: 'All categories are in.',
                    allProductCategories: categories
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not find any categories.'
                });
            }
        });
};

// ------------------------------------------------
module.exports.getMediaLibrary = function(req, res) {
    console.log('Media req.body');
    console.log(req.body);
    console.log('----------------------');
    // create the empty query object
    let query = {};
    let recordLimit = 100,
        requestedPage;

    if (req.body.page) {
        requestedPage = Number(req.body.page);
    } else {
        requestedPage = 1;
    }

    let options = {
        page: requestedPage,
        limit: recordLimit,
        sort: { image_name: 'asc', test: -1 }
    };

    // console.log('The search query');
    // console.log(query);

    // return res.json({
    //     success: true,
    //     message: 'Best in the west',
    //     query: query,
    //     options: options
    // });
    // -----------------------------
    Media.paginate(query, options, function(err, result) {
        console.log('result.docs.length');
        console.log(result.docs.length);
        if (err) {
            console.log(err);
            console.log('err above: ');
            return res.json({
                success: false,
                message: 'Sorry no images were found.'
            });
        } else if (result.docs.length > 0) {
            // console.log(result);
            var pageRange = [];
            // Create the page range
            for (var i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }
            return res.json({
                success: true,
                message: 'Results are in!',
                gallery: result.docs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange,
                    pageinfo: req.body
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Sorry no images were found.'
            });
        }
    });
};

// ------------------------------------------------
module.exports.getPagedImageLibrary = function(req, res) {
    console.log('Media req.body');
    console.log(req.body);
    console.log('----------------------');
    // create the empty query object
    let query = {};
    let recordLimit = 100,
        requestedPage;

    if (req.body.pageInfo) {
        requestedPage = parseInt(req.body.pageInfo);
    } else {
        requestedPage = 1;
    }

    let options = {
        page: requestedPage,
        limit: recordLimit
    };

    console.log('The search query');
    console.log(query);

    // -----------------------------
    Media.paginate(query, options, function(err, result) {
        console.log('result.docs.length');
        console.log(result.docs.length);
        if (err) {
            console.log(err);
            console.log('err above: ');
            return res.json({
                success: false,
                message: 'Sorry no images were found.'
            });
        } else if (result.docs.length > 0) {
            // console.log(result);
            var pageRange = [];
            // Create the page range
            for (var i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }
            return res.json({
                success: true,
                message: 'Results are in!',
                gallery: result.docs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange,
                    pageinfo: req.body
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Sorry no images were found.'
            });
        }
    });
};

module.exports.createNewProduct = function(req, res) {
    console.log(req.body);
    console.log('req.body create a new product');

    let productObj = req.body.productObj;
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                function createSlugTextString(arg) {
                    var finalSlug = arg.split(' ').join('_').toLowerCase();
                    return finalSlug
                }

                // Create a new product record
                let product = new Product();
                if (productObj.interior) {
                    product.interior = productObj.interior;
                    product.interior_slug = createSlugTextString(productObj.interior);
                }
                if (productObj.product_name) {
                    product.product_name = productObj.product_name;
                    product.product_name_slug = createSlugTextString(productObj.product_name);
                }
                if (productObj.color_finish) {
                    product.color_finish = productObj.color_finish;
                    product.color_finish_slug = createSlugTextString(productObj.color_finish);
                }
                if (productObj.material) {
                    product.material = productObj.material;
                    product.material_slug = createSlugTextString(productObj.material);
                }
                if (productObj.product_description) product.product_description             = productObj.product_description;
                if (productObj.category_code) product.category_code                         = productObj.category_code;
                if (productObj.base_price) product.base_price                               = productObj.base_price;
                if (productObj.image_name) product.image_name                               = productObj.image_name;
                if (productObj.thumbnail_image) product.thumbnail_image                     = productObj.thumbnail_image;
                if (productObj.model_style) product.model_style                             = productObj.model_style;
                if (productObj.notes) product.notes                                         = productObj.notes;
                if (req.body.todaysFormattedDate) product.formattedDate                     = req.body.todaysFormattedDate;
                product.created = todays;

                // Programatically create a product code
                function createProductCode() {
                    let getTimeNow = Date.parse(new Date());
                    let randNum = Math.random().toString();
                    let slicedNum = randNum.slice(3,9);

                    let newUserCode = 'product_'+getTimeNow+slicedNum;
                    return newUserCode;
                }
                product.product_code = createProductCode();

                // console.log(product);
                // console.log('New constructed product.');
                //
                // return res.json({
                //     success: false,
                //     message: 'No worries.  We are, Testing testingâ€¦',
                //     productToSave: product
                // });

                // save the product and check for errors
                product.save(function(err) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Something went wrong while creating this product.'
                        });
                    } else {
                        return res.json({
                            success: true,
                            message: 'Product created successfully!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while creating this product.'
                });
            }
        });
};

module.exports.createNewCategory = function(req, res) {
    // console.log(req.body);
    // console.log('req.body create a new category');

    let categoryObj = req.body.categoryObj;
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                // Create a new category record
                let category = new Category();

                if (categoryObj.category_description) category.category_description         = categoryObj.category_description;
                if (categoryObj.file_name) category.category_image                          = categoryObj.file_name;
                if (req.body.todaysFormattedDate) category.formattedDate                    = req.body.todaysFormattedDate;
                category.created = todays;

                // Programatically create a category code
                function createCategoryCode() {
                    let getTimeNow = Date.parse(new Date());
                    let randNum = Math.random().toString();
                    let slicedNum = randNum.slice(3,9);

                    let newUserCode = 'category_'+getTimeNow+slicedNum;
                    return newUserCode;
                }
                category.category_code = createCategoryCode();

                // console.log(category);
                // console.log('New constructed category.');

                // return res.json({
                //     success: false,
                //     message: 'No worries.  We are, Testing testingâ€¦',
                //     categoryToSave: category
                // });

                // save the category and check for errors
                category.save(function(err) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Something went wrong while creating this category.'
                        });
                    } else {
                        return res.json({
                            success: true,
                            message: 'Category created successfully!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while creating this category.'
                });
            }
        });
};

module.exports.getProductsByCategoryCode = function(req, res) {
    let query = {
        'category_code': req.params.category_code
    };
    let recordLimit = 100,
        requestedPage;

    if (req.body.page) {
        requestedPage = Number(req.body.page);
    } else {
        requestedPage = 1;
    }

    let options = {
        page: requestedPage,
        limit: recordLimit,
        sort: { product_name: 'asc', test: -1 }
    };
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        '__v': 0,
        'created': 0
    };

    // Product.find(query, projection)
    //     .sort({ product_name: 'asc', test: -1 })
    //     .exec(function(err, products) {
    Product.paginate(query, options, function(err, result) {
        if (err) {
            console.log(err);
            return res.json({
                success: false,
                message: 'Could not find any products.'
            });
        } else if (result.docs.length > 0) {
            // console.log(products);
            // console.log('User that wants to view all products.');
            // return res.json({
            //     success: true,
            //     message: 'All products are in.',
            //     productsFoundByCode: products
            // });
            console.log(result);
            console.log('result above --------------------------');
            var pageRange = [];
            // Create the page range
            for (var i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }
            return res.json({
                success: true,
                message: 'Products are in!',
                allProducts: result.docs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange,
                    pageinfo: req.body
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Could not find any products.'
            });
        }
    });
};

module.exports.getSingleProductByCode = function(req, res) {
    let query = {
        'product_code': req.params.product_code
    };
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        '__v': 0,
        'created': 0
    };

    Product.findOne(query, projection)
        .exec(function(err, product) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not find this product.'
                });
            } else if (product) {
                // console.log(product);
                // console.log('User that wants to view this product.');
                return res.json({
                    success: true,
                    message: 'Product is in.',
                    activeSingleProduct: product
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not find this product.'
                });
            }
        });
};

module.exports.getAllProducts = function(req, res) {
    let query = {

    };
    let recordLimit = 100,
        requestedPage;

    if (req.body.page) {
        requestedPage = req.body.page;
    } else {
        requestedPage = 1;
    }

    let options = {
        page: requestedPage,
        limit: recordLimit,
        sort: { product_name: 'asc', test: -1 }
    };
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        '__v': 0,
        'created': 0
    };

    // Product.find(query, projection)
    //     .sort({ product_name: 'asc', test: -1 })
    //     .exec(function(err, products) {
    Product.paginate(query, options, function(err, result) {
        console.log('result.docs.length');
        console.log(result.docs.length);
        if (err) {
            console.log(err);
            return res.json({
                success: false,
                message: 'Could not find any products.'
            });
        } else if (result.docs.length > 0) {
            // console.log(products);
            // console.log('User that wants to view all products.');
            // return res.json({
            //     success: true,
            //     message: 'All products are in.',
            //     allProducts: products
            // });
            // console.log(result);
            var pageRange = [];
            // Create the page range
            for (var i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }
            return res.json({
                success: true,
                message: 'Products are in!',
                allProducts: result.docs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange,
                    pageinfo: req.body
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Could not find any products.'
            });
        }
    });
};

module.exports.changeSingleCategoryData = function(req, res) {
    console.log(req.body);
    console.log('req.body create a new category');

    let categoryObj = req.body.categoryObj;
    let query = {
        '_id': req.body.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                let query = {
                    'category_code': req.body.category.category_code
                };
                Category.findOne(query)
                    .exec(function(err, category) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find this category.'
                            });
                        } else if (category) {
                            console.log(category);
                            console.log('category to be edited above: ');

                            if (req.body.category.category_description) category.category_description         = req.body.category.category_description;
                            if (req.body.category.category_image) category.category_image                     = req.body.category.category_image;
                            category.updated = todays;

                            // console.log(category);
                            // console.log('Category to update.');

                            // return res.json({
                            //     success: false,
                            //     message: 'No worries.  We are, Testing testingâ€¦'
                            // });

                            // save the category and check for errors
                            category.save(function(err) {
                                if (err) {
                                    console.log(err);
                                    return res.json({
                                        success: false,
                                        message: 'Something went wrong while updating this category.'
                                    });
                                } else {
                                    return res.json({
                                        success: true,
                                        message: 'Category updated successfully!'
                                    });
                                }
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find this category.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while updating this category.'
                });
            }
        });
};

module.exports.changeSingleProductData = function(req, res) {
    console.log(req.body);
    console.log('req.body update a product');

    let productObj = req.body.product;
    let query = {
        '_id': req.body.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                let query = {
                    'product_code': req.body.product.product_code
                };
                Product.findOne(query)
                    .exec(function(err, product) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not find this product.'
                            });
                        } else if (product) {
                            console.log(product);
                            console.log('product to be edited above: ');

                            if (req.body.product.category_code) product.category_code               = req.body.product.category_code;
                            if (req.body.product.product_description) product.product_description   = req.body.product.product_description;
                            if (req.body.product.interior) product.interior                         = req.body.product.interior;
                            if (req.body.product.product_name) product.product_name                 = req.body.product.product_name;
                            if (req.body.product.base_price) product.base_price                     = req.body.product.base_price;
                            if (req.body.product.color_finish) product.color_finish                 = req.body.product.color_finish;
                            if (req.body.product.image_name) product.image_name                     = req.body.product.image_name;
                            if (req.body.product.thumbnail_image) product.thumbnail_image           = req.body.product.thumbnail_image;
                            if (req.body.product.material) product.material                         = req.body.product.material;
                            if (req.body.product.model_style) product.model_style                   = req.body.product.model_style;
                            if (req.body.product.notes) product.notes                               = req.body.product.notes;
                            product.updated = todays;

                            // console.log(product);
                            // console.log('Product to update.');
                            //
                            // return res.json({
                            //     success: false,
                            //     message: 'No worries.  We are, Testing testingâ€¦'
                            // });

                            // save the product and check for errors
                            product.save(function(err) {
                                if (err) {
                                    console.log(err);
                                    return res.json({
                                        success: false,
                                        message: 'Something went wrong while updating this product.'
                                    });
                                } else {
                                    return res.json({
                                        success: true,
                                        message: 'Product updated successfully!'
                                    });
                                }
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not find this product.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while updating this product.'
                });
            }
        });
};

module.exports.deleteProduct = function(req, res) {
    // console.log(req.body);
    // console.log('req.body delete a product');

    let productCode = req.body.product_code;
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                Product.remove({
                    'product_code': productCode
                }, function(err, product) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Could not remove this product.  Something went wrong!'
                        });
                    } else if (product) {
                        return res.json({
                            success: true,
                            message: 'Product successfully removed!'
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'Could not remove this product.  Something went wrong!'
                        });
                    }
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while updating this product.'
                });
            }
        });
};

module.exports.deleteCategory = function(req, res) {
    console.log(req.body);
    console.log('req.body delete a category');

    let categoryId = req.body.category_id;
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                // console.log(user);
                // console.log('User that wants toâ€¦');

                // Category.findOne({'category_code': categoryCode})
                Category.findOne({'_id': categoryId})
                    .remove()
                    .exec(function(err, category) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not remove this category.  Something went wrong!'
                            });
                        } else if (category) {
                            // console.log(category);
                            return res.json({
                                success: true,
                                message: 'Category successfully removed!'
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not remove this category.  Something went wrong!'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while updating this category.'
                });
            }
        });
};

module.exports.manageMailingList = function(req, res) {
    console.log(req.body);
    console.log('req.body unsubscribe a mailinglist');

    function mailingListCreation() {
        // Create a new mailinglist record
        let mailinglist = new MailingList();

        if (req.body.users_mailing_name) mailinglist.users_mailing_name = req.body.users_mailing_name;
        if (req.body.email) mailinglist.email                           = req.body.email;
        if (req.body.todaysFormattedDate) mailinglist.formattedDate     = req.body.todaysFormattedDate;
        mailinglist.user_wants_subscription                             = true;
        mailinglist.created                                             = todays;

        // console.log(mailinglist);
        // console.log('New constructed mailinglist.');

        // return res.json({
        //     success: false,
        //     message: 'No worries.  We are, Testing testingâ€¦',
        //     mailinglist: mailinglist
        // });

        mailinglist.save(function(err) {
            if (err) {
                console.log(err);
                if (err.code === 11000) {
                    return res.json({
                        success: false,
                        message: 'This email address has already been added to our unsubscribe list.'
                    });
                } else {
                    return res.json({
                        success: false,
                        message: 'Something went wrong while processing your request.'
                    });
                }
            } else {
                return res.json({
                    success: true,
                    message: 'Your email address has been successfully removed from our mailing list!'
                });
            }
        });
    }

    function mailingListUpdate() {
        let mailingListUpdateQuery = {
            'email': req.body.email
        };

        MailingList.findOne(mailingListUpdateQuery)
            .exec(function(err, managemailinglist) {
                if (err) {
                    console.log(err);
                    return res.json({
                        success: false,
                        message: 'Could not process this request.'
                    });
                } else if (managemailinglist) {
                    console.log(managemailinglist);
                    console.log('managemailinglist TO UPDATE above: ---------------------');

                    // what is the type of subscription var
                    var wantsSub = typeof(req.body.user_wants_subscription);
                    console.log(wantsSub);
                    console.log('wantsSub');

                    if (req.body.users_mailing_name) managemailinglist.users_mailing_name       = req.body.users_mailing_name;
                    if (req.body.todaysFormattedDate) managemailinglist.formattedDate           = req.body.todaysFormattedDate;
                    if (req.body.user_wants_subscription === true) {
                        managemailinglist.user_wants_subscription = true;
                    } else {
                        managemailinglist.user_wants_subscription = false;
                    }
                    managemailinglist.created                                                         = todays;

                    // console.log(managemailinglist);
                    // console.log('New constructed managemailinglist.');

                    // return res.json({
                    //     success: false,
                    //     message: 'No worries.  We are, Testing testingâ€¦',
                    //     managemailinglist: managemailinglist
                    // });

                    managemailinglist.save(function(err) {
                        if (err) {
                            console.log(err);
                            if (err.code === 11000) {
                                return res.json({
                                    success: false,
                                    message: 'This email address has already been added to our unsubscribe list.'
                                });
                            } else {
                                return res.json({
                                    success: false,
                                    message: 'Something went wrong while processing your request.'
                                });
                            }
                        } else {
                            return res.json({
                                success: true,
                                message: 'Your email address has been successfully removed from our mailing list!'
                            });
                        }
                    });
                } else {
                    return res.json({
                        success: false,
                        message: 'Could not process this request.'
                    });
                }
            });
    }

    // check to see if email address exists first
    let mailingListQuery = {
        'email': req.body.email
    };

    MailingList.findOne(mailingListQuery)
        .exec(function(err, foundmailingrecord) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not process this request.'
                });
            } else if (foundmailingrecord) {
                // console.log(foundmailingrecord);
                // console.log('foundmailingrecord above:');
                mailingListUpdate();
            } else if (!foundmailingrecord) {
                // console.log('No email record found above.  Let me create a new one.');
                mailingListCreation();
            } else {
                return res.json({
                    success: false,
                    message: 'Could not process this request.'
                });
            }
        });
};

module.exports.getMailingListEmails = function(req, res) {
    console.log(req.body);
    console.log('req.body unsubscribe a mailinglist');

    let query = {};
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        'created': 0,
        '__v': 0
    };

    MailingList.find(query, projection)
        .exec(function(err, list) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not find any emails.'
                });
            } else if (list) {
                let subscribedEmailsList = [];
                let unSubscribedEmailsList = [];
                // console.log(list);
                // console.log('list above:');

                // Put Emails in their respective array
                list.forEach(function(item) {
                    if (item.user_wants_subscription === true) {
                        subscribedEmailsList.push(item);
                    } else {
                        unSubscribedEmailsList.push(item);
                    }
                });

                return res.json({
                    success: true,
                    message: 'Mailing list is in!',
                    subscribedEmailsList: subscribedEmailsList,
                    unSubscribedEmailsList: unSubscribedEmailsList
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not find any emails.'
                });
            }
        });

};

module.exports.searchProducts = function(req, res) {
    console.log(req.body);
    console.log('req.body for searching products');

    let query = {};
    let projection = {
        '_id': 0,
        'formattedDate': 0,
        'updated': 0,
        'created': 0
    };

    // function createSlugTextString(arg) {
    //     var finalSlug = arg.split(' ').join('_').toLowerCase();
    //     return finalSlug
    // }

    // function createLowerCaseString(arg) {
    //     let finalString = {
    //         loweredString: arg.toLowerCase(),
    //         // regPattern: /^arg.toLowerCase()/i
    //         // regPattern: '/^'+arg.toLowerCase()+'/i'
    //         regPattern: '/'+arg.toLowerCase()+'w/'
    //     }
    //     return finalString
    // }

    // Add items to the query if they exist.
    if (req.body.searchProductsObj.product_name) {
        // query.product_name_slug = req.body.searchProductsObj.product_name;
        // let productString = createLowerCaseString(req.body.searchProductsObj.product_name);
        let productString = req.body.searchProductsObj.product_name.toLowerCase();
        // let regStringPattern = '/^'+productString+'/i';
        // let regStringPattern = /^+productString+/i;
        // let regProductPatter
        // query.product_name_slug = { $regex: /^'+productString+'/i };
        // query.product_name_slug = { $regex: /^productString/i };
        // query.product_name_slug = { $regex: productString.regPattern };
        // query.product_name_slug = { $regex: productString.regPattern };
        // query.product_name_slug = { $regex: /^pembro/i };
        // query.product_name_slug = { $regex: /pembro/ };
        query.product_name_slug = { $regex: productString };
        // query.product_name_slug = { $regex: regStringPattern };
        // query.product_name_slug = productString;
        // { 'color_finish_slug': { $regex: /^polished_finish/i } }
    }
    if (req.body.searchProductsObj.material) {
        let materialString = req.body.searchProductsObj.material.toLowerCase();
        query.material_slug = { $regex: materialString };
        // query.material_slug = req.body.searchProductsObj.material;
    }
    if (req.body.searchProductsObj.color_finish) {
        let colorFinishString = req.body.searchProductsObj.color_finish.toLowerCase();
        query.color_finish_slug = { $regex: colorFinishString };
        // query.color_finish_slug = req.body.searchProductsObj.color_finish;
    }
    if (req.body.searchProductsObj.interior) {
        let interiorString = req.body.searchProductsObj.interior.toLowerCase();
        query.interior_slug = { $regex: interiorString };
        // query.interior_slug = req.body.searchProductsObj.interior;
    }
    console.log(query);
    console.log('query object: ');
    //
    // return res.json({
    //     success: false,
    //     message: 'No worries.  We are, Testing testingâ€¦',
    //     query: query,
    //     projection: projection
    // });

    Product.find(query, projection)
        .sort({ product_name: 'asc', test: -1 })
        .exec(function(err, products) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'No products found.  Something went wrong while processing your request.',
                    query: query
                });
            } else if (products.length > 0) {
                console.log(products);
                console.log('Products array listed above.');
                return res.json({
                    success: true,
                    message: 'Products are in!',
                    products: products,
                    query: query
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No products found.',
                    query: query
                });
            }
        });
};

module.exports.masterSiteRateIncrease = function(req, res) {
    // User must be a super administrator in order process / access this method.
    let query = {
        '_id': req.params.user_id,
        'access_level': 1
    };

    User.findOne(query)
        .exec(function(err, user) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not grant access with your credentials for this action.'
                });
            } else if (user) {
                console.log(user);
                console.log('SUPER User that wants toâ€¦');
                console.log('----- NOW ON TO THE SITE MASTER PROCESSING ---------');
                console.log(req.body);
                console.log('req.body for the master site configuration record.');

                function siteMasterCreation() {
                    // Create a new site master record
                    let sitemaster = new SiteMaster();

                    if (req.body.master_product_price_markup) sitemaster.master_product_price_markup  = req.body.master_product_price_markup;
                    if (req.body.todaysFormattedDate) sitemaster.formattedDate                        = req.body.todaysFormattedDate;
                    sitemaster.created                                                                = todays;

                    // console.log(sitemaster);
                    // console.log('New constructed sitemaster record.');
                    //
                    // return res.json({
                    //     success: false,
                    //     message: 'No worries.  We are, Testing testingâ€¦',
                    //     siteMasterRecord: sitemaster
                    // });

                    sitemaster.save(function(err) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Something went wrong while processing your request.'
                            });
                        } else {
                            return res.json({
                                success: true,
                                message: 'Your master record is now created!',
                                siteMasterRecord: sitemaster
                            });
                        }
                    });
                }

                function siteMasterUpdate() {
                    // THERE SHOULD ONLY EVER BE ONE RECORD IN THIS COLLECTION!!!!
                    SiteMaster.findOne()
                        .exec(function(err, sitemaster) {
                            if (err) {
                                console.log(err);
                                return res.json({
                                    success: false,
                                    message: 'Could not process this request.'
                                });
                            } else if (sitemaster) {
                                // console.log(sitemaster);
                                // console.log('sitemaster TO UPDATE above: ---------------------');

                                if (req.body.master_product_price_markup) sitemaster.master_product_price_markup = req.body.master_product_price_markup;
                                if (req.body.todaysFormattedDate) sitemaster.formattedDate                       = req.body.todaysFormattedDate;
                                sitemaster.created                                                               = todays;

                                // console.log(sitemaster);
                                // console.log('New constructed sitemaster to UPDATE.');
                                //
                                // return res.json({
                                //     success: false,
                                //     message: 'No worries.  We are, Testing testingâ€¦',
                                //     siteMasterRecord: sitemaster
                                // });

                                sitemaster.save(function(err) {
                                    if (err) {
                                        console.log(err);
                                        return res.json({
                                            success: false,
                                            message: 'Something went wrong while processing your request.'
                                        });
                                    } else {
                                        return res.json({
                                            success: true,
                                            message: 'The master site record has been updated!',
                                            siteMasterRecord: sitemaster
                                        });
                                    }
                                });
                            } else {
                                return res.json({
                                    success: false,
                                    message: 'Could not process this request.'
                                });
                            }
                        });
                }

                // check to see if a master site record exists first
                SiteMaster.findOne()
                    .exec(function(err, foundsitemaster) {
                        if (err) {
                            console.log(err);
                            return res.json({
                                success: false,
                                message: 'Could not process this request.'
                            });
                        } else if (foundsitemaster) {
                            console.log(foundsitemaster);
                            console.log('foundsitemaster above:');
                            siteMasterUpdate();
                        } else if (!foundsitemaster) {
                            console.log('No site master record found above.  Let me create a new one.');
                            siteMasterCreation();
                        } else {
                            return res.json({
                                success: false,
                                message: 'Could not process this request.'
                            });
                        }
                    });
            } else {
                return res.json({
                    success: false,
                    message: 'Something went wrong while updating this product.'
                });
            }
        });
};

module.exports.getMasterPriceRate = function(req, res) {
    // check to see if a master site record exists first
    SiteMaster.findOne()
        .exec(function(err, foundsitemaster) {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message: 'Could not process this request.'
                });
            } else if (foundsitemaster) {
                console.log(foundsitemaster);
                console.log('foundsitemaster above:');
                return res.json({
                    success: true,
                    message: 'Could not process this request.',
                    siteMasterObj: foundsitemaster
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not process this request.'
                });
            }
        });
};
//
// module.exports.migrateOldUsers = function(req, res) {
//     console.log(req.body);
//     console.log('req.body for migrating old users.');
//
//     // Make sure the user object has a username and password or skip over it.
//     if ((req.body.oldUserObj.username) &&
//         (req.body.oldUserObj.password)) {
//         let oldUserQuery = {
//             'username': req.body.oldUserObj.username
//         };
//         // check a few things before continuing
//         User.findOne(oldUserQuery)
//             .exec(function(err, founduser) {
//                 if (err) {
//                     console.log(err);
//                     return res.json({
//                         success: true,
//                         message: 'There was an error but keep going.'
//                     });
//                 } else if (!founduser) {
//                     // create a new instance of the User model
//                     let newUserObject = req.body.oldUserObj;
//                     let accessLevel;
//                     let user = new User();
//
//                     // set the users information (comes from the request)
//                     if (newUserObject.old_id) user.old_id                                   = newUserObject.old_id;
//                     if (newUserObject.username) user.username                               = newUserObject.username.toLowerCase();
//                     if (newUserObject.contact_person) user.contact_person                   = newUserObject.contact_person;
//                     if (newUserObject.fac_name) user.fac_name                               = newUserObject.fac_name;
//                     if (req.body.account_type) user.account_type                            = req.body.account_type;
//                     if (newUserObject.password) user.password                               = newUserObject.password;
//                     if (newUserObject.parent_account) {
//                         if (newUserObject.parent_account.user_code) user.parent_account.user_code = newUserObject.parent_account.user_code;
//                         if (newUserObject.parent_account.contact_person) user.parent_account.contact_person = newUserObject.parent_account.contact_person;
//                     }
//                     if (newUserObject.email) user.email                                     = newUserObject.email;
//                     if (newUserObject.phone) user.phone                                     = newUserObject.phone;
//                     if (newUserObject.fax) user.fax                                         = newUserObject.fax;
//                     if (newUserObject.firstname) user.firstname                             = newUserObject.firstname;
//                     if (newUserObject.lastname) user.lastname                               = newUserObject.lastname;
//                     if (newUserObject.address_1) user.address_1                             = newUserObject.address_1;
//                     if (newUserObject.address_2) user.address_2                             = newUserObject.address_2;
//                     if (newUserObject.city) user.city                                       = newUserObject.city;
//                     if (newUserObject.state) user.state                                     = newUserObject.state;
//                     if (newUserObject.zip) user.zip                                         = newUserObject.zip;
//                     if (newUserObject.web) user.web                                         = newUserObject.web;
//                     if (newUserObject.price_rounding) user.price_rounding                   = newUserObject.price_rounding;
//                     if (req.body.todaysFormattedDate) user.formattedDate                    = req.body.todaysFormattedDate;
//                     switch (newUserObject.access_level) {
//                         case '1':
//                             accessLevel = 1;
//                             break;
//                         case '2':
//                             accessLevel = 2;
//                             break;
//                         case '3':
//                             accessLevel = 3;
//                             break;
//                         case '4':
//                             accessLevel = 4;
//                             break;
//                         case '5':
//                             accessLevel = 5;
//                             break;
//                       default:
//                           accessLevel = 5;
//                     }
//                     user.access_level                                                       = accessLevel;
//                     user.created = todays;
//
//                     // Programatically create a user code
//                     function createUserCode() {
//                         let getTimeNow = Date.parse(new Date());
//                         let randNum = Math.random().toString();
//                         let slicedNum = randNum.slice(3,9);
//
//                         let newUserCode = 'user_'+getTimeNow+slicedNum;
//                         return newUserCode;
//                     }
//                     user.user_code = createUserCode();
//
//                     // console.log('New constructed user.');
//                     // console.log(user);
//                     //
//                     // return res.json({
//                     //     success: true,
//                     //     message: 'No worries.  We are, Testing testingâ€¦',
//                     //     userType: req.body.account_type,
//                     //     userToSave: user
//                     // });
//
//                     // save the user and check for errors
//                     user.save(function(err) {
//                         if (err) {
//                             console.log(err);
//                             return res.json({
//                                 success: true,
//                                 message: 'Something went wrong while saving this account.'
//                             });
//                         } else {
//                             // sendSignupEmailMessage(user.email, specialMessage);
//                             return res.json({
//                                 success: true,
//                                 message: 'Account created successfully!'
//                             });
//                         }
//                     });
//                 } else if (founduser) {
//                     // console.log(founduser);
//                     // console.log('founduser above:');
//                     return res.json({
//                         success: true,
//                         message: 'This user already exists so keep going.'
//                     });
//                 } else {
//                     return res.json({
//                         success: true,
//                         message: 'There was an error but keep going.'
//                     });
//                 }
//             });
//     } else {
//         return res.json({
//             success: true,
//             message: 'Skipping over this one.  Did not meet the requirements.'
//         });
//     }
// };
//
// module.exports.migrateOldProducts = function(req, res) {
//     console.log(req.body);
//     console.log('req.body for migrating old products.');
//
//     return res.json({
//         success: false,
//         message: 'No worries.  We are, Testing testingâ€¦',
//         requestBodyObj: req.body
//     });
// };
//
// module.exports.migrateOldImages = function(req, res) {
//     console.log(req.body);
//     console.log('req.body for migrating old images.');
//
//     return res.json({
//         success: false,
//         message: 'No worries.  We are, Testing testingâ€¦',
//         requestBodyObj: req.body
//     });
// };

module.exports.fixOldProductsArray = function(req, res) {
    console.log('Create a json reference');
    let oldProductsArray = [
        {
        	"old_id": "1",
        	"product_name": "Clifton",
        	"product_description": "",
        	"image_name": "images\/products\/1452.jpg",
        	"base_price": "2960",
        	"material": "Solid Walnut",
        	"old_color_finish": null,
        	"interior": "****Special Order****",
        	"color_finish": " Rubbed Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1452.jpg"
        }, {
        	"old_id": "4",
        	"product_name": "Pembroke Cherry",
        	"product_description": "12V",
        	"image_name": "images\/products\/1415pembroke.jpg",
        	"base_price": "3320",
        	"material": "Solid Cherry",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1415pembroke.jpg"
        }, {
        	"old_id": "5",
        	"product_name": "Oregon",
        	"product_description": "12L",
        	"image_name": "images\/products\/1437.jpg",
        	"base_price": "2120",
        	"material": "Solid Cherry",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Polish Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1437.jpg"
        }, {
        	"old_id": "6",
        	"product_name": "Victoria",
        	"product_description": "12V",
        	"image_name": "images\/products\/1414W.jpg",
        	"base_price": "2420",
        	"material": "Solid Cherry",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1414W.jpg"
        }, {
        	"old_id": "7",
        	"product_name": "Washington",
        	"product_description": "12P",
        	"image_name": "images\/products\/1412.jpg",
        	"base_price": "2420",
        	"material": "Solid Cherry",
        	"old_color_finish": null,
        	"interior": "Eggshell Velvet",
        	"color_finish": "Rubbed Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1412.jpg"
        }, {
        	"old_id": "9",
        	"product_name": "Drake Oak",
        	"product_description": "12G",
        	"image_name": "images\/products\/1406.png",
        	"base_price": "1920",
        	"material": "Solid Oak",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Saitn Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1406.png"
        }, {
        	"old_id": "10",
        	"product_name": "Memory",
        	"product_description": "12N",
        	"image_name": "images\/products\/1415.jpg",
        	"base_price": "2020",
        	"material": "Solid Oak",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1415.jpg"
        }, {
        	"old_id": "11",
        	"product_name": "Oak Ridge ",
        	"product_description": "12T",
        	"image_name": "images\/products\/1422.jpg",
        	"base_price": "1320",
        	"material": "Oak",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Light Oak",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1422.jpg"
        }, {
        	"old_id": "12",
        	"product_name": "Oakboro",
        	"product_description": "12Z",
        	"image_name": "images\/products\/1426.jpg",
        	"base_price": "1320",
        	"material": "Oak",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Dark Oak",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1426.jpg"
        }, {
        	"old_id": "13",
        	"product_name": "Gaston",
        	"product_description": "12K",
        	"image_name": "images\/products\/1409.jpg",
        	"base_price": "1920",
        	"material": "Solid Maple",
        	"old_color_finish": null,
        	"interior": "Pink or Rosetan",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1409.jpg"
        }, {
        	"old_id": "14",
        	"product_name": "Woodsdale",
        	"product_description": "12C",
        	"image_name": "images\/products\/1455.jpg",
        	"base_price": "1870",
        	"material": "Pecan",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1455.jpg"
        }, {
        	"old_id": "15",
        	"product_name": "Lane",
        	"product_description": "12K",
        	"image_name": "images\/products\/1420.jpg",
        	"base_price": "1870",
        	"material": "Solid Cedar",
        	"old_color_finish": null,
        	"interior": "Eggshell Crepe",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1420.jpg"
        }, {
        	"old_id": "17",
        	"product_name": "Alston",
        	"product_description": "12E",
        	"image_name": "images\/products\/1423.jpg",
        	"base_price": "1320",
        	"material": "Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Cherry Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1423.jpg"
        }, {
        	"old_id": "20",
        	"product_name": "Magnolia Velvet",
        	"product_description": "12S",
        	"image_name": "images\/products\/1454velvet.jpg",
        	"base_price": "1920",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1454velvet.jpg"
        }, {
        	"old_id": "22",
        	"product_name": "Caswell",
        	"product_description": "",
        	"image_name": "images\/products\/1458.jpg",
        	"base_price": "1520",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Basketweave",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1458.jpg"
        }, {
        	"old_id": "23",
        	"product_name": "American",
        	"product_description": "12F",
        	"image_name": "images\/products\/1419.jpg",
        	"base_price": "1470",
        	"material": "Pine",
        	"old_color_finish": null,
        	"interior": "Eggshell Crepe",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1419.jpg"
        }, {
        	"old_id": "26",
        	"product_name": "Levi Flat Top",
        	"product_description": "12C",
        	"image_name": "images\/products\/1483.jpg",
        	"base_price": "840",
        	"material": "Pine Orthodox",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Clear Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1483.jpg"
        }, {
        	"old_id": "27",
        	"product_name": "Berkley",
        	"product_description": "12C",
        	"image_name": "images\/products\/1484.jpg",
        	"base_price": "880",
        	"material": "Solid Pine",
        	"old_color_finish": null,
        	"interior": "Cotton",
        	"color_finish": "Certified Green",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1484.jpg"
        }, {
        	"old_id": "28",
        	"product_name": "Pinetown",
        	"product_description": "12S",
        	"image_name": "images\/products\/1421.jpg",
        	"base_price": "1150",
        	"material": "Pine",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Dark Pine",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1421.jpg"
        }, {
        	"old_id": "29",
        	"product_name": "Agean",
        	"product_description": "12e",
        	"image_name": "images\/products\/1506CP.jpg",
        	"base_price": "3420",
        	"material": "Solid Copper",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Brushed Natural",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1506CP.jpg"
        }, {
        	"old_id": "30",
        	"product_name": "Alamance",
        	"product_description": "12E",
        	"image_name": "images\/products\/1510e.jpg",
        	"base_price": "1660",
        	"material": "Stainless Steel",
        	"old_color_finish": null,
        	"interior": "Velvet",
        	"color_finish": "Ebony Brushed Natural",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1510e.jpg"
        }, {
        	"old_id": "31",
        	"product_name": "Onyx Stainless",
        	"product_description": "12F",
        	"image_name": "images\/products\/1504E.jpg",
        	"base_price": "1660",
        	"material": "Stainless Steel",
        	"old_color_finish": null,
        	"interior": "Silver Velvet",
        	"color_finish": "Ebony Brushed Natural ",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1504E.jpg"
        }, {
        	"old_id": "32",
        	"product_name": "Prayer Stainless",
        	"product_description": "12J",
        	"image_name": "images\/products\/1504sb.jpg",
        	"base_price": "1660",
        	"material": "Stainless Steel ",
        	"old_color_finish": null,
        	"interior": "Blue Velvet",
        	"color_finish": "Blue Brushed Natural",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1504sb.jpg"
        }, {
        	"old_id": "33",
        	"product_name": "Tapestry Rose",
        	"product_description": "12C",
        	"image_name": "images\/products\/1504HY.jpg",
        	"base_price": "1660",
        	"material": "Stainless Steel ",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Rose Brushed",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1504HY.jpg"
        }, {
        	"old_id": "35",
        	"product_name": "Masterpiece***Special Order***",
        	"product_description": "12J",
        	"image_name": "images\/products\/1601B.jpg",
        	"base_price": "2400",
        	"material": "16 Gauge",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Silver",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1601B.jpg"
        }, {
        	"old_id": "36",
        	"product_name": "Alamance Wineberry",
        	"product_description": "12F",
        	"image_name": "images\/products\/1809wn.jpg",
        	"base_price": "1360",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Wineberry Brushed ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1809wn.jpg"
        }, {
        	"old_id": "38",
        	"product_name": "Burgandy Rose",
        	"product_description": "12J",
        	"image_name": "images\/products\/1840byr.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Burgandy Brushed",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1840byr.jpg"
        }, {
        	"old_id": "39",
        	"product_name": "Calvary Blue",
        	"product_description": "12T",
        	"image_name": "images\/products\/2082bb.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue Bl. Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2082bb.jpg"
        }, {
        	"old_id": "40",
        	"product_name": "Carnation",
        	"product_description": "12J",
        	"image_name": "images\/products\/1862wv.jpg",
        	"base_price": "1310",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "White Shaded Rose",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1862wv.jpg"
        }, {
        	"old_id": "41",
        	"product_name": "Catawba Silver",
        	"product_description": "12C",
        	"image_name": "images\/products\/1882ps.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1882ps.jpg"
        }, {
        	"old_id": "42",
        	"product_name": "Corinthian Blue",
        	"product_description": "",
        	"image_name": "images\/products\/1872B.jpg",
        	"base_price": "990",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1872B.jpg"
        }, {
        	"old_id": "43",
        	"product_name": "Faithful White",
        	"product_description": "12G",
        	"image_name": "images\/products\/2080w.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2080w.jpg"
        }, {
        	"old_id": "44",
        	"product_name": "Father ",
        	"product_description": "12F",
        	"image_name": "images\/products\/1807bs.jpg",
        	"base_price": "1130",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue Blend ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1807bs.jpg"
        }, {
        	"old_id": "45",
        	"product_name": "Fleming Brushed",
        	"product_description": "12T",
        	"image_name": "images\/products\/1808vbz.jpg",
        	"base_price": "1210",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "V. Bronze Brushed Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1808vbz.jpg"
        }, {
        	"old_id": "47",
        	"product_name": "Going Home",
        	"product_description": "12C",
        	"image_name": "images\/products\/1803VB.jpg",
        	"base_price": "930",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1803VB.jpg"
        }, {
        	"old_id": "48",
        	"product_name": "Hamilton Brushed",
        	"product_description": "12G",
        	"image_name": "images\/products\/1832e.jpg",
        	"base_price": "1240",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Ebony Brushed Natural ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1832e.jpg"
        }, {
        	"old_id": "50",
        	"product_name": "Hatteras",
        	"product_description": "12K",
        	"image_name": "images\/products\/1882vb.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue Shaded Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1882vb.jpg"
        }, {
        	"old_id": "53",
        	"product_name": "Lexington Glass Full Couch",
        	"product_description": "12T",
        	"image_name": "images\/products\/1805GG.jpg",
        	"base_price": "2990",
        	"material": "18 Gauge w\/ Glass",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Gold Brushed Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805GG.jpg"
        }, {
        	"old_id": "54",
        	"product_name": "Lords Supper\/West Minister",
        	"product_description": "12G",
        	"image_name": "images\/products\/1843S.jpg",
        	"base_price": "1770",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Silver Brushed Natural",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1843S.jpg"
        }, {
        	"old_id": "55",
        	"product_name": "Meadow ",
        	"product_description": "12K",
        	"image_name": "images\/products\/1885wpx.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1885wpx.jpg"
        }, {
        	"old_id": "56",
        	"product_name": "Mother",
        	"product_description": "12T",
        	"image_name": "images\/products\/1807oz.jpg",
        	"base_price": "1130",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid Blend",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1807oz.jpg"
        }, {
        	"old_id": "57",
        	"product_name": "Neuse Blue",
        	"product_description": "12J",
        	"image_name": "images\/products\/1820B.jpg",
        	"base_price": "840",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Powder Blue Shaded Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1820B.jpg"
        }, {
        	"old_id": "58",
        	"product_name": "Rose Hill",
        	"product_description": "12Y",
        	"image_name": "images\/products\/2083wp.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2083wp.jpg"
        }, {
        	"old_id": "59",
        	"product_name": "Silver Rose",
        	"product_description": "12K",
        	"image_name": "images\/products\/1840sr.jpg",
        	"base_price": "1450",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Silver Rose",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1840sr.jpg"
        }, {
        	"old_id": "61",
        	"product_name": "Wallace",
        	"product_description": "",
        	"image_name": "images\/products\/2085se.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2085se.jpg"
        }, {
        	"old_id": "66",
        	"product_name": "Birds",
        	"product_description": "12Z",
        	"image_name": "images\/products\/2218VB.jpg",
        	"base_price": "660",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue Shaded Silver",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2218VB.jpg"
        }, {
        	"old_id": "67",
        	"product_name": "Garland",
        	"product_description": "12W",
        	"image_name": "images\/products\/2209wp.jpg",
        	"base_price": "690",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2209wp.jpg"
        }, {
        	"old_id": "68",
        	"product_name": "Lumber River Pink",
        	"product_description": "12S",
        	"image_name": "images\/products\/2211wp.jpg",
        	"base_price": "660",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "White\/Pink or White\/Blue",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2211wp.jpg"
        }, {
        	"old_id": "69",
        	"product_name": "Pace Silver",
        	"product_description": "12E",
        	"image_name": "images\/products\/2206.jpg",
        	"base_price": "550",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver ",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2206.jpg"
        }, {
        	"old_id": "70",
        	"product_name": "Rose",
        	"product_description": "12R",
        	"image_name": "images\/products\/2217.jpg",
        	"base_price": "840",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2217.jpg"
        }, {
        	"old_id": "71",
        	"product_name": "Fair",
        	"product_description": "",
        	"image_name": "images\/products\/1203b.jpg",
        	"base_price": "440",
        	"material": "Cardboard",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue Tabor Wood Handles",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1203b.jpg"
        }, {
        	"old_id": "72",
        	"product_name": "Eden",
        	"product_description": "",
        	"image_name": "images\/products\/1201.jpg",
        	"base_price": "360",
        	"material": "Particle Board",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Gray Doeskin",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1201.jpg"
        }, {
        	"old_id": "73",
        	"product_name": "Kraft",
        	"product_description": "12J",
        	"image_name": "images\/products\/1202.jpg",
        	"base_price": "350",
        	"material": "Cardboard",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Gray Doeskin",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1202.jpg"
        }, {
        	"old_id": "75",
        	"product_name": "Shellbrook",
        	"product_description": "12T",
        	"image_name": "images\/products\/1253.jpg",
        	"base_price": "830",
        	"material": "Particle Board",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Cherry Laminated",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1253.jpg"
        }, {
        	"old_id": "76",
        	"product_name": "Rental Insert",
        	"product_description": "12S",
        	"image_name": "images\/products\/1299.jpg",
        	"base_price": "240",
        	"material": "Cardboard",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "N\/A",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1299.jpg"
        }, {
        	"old_id": "87",
        	"product_name": "Child 3-6 White\/Gold",
        	"product_description": "12T",
        	"image_name": "images\/products\/c46wg.jpg",
        	"base_price": "800",
        	"material": "20 GA Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White Shaded Gold ",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_c46wg.jpg"
        }, {
        	"old_id": "93",
        	"product_name": "Alexander Calvary 29",
        	"product_description": "12J",
        	"image_name": "images\/products\/1828bb.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Viking Blue or Bronze",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828bb.jpg"
        }, {
        	"old_id": "94",
        	"product_name": "Alexander Catawba Silver 29",
        	"product_description": "12F",
        	"image_name": "images\/products\/1828s.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Silver or Blue",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828s.jpg"
        }, {
        	"old_id": "95",
        	"product_name": "Alexander Faithful White 29",
        	"product_description": "12K",
        	"image_name": "images\/products\/1828wg.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White w\/ Gold Pinstripes",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828wg.jpg"
        }, {
        	"old_id": "96",
        	"product_name": "Alexander Meadow 29",
        	"product_description": "12T",
        	"image_name": "images\/products\/1828wp.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White shaded Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828wp.jpg"
        }, {
        	"old_id": "97",
        	"product_name": "Emperor White 28",
        	"product_description": "12G",
        	"image_name": "images\/products\/2004W.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004W.jpg"
        }, {
        	"old_id": "98",
        	"product_name": "Bishop 28",
        	"product_description": "12F",
        	"image_name": "images\/products\/2004WG.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White Shaded Gold",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004WG.jpg"
        }, {
        	"old_id": "100",
        	"product_name": "Emperor Silver 28",
        	"product_description": "12T",
        	"image_name": "images\/products\/2004s.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver shaded Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004s.jpg"
        }, {
        	"old_id": "101",
        	"product_name": "Empress White\/Pink 28",
        	"product_description": "12F",
        	"image_name": "images\/products\/2004WP.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink ",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004WP.jpg"
        }, {
        	"old_id": "102",
        	"product_name": "Father Plus 28",
        	"product_description": "12K",
        	"image_name": "images\/products\/2004bs.jpg",
        	"base_price": "1230",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue Blend",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004bs.jpg"
        }, {
        	"old_id": "104",
        	"product_name": "Homeward Bronze 28",
        	"product_description": "12J",
        	"image_name": "images\/products\/2004BZ-G.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Gold",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004BZ-G.jpg"
        }, {
        	"old_id": "105",
        	"product_name": "Mother Plus 28",
        	"product_description": "12C",
        	"image_name": "images\/products\/2004o.jpg",
        	"base_price": "1230",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid Blend",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004o.jpg"
        }, {
        	"old_id": "106",
        	"product_name": "Knight Plus 28",
        	"product_description": "12T",
        	"image_name": "images\/products\/2004E-M.jpg",
        	"base_price": "1230",
        	"material": "20 Gauge Gasket",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony Silver Mirrors",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004E-M.jpg"
        }, {
        	"old_id": "107",
        	"product_name": "Nile Blue 28",
        	"product_description": "12F",
        	"image_name": "images\/products\/2212B.jpg",
        	"base_price": "890",
        	"material": "20 GA Non-gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212B.jpg"
        }, {
        	"old_id": "108",
        	"product_name": "Moreland 32 Silver",
        	"product_description": "12T",
        	"image_name": "images\/products\/os32s.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_os32s.jpg"
        }, {
        	"old_id": "109",
        	"product_name": "Moreland 36 Silver",
        	"product_description": "12J",
        	"image_name": "images\/products\/os32s.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_os32s.jpg"
        }, {
        	"old_id": "110",
        	"product_name": "Moreland 40",
        	"product_description": "12F",
        	"image_name": "images\/products\/40s.jpg",
        	"base_price": "2420",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_40s.jpg"
        }, {
        	"old_id": "111",
        	"product_name": "Moreland 44",
        	"product_description": "12K",
        	"image_name": "images\/products\/40s.jpg",
        	"base_price": "2520",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_40s.jpg"
        }, {
        	"old_id": "148",
        	"product_name": "Bayside Ebony",
        	"product_description": "12F",
        	"image_name": "images\/products\/2052E.jpg",
        	"base_price": "790",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Ebony Precious Lord Theme",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2052E.jpg"
        }, {
        	"old_id": "149",
        	"product_name": "Bennett Silver",
        	"product_description": "12K",
        	"image_name": "images\/products\/2001s.jpg",
        	"base_price": "840",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver\/Gunmetal",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2001s.jpg"
        }, {
        	"old_id": "151",
        	"product_name": "Birds",
        	"product_description": "12T",
        	"image_name": "images\/products\/2218VB.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue Shaded Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2218VB.jpg"
        }, {
        	"old_id": "153",
        	"product_name": "Butner Bronze",
        	"product_description": "12C",
        	"image_name": "images\/products\/2029S.jpg",
        	"base_price": "940",
        	"material": "20 GA Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2029S.jpg"
        }, {
        	"old_id": "154",
        	"product_name": "Carnation 20ga",
        	"product_description": "12G",
        	"image_name": "images\/products\/2062wp.jpg",
        	"base_price": "850",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2062wp.jpg"
        }, {
        	"old_id": "155",
        	"product_name": "Clarksville",
        	"product_description": "12F",
        	"image_name": "images\/products\/2016shand.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver w\/ Gold Pinstripe",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2016shand.jpg"
        }, {
        	"old_id": "157",
        	"product_name": "Dove ",
        	"product_description": "12T",
        	"image_name": "images\/products\/2016S.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver Shaded Ebony",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2016S.jpg"
        }, {
        	"old_id": "159",
        	"product_name": "Forget Me Not",
        	"product_description": "12G",
        	"image_name": "images\/products\/2026wm.jpg",
        	"base_price": "990",
        	"material": "20 GA Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/pink w\/ Silver Mirrors",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026wm.jpg"
        }, {
        	"old_id": "160",
        	"product_name": "Garland White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/2009wp.jpg",
        	"base_price": "790",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2009wp.jpg"
        }, {
        	"old_id": "163",
        	"product_name": "Heartland Gunmetal",
        	"product_description": "12K",
        	"image_name": "images\/products\/2070GM.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Gunmetal",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070GM.jpg"
        }, {
        	"old_id": "164",
        	"product_name": "Knight",
        	"product_description": "12C",
        	"image_name": "images\/products\/2025e.jpg",
        	"base_price": "990",
        	"material": "20 GA Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Ebony w\/ Silver Mirrors",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2025e.jpg"
        }, {
        	"old_id": "167",
        	"product_name": "Lumber River Pink",
        	"product_description": "",
        	"image_name": "images\/products\/2011WP.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2011WP.jpg"
        }, {
        	"old_id": "168",
        	"product_name": "Rose",
        	"product_description": "12T",
        	"image_name": "images\/products\/2015wp.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2015wp.jpg"
        }, {
        	"old_id": "170",
        	"product_name": "Spring Rose",
        	"product_description": "12K",
        	"image_name": "images\/products\/2016WP.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White Shaded Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2016WP.jpg"
        }, {
        	"old_id": "171",
        	"product_name": "St John ",
        	"product_description": "12T",
        	"image_name": "images\/products\/2016WG.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White Shaded Gold ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2016WG.jpg"
        }, {
        	"old_id": "178",
        	"product_name": "Saybrook Cherry",
        	"product_description": "1260",
        	"image_name": "images\/products\/1257.jpg",
        	"base_price": "910",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Dark Cherry",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1257.jpg"
        }, {
        	"old_id": "181",
        	"product_name": "Corinthian Bronze",
        	"product_description": "12C",
        	"image_name": "images\/products\/1872BZ.jpg",
        	"base_price": "990",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1872BZ.jpg"
        }, {
        	"old_id": "183",
        	"product_name": "Corinthian Neo Blue",
        	"product_description": "12C",
        	"image_name": "images\/products\/1872NB.jpg",
        	"base_price": "990",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1872NB.jpg"
        }, {
        	"old_id": "188",
        	"product_name": "Lexington Blue",
        	"product_description": "12C",
        	"image_name": "images\/products\/1805b.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Velvet",
        	"color_finish": "Powder Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805b.jpg"
        }, {
        	"old_id": "189",
        	"product_name": "Lexington Ebony ",
        	"product_description": "12C",
        	"image_name": "images\/products\/1805es.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Ebony Brushed Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805es.jpg"
        }, {
        	"old_id": "190",
        	"product_name": "Lexington Red\/Gold",
        	"product_description": "12C",
        	"image_name": "images\/products\/1805R.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Red Brushed Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805R.jpg"
        }, {
        	"old_id": "191",
        	"product_name": "Lexington Silver",
        	"product_description": "12C",
        	"image_name": "images\/products\/1805sa.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Silver Brushed Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805sa.jpg"
        }, {
        	"old_id": "192",
        	"product_name": "Anchor Orchid",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020o.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020o.jpg"
        }, {
        	"old_id": "193",
        	"product_name": "Anchor Blue",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020b.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020b.jpg"
        }, {
        	"old_id": "194",
        	"product_name": "Anchor Bronze",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020bz.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020bz.jpg"
        }, {
        	"old_id": "195",
        	"product_name": "Anchor White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/2020wg.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020wg.jpg"
        }, {
        	"old_id": "196",
        	"product_name": "Anchor Pink",
        	"product_description": "",
        	"image_name": "images\/products\/2020p.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": " Pink Crepe",
        	"color_finish": "Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020p.jpg"
        }, {
        	"old_id": "197",
        	"product_name": "Anchor Neo Blue",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020nb.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020nb.jpg"
        }, {
        	"old_id": "198",
        	"product_name": "Anchor Silver",
        	"product_description": "",
        	"image_name": "images\/products\/2020s.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020s.jpg"
        }, {
        	"old_id": "199",
        	"product_name": "Anchor Ebony",
        	"product_description": "",
        	"image_name": "images\/products\/2020e.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020e.jpg"
        }, {
        	"old_id": "200",
        	"product_name": "Anchor Blue\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/2020BG.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": " White Crepe",
        	"color_finish": "Blue\/Gold",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020BG.jpg"
        }, {
        	"old_id": "201",
        	"product_name": "Anchor Forest Green",
        	"product_description": "",
        	"image_name": "images\/products\/2020FG.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": " Crepe",
        	"color_finish": "Forest Green",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020FG.jpg"
        }, {
        	"old_id": "202",
        	"product_name": "Anchor White",
        	"product_description": "",
        	"image_name": "images\/products\/2020w.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020w.jpg"
        }, {
        	"old_id": "203",
        	"product_name": "Anchor Gold",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020g.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": " Rosetan Crepe",
        	"color_finish": "Gold",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020g.jpg"
        }, {
        	"old_id": "204",
        	"product_name": "Anchor White\/Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2020wb.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": " Blue Crepe",
        	"color_finish": "White\/Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020wb.jpg"
        }, {
        	"old_id": "205",
        	"product_name": "Anchor Gunmetal",
        	"product_description": "12V",
        	"image_name": "images\/products\/2020GM.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Gunmetal",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020GM.jpg"
        }, {
        	"old_id": "206",
        	"product_name": "Anchor Red",
        	"product_description": "",
        	"image_name": "images\/products\/2020r.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Red",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020r.jpg"
        }, {
        	"old_id": "207",
        	"product_name": "Anchor White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/2020wp.jpg",
        	"base_price": "610",
        	"material": "20 GA Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020wp.jpg"
        }, {
        	"old_id": "208",
        	"product_name": "Edenton Blue",
        	"product_description": "12J",
        	"image_name": "images\/products\/2026b.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026b.jpg"
        }, {
        	"old_id": "210",
        	"product_name": "Edenton White\/Gold",
        	"product_description": "12J",
        	"image_name": "images\/products\/2026wg.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026wg.jpg"
        }, {
        	"old_id": "212",
        	"product_name": "Edenton Silver",
        	"product_description": "12J",
        	"image_name": "images\/products\/2026s.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026s.jpg"
        }, {
        	"old_id": "213",
        	"product_name": "Edenton Orchid",
        	"product_description": "12J",
        	"image_name": "images\/products\/2026o.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Orchid Crepe",
        	"color_finish": "Orchid",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026o.jpg"
        }, {
        	"old_id": "219",
        	"product_name": "Treemont Silver",
        	"product_description": "12C",
        	"image_name": "images\/products\/2008S.jpg",
        	"base_price": "670",
        	"material": "20 Gauge Casketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2008S.jpg"
        }, {
        	"old_id": "220",
        	"product_name": "Treemont Copper",
        	"product_description": "",
        	"image_name": "images\/products\/2008C.jpg",
        	"base_price": "670",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Copper\/Ebony",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2008C.jpg"
        }, {
        	"old_id": "221",
        	"product_name": "Treemont Blue",
        	"product_description": "12C",
        	"image_name": "images\/products\/2008PB.jpg",
        	"base_price": "670",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/SIlver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2008PB.jpg"
        }, {
        	"old_id": "222",
        	"product_name": "Treemont White\/Pink ",
        	"product_description": "12C",
        	"image_name": "images\/products\/commingsoon.jpg",
        	"base_price": "740",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_commingsoon.jpg"
        }, {
        	"old_id": "223",
        	"product_name": "Varina Blue",
        	"product_description": "12F",
        	"image_name": "images\/products\/2002b.jpg",
        	"base_price": "840",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue Sh. SIlver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2002b.jpg"
        }, {
        	"old_id": "225",
        	"product_name": "Varina Gunmetal",
        	"product_description": "12F",
        	"image_name": "images\/products\/2002LGM.jpg",
        	"base_price": "840",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Gunmetal Shaded Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2002LGM.jpg"
        }, {
        	"old_id": "227",
        	"product_name": "Atlantic Bronze",
        	"product_description": "2201bz",
        	"image_name": "images\/products\/2201bz.jpg",
        	"base_price": "420",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze Shaded Copper",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2201bz.jpg"
        }, {
        	"old_id": "228",
        	"product_name": "Atlantic Gunmetal",
        	"product_description": "2201lgm",
        	"image_name": "images\/products\/2201GM.jpg",
        	"base_price": "420",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Gunmetal shaded Silver",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2201GM.jpg"
        }, {
        	"old_id": "229",
        	"product_name": "Atlantic Silver",
        	"product_description": "12C",
        	"image_name": "images\/products\/2201s.jpg",
        	"base_price": "420",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver sh. Gunmental",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2201s.jpg"
        }, {
        	"old_id": "232",
        	"product_name": "Bentley Blue",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202B.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202B.jpg"
        }, {
        	"old_id": "233",
        	"product_name": "Bentley Copper",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202C.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Copper",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202C.jpg"
        }, {
        	"old_id": "234",
        	"product_name": "Bentley Silver",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202S.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202S.jpg"
        }, {
        	"old_id": "235",
        	"product_name": "Bentley White\/Gold",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202wg.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202wg.jpg"
        }, {
        	"old_id": "236",
        	"product_name": "Bentley White\/Pink",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202wp.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202wp.jpg"
        }, {
        	"old_id": "237",
        	"product_name": "Bentley Ebony",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202E.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Ebony",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202E.jpg"
        }, {
        	"old_id": "238",
        	"product_name": "Bentley White",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202w.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "White",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202w.jpg"
        }, {
        	"old_id": "239",
        	"product_name": "Bentley Gold",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202G.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "Gold",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202G.jpg"
        }, {
        	"old_id": "240",
        	"product_name": "Bentley Neo Blue",
        	"product_description": "12S",
        	"image_name": "images\/products\/2202nb.jpg",
        	"base_price": "450",
        	"material": "20GA Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Crepe",
        	"color_finish": "N.Blue",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202nb.jpg"
        }, {
        	"old_id": "241",
        	"product_name": "Bible White",
        	"product_description": "",
        	"image_name": "images\/products\/2214w.jpg",
        	"base_price": "800",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2214w.jpg"
        }, {
        	"old_id": "280",
        	"product_name": "Atlantic Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2201b.jpg",
        	"base_price": "420",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2201b.jpg"
        }, {
        	"old_id": "281",
        	"product_name": "Lumber River Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2211wb.jpg",
        	"base_price": "660",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "White\/Blue",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2211wb.jpg"
        }, {
        	"old_id": "282",
        	"product_name": "Cross Ebony",
        	"product_description": "",
        	"image_name": "images\/products\/2209eg.jpg",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Ebony\/Gold",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2209eg.jpg"
        }, {
        	"old_id": "283",
        	"product_name": "Cross White",
        	"product_description": "",
        	"image_name": "images\/products\/2209WG.jpg",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2209WG.jpg"
        }, {
        	"old_id": "284",
        	"product_name": "Pace Copper",
        	"product_description": "",
        	"image_name": "images\/products\/2206c.jpg",
        	"base_price": "550",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Copper\/Ebony",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2206c.jpg"
        }, {
        	"old_id": "285",
        	"product_name": "Pontiff Ebony Mirror",
        	"product_description": "",
        	"image_name": "images\/products\/2223 EM.jpg",
        	"base_price": "640",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "IVory Crepe",
        	"color_finish": "Ebony\/Silver Mirror",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2223 EM.jpg"
        }, {
        	"old_id": "286",
        	"product_name": "Pontiff White ",
        	"product_description": "",
        	"image_name": "images\/products\/2223wm.jpg",
        	"base_price": "640",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White\/Gold Mirror",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2223wm.jpg"
        }, {
        	"old_id": "287",
        	"product_name": "Morgan Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2210nbx.jpg",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue\/Silver",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2210nbx.jpg"
        }, {
        	"old_id": "288",
        	"product_name": "Morgan White Pink",
        	"product_description": "",
        	"image_name": "images\/products\/2210wp.jpg",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2210wp.jpg"
        }, {
        	"old_id": "289",
        	"product_name": "Bentley Gunmetal ",
        	"product_description": "",
        	"image_name": "images\/products\/2202GM.jpg",
        	"base_price": "450",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Gunmetal ",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2202GM.jpg"
        }, {
        	"old_id": "291",
        	"product_name": "Saybrook Cherry",
        	"product_description": "",
        	"image_name": "images\/products\/1257.jpg",
        	"base_price": "900",
        	"material": "Paper Verneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Dark Cherry",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1257.jpg"
        }, {
        	"old_id": "294",
        	"product_name": "Edenton Gunmetal",
        	"product_description": "",
        	"image_name": "images\/products\/2026gm.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Gunmetal SIlver Pinstripes",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026gm.jpg"
        }, {
        	"old_id": "296",
        	"product_name": "Bayside White",
        	"product_description": "",
        	"image_name": "images\/products\/2052wg.jpg",
        	"base_price": "790",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White Beloved Mother Script",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2052wg.jpg"
        }, {
        	"old_id": "297",
        	"product_name": "Bayside Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2052vb.jpg",
        	"base_price": "790",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue Going Home Theme",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2052vb.jpg"
        }, {
        	"old_id": "298",
        	"product_name": "Bennett Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/2001BZ.jpg",
        	"base_price": "840",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2001BZ.jpg"
        }, {
        	"old_id": "299",
        	"product_name": "Lumber River Blue ",
        	"product_description": "",
        	"image_name": "images\/products\/2011wb.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "White\/Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2011wb.jpg"
        }, {
        	"old_id": "300",
        	"product_name": "Heartland Almond",
        	"product_description": "",
        	"image_name": "images\/products\/2070A.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Almond",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070A.jpg"
        }, {
        	"old_id": "301",
        	"product_name": "Heartland Orchid",
        	"product_description": "",
        	"image_name": "images\/products\/2070o.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070o.jpg"
        }, {
        	"old_id": "302",
        	"product_name": "Heartland Neo Blue ",
        	"product_description": "",
        	"image_name": "images\/products\/2070nb.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070nb.jpg"
        }, {
        	"old_id": "303",
        	"product_name": "Heartland Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2070B.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070B.jpg"
        }, {
        	"old_id": "304",
        	"product_name": "Heartland White",
        	"product_description": "",
        	"image_name": "images\/products\/2070WA.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070WA.jpg"
        }, {
        	"old_id": "305",
        	"product_name": "Heartland Silver",
        	"product_description": "",
        	"image_name": "images\/products\/2070sa.jpg",
        	"base_price": "770",
        	"material": "",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2070sa.jpg"
        }, {
        	"old_id": "306",
        	"product_name": "Moreland 32 Blue",
        	"product_description": "",
        	"image_name": "images\/products\/32b.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32b.jpg"
        }, {
        	"old_id": "308",
        	"product_name": "Moreland 32 Viking Blue",
        	"product_description": "",
        	"image_name": "images\/products\/32vb.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32vb.jpg"
        }, {
        	"old_id": "309",
        	"product_name": "Moreland 32 White\/PInk",
        	"product_description": "",
        	"image_name": "images\/products\/32wp.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Interior",
        	"color_finish": "White\/Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32wp.jpg"
        }, {
        	"old_id": "310",
        	"product_name": "Moreland 32 White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/32wg.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32wg.jpg"
        }, {
        	"old_id": "311",
        	"product_name": "Moreland 32 Bronze ",
        	"product_description": "",
        	"image_name": "images\/products\/os32bz.jpg",
        	"base_price": "1570",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_os32bz.jpg"
        }, {
        	"old_id": "312",
        	"product_name": "Moreland 36 Blue",
        	"product_description": "",
        	"image_name": "images\/products\/32b.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32b.jpg"
        }, {
        	"old_id": "313",
        	"product_name": "Moreland 36 Bronze ",
        	"product_description": "",
        	"image_name": "images\/products\/os36bz.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_os36bz.jpg"
        }, {
        	"old_id": "314",
        	"product_name": "Moreland 36 Viking Blue",
        	"product_description": "",
        	"image_name": "images\/products\/35vb.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_35vb.jpg"
        }, {
        	"old_id": "315",
        	"product_name": "Moreland 36 White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/32wg.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_32wg.jpg"
        }, {
        	"old_id": "316",
        	"product_name": "Moreland 36 White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/35wp.jpg",
        	"base_price": "1620",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Interior",
        	"color_finish": "White\/Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_35wp.jpg"
        }, {
        	"old_id": "317",
        	"product_name": "Marvel Poplar 27",
        	"product_description": "",
        	"image_name": "images\/products\/1430-31.jpg",
        	"base_price": "1740",
        	"material": "Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1430-31.jpg"
        }, {
        	"old_id": "318",
        	"product_name": "Marvel Poplar 31",
        	"product_description": "",
        	"image_name": "images\/products\/1430-31.jpg",
        	"base_price": "1850",
        	"material": "Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1430-31.jpg"
        }, {
        	"old_id": "319",
        	"product_name": "Marvel Poplar 31",
        	"product_description": "",
        	"image_name": "images\/products\/1430-31.jpg",
        	"base_price": "1840",
        	"material": "Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1430-31.jpg"
        }, {
        	"old_id": "320",
        	"product_name": "Alexander Calvary Bronze 29",
        	"product_description": "",
        	"image_name": "images\/products\/1828bzg.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828bzg.jpg"
        }, {
        	"old_id": "321",
        	"product_name": "Alexander Hatteras 29",
        	"product_description": "",
        	"image_name": "images\/products\/1828vb.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828vb.jpg"
        }, {
        	"old_id": "323",
        	"product_name": "Alexander Faithful Ebony 29",
        	"product_description": "",
        	"image_name": "images\/products\/1828e.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828e.jpg"
        }, {
        	"old_id": "324",
        	"product_name": "Alexander Catawba Blue 29",
        	"product_description": "",
        	"image_name": "images\/products\/1828b.jpg",
        	"base_price": "1340",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue Sh. Viking Blue",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1828b.jpg"
        }, {
        	"old_id": "325",
        	"product_name": "Carnation Plus 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004car.jpg",
        	"base_price": "1230",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Interior",
        	"color_finish": "White\/Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004car.jpg"
        }, {
        	"old_id": "326",
        	"product_name": "Homeward Viking Blue 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004VB.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004VB.jpg"
        }, {
        	"old_id": "327",
        	"product_name": "Emperor Blue 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004B.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004B.jpg"
        }, {
        	"old_id": "328",
        	"product_name": "Emperor Neo Blue 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004NB.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004NB.jpg"
        }, {
        	"old_id": "329",
        	"product_name": "Emperor Ebony 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004e.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004e.jpg"
        }, {
        	"old_id": "330",
        	"product_name": "Emperor White\/Blue 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004wb.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "White\/Blue",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004wb.jpg"
        }, {
        	"old_id": "331",
        	"product_name": "Emperor Bronze 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004BZ.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004BZ.jpg"
        }, {
        	"old_id": "332",
        	"product_name": "Emperor Gunmetal 28",
        	"product_description": "",
        	"image_name": "images\/products\/2004GM.jpg",
        	"base_price": "990",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Gunmetal\/Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2004GM.jpg"
        }, {
        	"old_id": "333",
        	"product_name": "Nile Ebony 28",
        	"product_description": "",
        	"image_name": "images\/products\/2212e.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212e.jpg"
        }, {
        	"old_id": "334",
        	"product_name": "Nile Bronze 28",
        	"product_description": "",
        	"image_name": "images\/products\/2212BZ.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212BZ.jpg"
        }, {
        	"old_id": "335",
        	"product_name": "Nile Silver 28",
        	"product_description": "",
        	"image_name": "images\/products\/2212S.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212S.jpg"
        }, {
        	"old_id": "336",
        	"product_name": "Nile White\/Pink 28",
        	"product_description": "",
        	"image_name": "images\/products\/2212WP.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Interior",
        	"color_finish": "White\/Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212WP.jpg"
        }, {
        	"old_id": "337",
        	"product_name": "Nile White\/Gold 28",
        	"product_description": "",
        	"image_name": "images\/products\/2212WG.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2212WG.jpg"
        }, {
        	"old_id": "338",
        	"product_name": "Homestead Cherry 28",
        	"product_description": "",
        	"image_name": "images\/products\/Homestead28_cherry.jpg",
        	"base_price": "890",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Cherry ",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_Homestead28_cherry.jpg"
        }, {
        	"old_id": "339",
        	"product_name": "Galaxy 29",
        	"product_description": "",
        	"image_name": "images\/products\/1232.jpg",
        	"base_price": "880",
        	"material": "Cloth Covered Wood ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Gray Cloth",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1232.jpg"
        }, {
        	"old_id": "341",
        	"product_name": "Lexington Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/1805nb.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Velvet",
        	"color_finish": "Neo Blue Brushed Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805nb.jpg"
        }, {
        	"old_id": "342",
        	"product_name": "Hero Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/1840NB.jpg",
        	"base_price": "1450",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue or White Velvet",
        	"color_finish": "Neo Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1840NB.jpg"
        }, {
        	"old_id": "344",
        	"product_name": "Calvary Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/2082BZ.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2082BZ.jpg"
        }, {
        	"old_id": "345",
        	"product_name": "Catawba Blue",
        	"product_description": "",
        	"image_name": "images\/products\/1882B.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue Sh. Viking Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1882B.jpg"
        }, {
        	"old_id": "346",
        	"product_name": "Corinthian White ",
        	"product_description": "",
        	"image_name": "images\/products\/1872W.jpg",
        	"base_price": "990",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1872W.jpg"
        }, {
        	"old_id": "347",
        	"product_name": "Fleming Forest Green ",
        	"product_description": "",
        	"image_name": "images\/products\/1808FG.jpg",
        	"base_price": "1210",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Forest Green Brushed Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1808FG.jpg"
        }, {
        	"old_id": "350",
        	"product_name": "Country Side",
        	"product_description": "",
        	"image_name": "images\/products\/1847BZ.jpg",
        	"base_price": "1530",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze Brushed ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1847BZ.jpg"
        }, {
        	"old_id": "352",
        	"product_name": "Neuse Gunmetal",
        	"product_description": "",
        	"image_name": "images\/products\/1820gm.jpg",
        	"base_price": "840",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Gunmetal\/Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1820gm.jpg"
        }, {
        	"old_id": "353",
        	"product_name": "Ann",
        	"product_description": "",
        	"image_name": "images\/products\/1806BY.jpg",
        	"base_price": "1330",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Burgandy",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1806BY.jpg"
        }, {
        	"old_id": "354",
        	"product_name": "Wrightsville",
        	"product_description": "",
        	"image_name": "images\/products\/1833ws.jpg",
        	"base_price": "1290",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "White Sand",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1833ws.jpg"
        }, {
        	"old_id": "355",
        	"product_name": "Faithful Ebony",
        	"product_description": "",
        	"image_name": "images\/products\/2080E.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2080E.jpg"
        }, {
        	"old_id": "357",
        	"product_name": "Winfield",
        	"product_description": "",
        	"image_name": "images\/products\/1427.jpg",
        	"base_price": "1020",
        	"material": "Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Walnut",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1427.jpg"
        }, {
        	"old_id": "358",
        	"product_name": "Magnolia Eyelet",
        	"product_description": "",
        	"image_name": "images\/products\/1454eyeletII.jpg",
        	"base_price": "1920",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "IVory Eyelet",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1454eyeletII.jpg"
        }, {
        	"old_id": "359",
        	"product_name": "Oak Grove",
        	"product_description": "",
        	"image_name": "images\/products\/1429.jpg",
        	"base_price": "1470",
        	"material": "Oak",
        	"old_color_finish": null,
        	"interior": "Beige Basketweave",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1429.jpg"
        }, {
        	"old_id": "361",
        	"product_name": "Child 3-6 White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/3-6wp.jpg",
        	"base_price": "800",
        	"material": "",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_3-6wp.jpg"
        }, {
        	"old_id": "362",
        	"product_name": "Child 3-6 Blue",
        	"product_description": "",
        	"image_name": "images\/products\/3-6b.jpg",
        	"base_price": "800",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_3-6b.jpg"
        }, {
        	"old_id": "363",
        	"product_name": "Child 4-6 Blue ",
        	"product_description": "",
        	"image_name": "images\/products\/4-6b.jpg",
        	"base_price": "830",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_4-6b.jpg"
        }, {
        	"old_id": "364",
        	"product_name": "Child 4-6 White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/c46wg.jpg",
        	"base_price": "830",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_c46wg.jpg"
        }, {
        	"old_id": "365",
        	"product_name": "Child 4-6 White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/4-6wp.jpg",
        	"base_price": "830",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_4-6wp.jpg"
        }, {
        	"old_id": "366",
        	"product_name": "Child 5-6 Blue ",
        	"product_description": "",
        	"image_name": "images\/products\/5-6b.jpg",
        	"base_price": "860",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_5-6b.jpg"
        }, {
        	"old_id": "367",
        	"product_name": "Child 5-6 White\/Pink",
        	"product_description": "",
        	"image_name": "images\/products\/5-6wp.jpg",
        	"base_price": "860",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_5-6wp.jpg"
        }, {
        	"old_id": "368",
        	"product_name": "Child 5-6 White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/c46wg.jpg",
        	"base_price": "860",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_c46wg.jpg"
        }, {
        	"old_id": "373",
        	"product_name": "Child 2-6 White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/commingsoon.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "8",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_commingsoon.jpg"
        }, {
        	"old_id": "374",
        	"product_name": "Gardenia Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2048wb.jpg",
        	"base_price": "850",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "White\/Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2048wb.jpg"
        }, {
        	"old_id": "375",
        	"product_name": "Garland Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/2009WPFC.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2009WPFC.jpg"
        }, {
        	"old_id": "376",
        	"product_name": "Calvary Blue Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/2082BBFC.jpg",
        	"base_price": "1000",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Viking Blue Bl. Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2082BBFC.jpg"
        }, {
        	"old_id": "377",
        	"product_name": "Oregon Cherry Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/1437FC.png",
        	"base_price": "2320",
        	"material": "Solid Cherry ",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1437FC.png"
        }, {
        	"old_id": "379",
        	"product_name": "Primrose Stainless",
        	"product_description": "",
        	"image_name": "images\/products\/1504wp.jpg",
        	"base_price": "1600",
        	"material": "Stainless Steel",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1504wp.jpg"
        }, {
        	"old_id": "380",
        	"product_name": "Regency Pewter",
        	"product_description": "",
        	"image_name": "images\/products\/1504gm.jpg",
        	"base_price": "1660",
        	"material": "Stainless Steel",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Pewter",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1504gm.jpg"
        }, {
        	"old_id": "381",
        	"product_name": "Belhaven Silver Rose",
        	"product_description": "",
        	"image_name": "images\/products\/1815O.jpg",
        	"base_price": "1920",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Silver Rose",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1815O.jpg"
        }, {
        	"old_id": "382",
        	"product_name": "Tribune Silver",
        	"product_description": "",
        	"image_name": "images\/products\/1505s.jpg",
        	"base_price": "1720",
        	"material": "Stainless Steel",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Silver Brushed Gunmetals",
        	"old_category_code": "2",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1505s.jpg"
        }, {
        	"old_id": "385",
        	"product_name": "Rental Oak ",
        	"product_description": "",
        	"image_name": "images\/products\/1298.jpg",
        	"base_price": "2000",
        	"material": "***SPECIAL ORDER***",
        	"old_color_finish": null,
        	"interior": "Removeable Insert",
        	"color_finish": "Solid Oak ",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1298.jpg"
        }, {
        	"old_id": "386",
        	"product_name": "Pearl Rose",
        	"product_description": "",
        	"image_name": "images\/products\/1804P.jpg",
        	"base_price": "930",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Interior",
        	"color_finish": "Pink ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1804P.jpg"
        }, {
        	"old_id": "387",
        	"product_name": "Spartan Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2040b.jpg",
        	"base_price": "710",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2040b.jpg"
        }, {
        	"old_id": "388",
        	"product_name": "Spartan Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/2040bz.jpg",
        	"base_price": "710",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2040bz.jpg"
        }, {
        	"old_id": "389",
        	"product_name": "Spartan White",
        	"product_description": "",
        	"image_name": "images\/products\/2040wg.jpg",
        	"base_price": "710",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White\/Gold",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2040wg.jpg"
        }, {
        	"old_id": "390",
        	"product_name": "Spartan Ebony",
        	"product_description": "",
        	"image_name": "images\/products\/2040e.jpg",
        	"base_price": "710",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Ebony\/Silver",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2040e.jpg"
        }, {
        	"old_id": "391",
        	"product_name": "Sportsman",
        	"product_description": "",
        	"image_name": "images\/products\/5t18SPORTSMAN.jpg",
        	"base_price": "2040",
        	"material": "***Special Order***",
        	"old_color_finish": null,
        	"interior": "Realtree Camouflage",
        	"color_finish": "Woodgrain Finish",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_5t18SPORTSMAN.jpg"
        }, {
        	"old_id": "392",
        	"product_name": "Carlisle Poplar",
        	"product_description": "",
        	"image_name": "images\/products\/1438.png",
        	"base_price": "1560",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1438.png"
        }, {
        	"old_id": "393",
        	"product_name": "Dare",
        	"product_description": "",
        	"image_name": "images\/products\/1810-O.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid brushed Natural",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1810-O.jpg"
        }, {
        	"old_id": "395",
        	"product_name": "Faithfull Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/TC2080WFCFAITHFULFULLCOUCH.jpg",
        	"base_price": "1000",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_TC2080WFCFAITHFULFULLCOUCH.jpg"
        }, {
        	"old_id": "396",
        	"product_name": "Spring Rose",
        	"product_description": "",
        	"image_name": "images\/products\/2016A.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Almond",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2016A.jpg"
        }, {
        	"old_id": "397",
        	"product_name": "Oakmont",
        	"product_description": "",
        	"image_name": "images\/products\/1252.jpg",
        	"base_price": "640",
        	"material": "Laminated Wood",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Oak Laminate",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1252.jpg"
        }, {
        	"old_id": "398",
        	"product_name": "Rosemont",
        	"product_description": "",
        	"image_name": "images\/products\/1251a.jpg",
        	"base_price": "650",
        	"material": "Laminated Wood",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Cherry Laminate",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1251a.jpg"
        }, {
        	"old_id": "405",
        	"product_name": "Cottage Rose",
        	"product_description": "",
        	"image_name": "images\/products\/2082cr.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Silver\/Wineberry",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2082cr.jpg"
        }, {
        	"old_id": "406",
        	"product_name": "Homestead Oak 28",
        	"product_description": "",
        	"image_name": "images\/products\/1228oak.jpg",
        	"base_price": "890",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan ",
        	"color_finish": "Oak ",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1228oak.jpg"
        }, {
        	"old_id": "407",
        	"product_name": "Homestead Oak 28",
        	"product_description": "",
        	"image_name": "images\/products\/1228oak.jpg",
        	"base_price": "890",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Oak ",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1228oak.jpg"
        }, {
        	"old_id": "408",
        	"product_name": "Homestead Cherry 28",
        	"product_description": "",
        	"image_name": "images\/products\/Homestead28_cherry.jpg",
        	"base_price": "890",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Cherry",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_Homestead28_cherry.jpg"
        }, {
        	"old_id": "409",
        	"product_name": "Marvel Poplar 27",
        	"product_description": "",
        	"image_name": "images\/products\/1430-31.jpg",
        	"base_price": "1750",
        	"material": "Solid polar",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Dark Cherry",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1430-31.jpg"
        }, {
        	"old_id": "411",
        	"product_name": "Marvel Oak 27",
        	"product_description": "",
        	"image_name": "images\/products\/marveloak.jpg",
        	"base_price": "1750",
        	"material": "Solid Oak",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_marveloak.jpg"
        }, {
        	"old_id": "412",
        	"product_name": "Marvel Oak 31",
        	"product_description": "",
        	"image_name": "images\/products\/oak.jpg",
        	"base_price": "1850",
        	"material": "Solid Oak",
        	"old_color_finish": null,
        	"interior": "Rosetan ",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_oak.jpg"
        }, {
        	"old_id": "413",
        	"product_name": "Gardenia Orchid",
        	"product_description": "",
        	"image_name": "images\/products\/2048wo.jpg",
        	"base_price": "850",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink ",
        	"color_finish": "Orchid\/White",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2048wo.jpg"
        }, {
        	"old_id": "414",
        	"product_name": "Lexington Glass Silver Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/1805sglass.jpg",
        	"base_price": "2990",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Silver Brushed Natural",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805sglass.jpg"
        }, {
        	"old_id": "431",
        	"product_name": "Wallace Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/2085sefc.jpg",
        	"base_price": "1000",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Silver\/Ebony",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2085sefc.jpg"
        }, {
        	"old_id": "433",
        	"product_name": "Marvel Oak 27",
        	"product_description": "",
        	"image_name": "images\/products\/marveloak.jpg",
        	"base_price": "1740",
        	"material": "Oak",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_marveloak.jpg"
        }, {
        	"old_id": "434",
        	"product_name": "Marvel Oak 31",
        	"product_description": "",
        	"image_name": "images\/products\/marveloak.jpg",
        	"base_price": "1840",
        	"material": "Oak",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_marveloak.jpg"
        }, {
        	"old_id": "435",
        	"product_name": "Melrose Burgandy",
        	"product_description": "",
        	"image_name": "images\/products\/1890BY.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Burgandy",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1890BY.jpg"
        }, {
        	"old_id": "436",
        	"product_name": "Melrose Blue",
        	"product_description": "",
        	"image_name": "images\/products\/1890B.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1890B.jpg"
        }, {
        	"old_id": "437",
        	"product_name": "Melrose White",
        	"product_description": "",
        	"image_name": "images\/products\/1890Wg.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "White ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1890Wg.jpg"
        }, {
        	"old_id": "438",
        	"product_name": "Melrose Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/1890bz.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze\/Copper",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1890bz.jpg"
        }, {
        	"old_id": "440",
        	"product_name": "Melrose Silver",
        	"product_description": "",
        	"image_name": "images\/products\/1890S.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1890S.jpg"
        }, {
        	"old_id": "441",
        	"product_name": "Anchor Purple",
        	"product_description": "",
        	"image_name": "images\/products\/2020pr.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Purple",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020pr.jpg"
        }, {
        	"old_id": "442",
        	"product_name": "Lincoln Poplar",
        	"product_description": "",
        	"image_name": "images\/products\/1451LINCOLN.jpg",
        	"base_price": "1820",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1451LINCOLN.jpg"
        }, {
        	"old_id": "443",
        	"product_name": "Titan",
        	"product_description": "",
        	"image_name": "images\/products\/1837gm.jpg",
        	"base_price": "1220",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Silver Crepe",
        	"color_finish": "Gunmetal\/Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1837gm.jpg"
        }, {
        	"old_id": "444",
        	"product_name": "Sportsman II",
        	"product_description": "",
        	"image_name": "images\/products\/1838.jpg",
        	"base_price": "1220",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Realtree Camo",
        	"color_finish": "Hunter Green",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1838.jpg"
        }, {
        	"old_id": "446",
        	"product_name": "Rustic Oak ",
        	"product_description": "",
        	"image_name": "images\/products\/1407.jpg",
        	"base_price": "1600",
        	"material": "Solid Oak",
        	"old_color_finish": null,
        	"interior": "Rosetan ",
        	"color_finish": "Satin",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1407.jpg"
        }, {
        	"old_id": "448",
        	"product_name": "Veteran ",
        	"product_description": "",
        	"image_name": "images\/products\/1881S.jpg",
        	"base_price": "900",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "SIlver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1881S.jpg"
        }, {
        	"old_id": "450",
        	"product_name": "Country Pine",
        	"product_description": "",
        	"image_name": "images\/products\/1419.jpg",
        	"base_price": "1470",
        	"material": "Solid Pine",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1419.jpg"
        }, {
        	"old_id": "451",
        	"product_name": "Huntsman",
        	"product_description": "",
        	"image_name": "images\/products\/1448x.jpg",
        	"base_price": "1980",
        	"material": "Solid Pecan",
        	"old_color_finish": null,
        	"interior": "Camo Interior",
        	"color_finish": "Satin",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1448x.jpg"
        }, {
        	"old_id": "452",
        	"product_name": "Franklin ",
        	"product_description": "",
        	"image_name": "images\/products\/1459.jpg",
        	"base_price": "1560",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Crepe",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1459.jpg"
        }, {
        	"old_id": "453",
        	"product_name": "Cameo Poplar",
        	"product_description": "",
        	"image_name": "images\/products\/1450CAMEO.jpg",
        	"base_price": "1770",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1450CAMEO.jpg"
        }, {
        	"old_id": "455",
        	"product_name": "Person",
        	"product_description": "",
        	"image_name": "images\/products\/1887bb.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Blue\/Viking Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1887bb.jpg"
        }, {
        	"old_id": "457",
        	"product_name": "Carnation Purple",
        	"product_description": "",
        	"image_name": "images\/products\/2062pr.jpg",
        	"base_price": "850",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Purple",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2062pr.jpg"
        }, {
        	"old_id": "458",
        	"product_name": "Anchor Orange",
        	"product_description": "",
        	"image_name": "images\/products\/2020Or.jpg",
        	"base_price": "610",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Orange",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2020Or.jpg"
        }, {
        	"old_id": "459",
        	"product_name": "Lee ",
        	"product_description": "",
        	"image_name": "images\/products\/1402.jpg",
        	"base_price": "1980",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Cherry Satin",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1402.jpg"
        }, {
        	"old_id": "460",
        	"product_name": "Statesman Carved Top",
        	"product_description": "",
        	"image_name": "images\/products\/1400.png",
        	"base_price": "6020",
        	"material": "Solid Mahogany",
        	"old_color_finish": null,
        	"interior": "Ivory Velvet",
        	"color_finish": "Polished Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1400.png"
        }, {
        	"old_id": "461",
        	"product_name": "Crown Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2041vbx.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Neo Blue",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2041vbx.jpg"
        }, {
        	"old_id": "462",
        	"product_name": "Crown White",
        	"product_description": "",
        	"image_name": "images\/products\/2041wg.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2041wg.jpg"
        }, {
        	"old_id": "463",
        	"product_name": "Crown Burgandy",
        	"product_description": "",
        	"image_name": "images\/products\/2041wn.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Burgandy",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2041wn.jpg"
        }, {
        	"old_id": "464",
        	"product_name": "Crown Gunmetal ",
        	"product_description": "",
        	"image_name": "images\/products\/2041gm.jpg",
        	"base_price": "770",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Gunmetal Metallic",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2041gm.jpg"
        }, {
        	"old_id": "465",
        	"product_name": "Bayside Orchid",
        	"product_description": "",
        	"image_name": "images\/products\/2052o.jpg",
        	"base_price": "790",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchird Beloved Mother",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2052o.jpg"
        }, {
        	"old_id": "466",
        	"product_name": "Lexington Red\/Silver",
        	"product_description": "",
        	"image_name": "images\/products\/1805RS.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Red\/Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805RS.jpg"
        }, {
        	"old_id": "467",
        	"product_name": "Neuse Coral Rose",
        	"product_description": "",
        	"image_name": "images\/products\/1820cr.jpg",
        	"base_price": "840",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Coral Rose\/Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1820cr.jpg"
        }, {
        	"old_id": "468",
        	"product_name": "Hamilton Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/1832bz.jpg",
        	"base_price": "1240",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze Brushed",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1832bz.jpg"
        }, {
        	"old_id": "470",
        	"product_name": "Harrison",
        	"product_description": "",
        	"image_name": "images\/products\/1438.jpg",
        	"base_price": "1560",
        	"material": "Solid Poplar",
        	"old_color_finish": null,
        	"interior": "Beige Basketweave",
        	"color_finish": "Saitn Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1438.jpg"
        }, {
        	"old_id": "471",
        	"product_name": "Diamond ",
        	"product_description": "",
        	"image_name": "images\/products\/2043pr.jpg",
        	"base_price": "930",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Dark Lilac",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2043pr.jpg"
        }, {
        	"old_id": "472",
        	"product_name": "Three Rose ",
        	"product_description": "",
        	"image_name": "images\/products\/3rosepink.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_3rosepink.jpg"
        }, {
        	"old_id": "473",
        	"product_name": "Blessing",
        	"product_description": "",
        	"image_name": "images\/products\/BlessingWS.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_BlessingWS.jpg"
        }, {
        	"old_id": "474",
        	"product_name": "Dove ",
        	"product_description": "",
        	"image_name": "images\/products\/DoveBL.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_DoveBL.jpg"
        }, {
        	"old_id": "475",
        	"product_name": "Father ",
        	"product_description": "",
        	"image_name": "images\/products\/FatherWS.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_FatherWS.jpg"
        }, {
        	"old_id": "476",
        	"product_name": "Flag at Rest",
        	"product_description": "",
        	"image_name": "images\/products\/FLAGatRestWS.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_FLAGatRestWS.jpg"
        }, {
        	"old_id": "478",
        	"product_name": "Going Home ",
        	"product_description": "",
        	"image_name": "images\/products\/GoingHomeWG.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_GoingHomeWG.jpg"
        }, {
        	"old_id": "481",
        	"product_name": "In Gods Care",
        	"product_description": "",
        	"image_name": "images\/products\/InGodsCarBl.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_InGodsCarBl.jpg"
        }, {
        	"old_id": "487",
        	"product_name": "Bible John 3:16",
        	"product_description": "",
        	"image_name": "images\/products\/John316wg.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_John316wg.jpg"
        }, {
        	"old_id": "488",
        	"product_name": "Mother\/Grandmother Rose",
        	"product_description": "",
        	"image_name": "images\/products\/mothergrandmotherrosewh.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_mothergrandmotherrosewh.jpg"
        }, {
        	"old_id": "489",
        	"product_name": "Mother ",
        	"product_description": "",
        	"image_name": "images\/products\/MOtherPK.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_MOtherPK.jpg"
        }, {
        	"old_id": "490",
        	"product_name": "Mother Pink Rose",
        	"product_description": "",
        	"image_name": "images\/products\/motherrosepink.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_motherrosepink.jpg"
        }, {
        	"old_id": "491",
        	"product_name": "Mother Red Rose",
        	"product_description": "",
        	"image_name": "images\/products\/PanelComingSoon.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PanelComingSoon.jpg"
        }, {
        	"old_id": "492",
        	"product_name": "Mother Yellow Rose",
        	"product_description": "",
        	"image_name": "images\/products\/motheryellowrose.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_motheryellowrose.jpg"
        }, {
        	"old_id": "493",
        	"product_name": "Mother Proverbs",
        	"product_description": "",
        	"image_name": "images\/products\/MOtherProverbsWG.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_MOtherProverbsWG.jpg"
        }, {
        	"old_id": "494",
        	"product_name": "US Navy Emblem",
        	"product_description": "",
        	"image_name": "images\/products\/NavyBlue.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_NavyBlue.jpg"
        }, {
        	"old_id": "495",
        	"product_name": "US Army Emblem",
        	"product_description": "",
        	"image_name": "images\/products\/army.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_army.jpg"
        }, {
        	"old_id": "496",
        	"product_name": "US Marine Emblem",
        	"product_description": "",
        	"image_name": "images\/products\/PanelComingSoon.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PanelComingSoon.jpg"
        }, {
        	"old_id": "497",
        	"product_name": "US Air Force Emblem",
        	"product_description": "",
        	"image_name": "images\/products\/PanelComingSoon.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PanelComingSoon.jpg"
        }, {
        	"old_id": "498",
        	"product_name": "Praying Hands",
        	"product_description": "",
        	"image_name": "images\/products\/PrayingHandsPK.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PrayingHandsPK.jpg"
        }, {
        	"old_id": "499",
        	"product_name": "Praying Hands\/Works",
        	"product_description": "",
        	"image_name": "images\/products\/prayinghandsworkswg.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_prayinghandsworkswg.jpg"
        }, {
        	"old_id": "500",
        	"product_name": "Precious Lord Scene",
        	"product_description": "",
        	"image_name": "images\/products\/PreciousLordpink.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PreciousLordpink.jpg"
        }, {
        	"old_id": "501",
        	"product_name": "Ray ",
        	"product_description": "",
        	"image_name": "images\/products\/RayWH.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_RayWH.jpg"
        }, {
        	"old_id": "502",
        	"product_name": "Tractor Red",
        	"product_description": "",
        	"image_name": "images\/products\/RedTractorWS.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_RedTractorWS.jpg"
        }, {
        	"old_id": "503",
        	"product_name": "Works",
        	"product_description": "",
        	"image_name": "images\/products\/WorksBl.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_WorksBl.jpg"
        }, {
        	"old_id": "504",
        	"product_name": "Tractor Green",
        	"product_description": "",
        	"image_name": "images\/products\/greentractor.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_greentractor.jpg"
        }, {
        	"old_id": "505",
        	"product_name": "Window Cross ",
        	"product_description": "",
        	"image_name": "images\/products\/windowcrosspink.jpg",
        	"base_price": "60",
        	"material": "Standard Insert Panel",
        	"old_color_finish": null,
        	"interior": "Crepe Fabric",
        	"color_finish": "Blue, Pink, Rosetan or White",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_windowcrosspink.jpg"
        }, {
        	"old_id": "506",
        	"product_name": "A Sample of what is in stock ",
        	"product_description": "",
        	"image_name": "images\/products\/PanelComingSoon.jpg",
        	"base_price": "0",
        	"material": "",
        	"old_color_finish": null,
        	"interior": "New Panels uploaded weekly",
        	"color_finish": "Call office for other options",
        	"old_category_code": "39",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_PanelComingSoon.jpg"
        }, {
        	"old_id": "507",
        	"product_name": "Edenton Turquoise",
        	"product_description": "",
        	"image_name": "images\/products\/2026t.jpg",
        	"base_price": "890",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Turquoise",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2026t.jpg"
        }, {
        	"old_id": "509",
        	"product_name": "Burgandy Jewel",
        	"product_description": "",
        	"image_name": "images\/products\/1853by.jpg",
        	"base_price": "1210",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Silver Velvet",
        	"color_finish": "Burgandy Brushed",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1853by.jpg"
        }, {
        	"old_id": "510",
        	"product_name": "Ametrine Jewel",
        	"product_description": "",
        	"image_name": "images\/products\/1852o.jpg",
        	"base_price": "1220",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "Orchid Brushed",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1852o.jpg"
        }, {
        	"old_id": "511",
        	"product_name": "Pieta",
        	"product_description": "",
        	"image_name": "images\/products\/1853bz.jpg",
        	"base_price": "1210",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Bronze Brushed",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1853bz.jpg"
        }, {
        	"old_id": "512",
        	"product_name": "Carnation Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/1862wvfc.jpg",
        	"base_price": "1410",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Velvet",
        	"color_finish": "White\/Rose",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1862wvfc.jpg"
        }, {
        	"old_id": "514",
        	"product_name": "Langston Oak",
        	"product_description": "",
        	"image_name": "images\/products\/1258.jpg",
        	"base_price": "900",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Oak",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1258.jpg"
        }, {
        	"old_id": "515",
        	"product_name": "Langston Oak",
        	"product_description": "",
        	"image_name": "images\/products\/1258.jpg",
        	"base_price": "910",
        	"material": "Paper Veneer",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Oak",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1258.jpg"
        }, {
        	"old_id": "516",
        	"product_name": "Military Air Force",
        	"product_description": "",
        	"image_name": "images\/products\/1831AF.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Uniform Blue",
        	"color_finish": "Metalic Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1831AF.jpg"
        }, {
        	"old_id": "517",
        	"product_name": "Military Army ",
        	"product_description": "",
        	"image_name": "images\/products\/1831AR.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Uniform Black",
        	"color_finish": "Black",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1831AR.jpg"
        }, {
        	"old_id": "518",
        	"product_name": "Military Navy",
        	"product_description": "",
        	"image_name": "images\/products\/1831NA.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Uniform Blue",
        	"color_finish": "White",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1831NA.jpg"
        }, {
        	"old_id": "519",
        	"product_name": "Military Marine",
        	"product_description": "",
        	"image_name": "images\/products\/1831MA.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Uniform Black",
        	"color_finish": "Black",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1831MA.jpg"
        }, {
        	"old_id": "520",
        	"product_name": "Dalton Metallic Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2260nb.png",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Metallic Blue",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2260nb.png"
        }, {
        	"old_id": "521",
        	"product_name": "Dalton Orchid",
        	"product_description": "",
        	"image_name": "images\/products\/2260o.png",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2260o.png"
        }, {
        	"old_id": "522",
        	"product_name": "Dalton White",
        	"product_description": "",
        	"image_name": "images\/products\/2260wg.png",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White ",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2260wg.png"
        }, {
        	"old_id": "523",
        	"product_name": "Corinthian Almond",
        	"product_description": "",
        	"image_name": "images\/products\/1872al.png",
        	"base_price": "990",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Almond",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1872al.png"
        }, {
        	"old_id": "524",
        	"product_name": "Reverence Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/reverenceBZ.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Bronze",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_reverenceBZ.jpg"
        }, {
        	"old_id": "525",
        	"product_name": "Reverence Almond",
        	"product_description": "",
        	"image_name": "images\/products\/reverenceal.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Almond",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_reverenceal.jpg"
        }, {
        	"old_id": "527",
        	"product_name": "Reverence Metallic Blue",
        	"product_description": "",
        	"image_name": "images\/products\/reverencenbl.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Metalic Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_reverencenbl.jpg"
        }, {
        	"old_id": "528",
        	"product_name": "Reverence Orchid",
        	"product_description": "",
        	"image_name": "images\/products\/reverencenO.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_reverencenO.jpg"
        }, {
        	"old_id": "529",
        	"product_name": "Reverence White",
        	"product_description": "",
        	"image_name": "images\/products\/reverencenw.jpg",
        	"base_price": "900",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "White",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_reverencenw.jpg"
        }, {
        	"old_id": "530",
        	"product_name": "Lords Prayer Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/1882nbfc.jpg",
        	"base_price": "1000",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1882nbfc.jpg"
        }, {
        	"old_id": "531",
        	"product_name": "Meadow Full Couch",
        	"product_description": "",
        	"image_name": "images\/products\/1885wpfc.jpg",
        	"base_price": "1000",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1885wpfc.jpg"
        }, {
        	"old_id": "532",
        	"product_name": "Garland White ",
        	"product_description": "",
        	"image_name": "images\/products\/2209wv.jpg",
        	"base_price": "690",
        	"material": "",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "White",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2209wv.jpg"
        }, {
        	"old_id": "533",
        	"product_name": "Cross White\/Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2209wbg.jpg",
        	"base_price": "560",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "White\/Blue\/Gold",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2209wbg.jpg"
        }, {
        	"old_id": "534",
        	"product_name": "Avalon Viking Blue",
        	"product_description": "",
        	"image_name": "images\/products\/188vb.jpg",
        	"base_price": "960",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Basketweave",
        	"color_finish": "Viking Blue\/Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_188vb.jpg"
        }, {
        	"old_id": "535",
        	"product_name": "Avalon Graphite",
        	"product_description": "",
        	"image_name": "images\/products\/1883gm.jpg",
        	"base_price": "960",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "White Basketweave",
        	"color_finish": "Gunmetal Metallic",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1883gm.jpg"
        }, {
        	"old_id": "536",
        	"product_name": "Avalon Bronze",
        	"product_description": "",
        	"image_name": "images\/products\/1883bz.jpg",
        	"base_price": "950",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Beige Basketweave",
        	"color_finish": "Bronze Metallic",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1883bz.jpg"
        }, {
        	"old_id": "537",
        	"product_name": "Olde South Barn Wood",
        	"product_description": "",
        	"image_name": "images\/products\/1470.jpg",
        	"base_price": "2220",
        	"material": "Authentic Barn Wood",
        	"old_color_finish": null,
        	"interior": "Broadcloth Cotton",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1470.jpg"
        }, {
        	"old_id": "538",
        	"product_name": "Olde South Cedar",
        	"product_description": "",
        	"image_name": "images\/products\/1471.jpg",
        	"base_price": "2240",
        	"material": "Solid Cedar",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Satin Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1471.jpg"
        }, {
        	"old_id": "539",
        	"product_name": "Old South Woods of Carolinas ",
        	"product_description": "",
        	"image_name": "images\/products\/1472.jpg",
        	"base_price": "2240",
        	"material": "Varity of 7 Hardwoods",
        	"old_color_finish": null,
        	"interior": "Beige Velvet",
        	"color_finish": "Saitn Finish",
        	"old_category_code": "1",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1472.jpg"
        }, {
        	"old_id": "540",
        	"product_name": "Lexington White\/Gold",
        	"product_description": "",
        	"image_name": "images\/products\/1805wg.jpg",
        	"base_price": "1560",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "White Brushed Gold",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1805wg.jpg"
        }, {
        	"old_id": "541",
        	"product_name": "Hero Silver",
        	"product_description": "",
        	"image_name": "images\/products\/1840s-ivory.jpg",
        	"base_price": "1450",
        	"material": "18 Gauge ",
        	"old_color_finish": null,
        	"interior": "White Velvet",
        	"color_finish": "Silver",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1840s-ivory.jpg"
        }, {
        	"old_id": "542",
        	"product_name": "Knight Neo Blue",
        	"product_description": "",
        	"image_name": "images\/products\/2025vb.jpg",
        	"base_price": "990",
        	"material": "20 Gauge ",
        	"old_color_finish": null,
        	"interior": "Blue Crepe",
        	"color_finish": "Neo Blue Silver Mirror ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2025vb.jpg"
        }, {
        	"old_id": "543",
        	"product_name": "Hamilton Orchid Rose",
        	"product_description": "",
        	"image_name": "images\/products\/1832HY.jpg",
        	"base_price": "1230",
        	"material": "18 Gauge",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "Orchid Brushed ",
        	"old_category_code": "3",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1832HY.jpg"
        }, {
        	"old_id": "544",
        	"product_name": "Moreland 40 White\/Pink ",
        	"product_description": "",
        	"image_name": "images\/products\/40wp.jpg",
        	"base_price": "2420",
        	"material": "18 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "Pink Crepe",
        	"color_finish": "White\/Pink",
        	"old_category_code": "6",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_40wp.jpg"
        }, {
        	"old_id": "545",
        	"product_name": "Butner Silver",
        	"product_description": "",
        	"image_name": "images\/products\/2029s.jpg",
        	"base_price": "940",
        	"material": "20 Gauge Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe ",
        	"color_finish": "Silver ",
        	"old_category_code": "4",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2029s.jpg"
        }, {
        	"old_id": "546",
        	"product_name": "Artic Flat Pewter",
        	"product_description": "",
        	"image_name": "images\/products\/2200PT.jpg",
        	"base_price": "390",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "White Crepe",
        	"color_finish": "Flat Gray ",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2200PT.jpg"
        }, {
        	"old_id": "547",
        	"product_name": "Artic Flat Brown ",
        	"product_description": "",
        	"image_name": "images\/products\/2200BZ.jpg",
        	"base_price": "390",
        	"material": "20 Gauge Non-Gasketed",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Flat Brown ",
        	"old_category_code": "5",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_2200BZ.jpg"
        }, {
        	"old_id": "548",
        	"product_name": "EcoVeiw 25",
        	"product_description": "",
        	"image_name": "images\/products\/1205.jpg",
        	"base_price": "110",
        	"material": "Cardboard",
        	"old_color_finish": null,
        	"interior": "Rosetan Crepe",
        	"color_finish": "Wood Grain Print",
        	"old_category_code": "7",
        	"notes": null,
        	"old_thumbnail_image": "images\/products\/t_1205.jpg"
        }
    ];
    // oldProductsArray Above this.
    let newProductsArray = [];

    // product functions

    function createSlugTextString(arg) {
        var finalSlug = arg.split(' ').join('_').toLowerCase();
        return finalSlug
    }

    // Programatically create a product code
    function createProductCode() {
        let getTimeNow = Date.parse(new Date());
        let randNum = Math.random().toString();
        let slicedNum = randNum.slice(3,9);

        let newUserCode = 'product_'+getTimeNow+slicedNum;
        return newUserCode;
    }

    oldProductsArray.forEach(item => {
        // console.log(item);
        // Category Rules
        let newCategoryCode;
        switch (item.old_category_code) {
            case '1':
                // Hardwoods
                newCategoryCode = 'category_1500336314000130734';
                break;
            case '2':
                // Precious Metals
                newCategoryCode = 'category_1500336333000175059';
                break;
            case '3':
                // 18 Gauge
                newCategoryCode = 'category_1500336350000352025';
                break;
            case '4':
                // 20 Gauge Gasketed
                newCategoryCode = 'category_1500336362000090056';
                break;
            case '5':
                // 20 Gauge Non-Gasketed
                newCategoryCode = 'category_1500336372000886046';
                break;
            case '6':
                // Oversize and Tall/Wides
                newCategoryCode = 'category_1500336383000938469';
                break;
            case '7':
                // Cremation/Cloth
                newCategoryCode = 'category_1500336395000800876';
                break;
            case '8':
                // Specialty
                newCategoryCode = 'category_1500336410000126030';
                break;
            case '39':
                // Panel Inserts
                newCategoryCode = 'category_1500336507000034412';
                break;
            default:
                newCategoryCode = 'no_category_code';
        }

        let newObject = {
            old_id: item.old_id,
            product_code: createProductCode(),
            product_name: item.product_name,
            product_description: item.product_description,
            image_name: 'stock_product_image_for_data_migration',
            thumbnail_image: 'stock_product_image_for_data_migration',
            base_price: item.base_price,
            material: item.material,
            old_color_finish: item.old_color_finish,
            interior: item.interior,
            color_finish: item.color_finish,
            category_code: newCategoryCode,
            old_category_code: item.old_category_code,
            notes: item.notes,
            old_thumbnail_image: item.old_thumbnail_image,
            created: todays
        };
        // if ((item.interior !== null) &&
        //     (item.interior !== undefined) &&
        //     (item.interior !== '')) {
        //
        // }
        newObject.interior_slug = createSlugTextString(item.interior);
        newObject.product_name_slug = createSlugTextString(item.product_name);
        newObject.color_finish_slug = createSlugTextString(item.color_finish);
        newObject.material_slug = createSlugTextString(item.material);
        newProductsArray.push(newObject);
    })
    return res.json({
        success: true,
        message: 'Working on this taskâ€¦',
        productsCount: newProductsArray.length,
        newProductsArray: newProductsArray
    });
};

// module.exports.someFunctionName = function(req, res) {
//
// };

// module.exports.someFunctionName = function(req, res) {
//
// };
