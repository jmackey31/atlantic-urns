let mongoose          = require('mongoose'),
    mongoosePaginate  = require('mongoose-paginate'),
    Schema            = mongoose.Schema;

let MediaSchema = new Schema({
    image_name: { type: String },
    image_caption: { type: String },
    file_name: { type: String },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

MediaSchema.plugin(mongoosePaginate);

// return the model
// let MediaModel = mongoose.model('Media', MediaSchema);

// return the model
module.exports = mongoose.model('Media', MediaSchema);
