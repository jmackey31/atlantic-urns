let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let MailingListSchema = new Schema({
    users_mailing_name: { type: String },
    email: { type: String, required: true, index: { unique: true }},
    user_wants_subscription: { type: Boolean, default: true },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

// return the model
module.exports = mongoose.model('MailingList', MailingListSchema);
