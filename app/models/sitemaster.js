let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let SiteMasterSchema = new Schema({
    master_product_price_markup: { type: Number, default: 0 },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

// return the model
module.exports = mongoose.model('SiteMaster', SiteMasterSchema);
