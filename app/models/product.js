let mongoose          = require('mongoose'),
    mongoosePaginate  = require('mongoose-paginate'),
    Schema            = mongoose.Schema;

let ProductSchema = new Schema({
    old_id: { type: String },
    product_code: { type: String },
    category_code: { type: String },
    product_description: { type: String },
    interior: { type: String },
    interior_slug: { type: String },
    product_name: { type: String },
    product_name_slug: { type: String },
    base_price: { type: String },
    master_price_price_increase: { type: String },
    color_finish: { type: String },
    color_finish_slug: { type: String },
    image_name: { type: String },
    thumbnail_image: { type: String },
    material: { type: String },
    material_slug: { type: String },
    notes: { type: String },
    model_style: { type: String },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

ProductSchema.plugin(mongoosePaginate);

// return the model
module.exports = mongoose.model('Product', ProductSchema);
