let jwt               = require('jsonwebtoken'),
    config            = require('../../config'),
    superSecret       = config.secret,
    fs                = require('fs');

// Load Global Exported Functons
let globalFunctions   = require('../logic/global-functions'),
    usersLogic        = require('../logic/users-logic/userlogic');

// Load modules specific to working with collection schemas
let User              = require('../models/user');

module.exports = function(app, express) {

    // get an instance of the express router
    let apiRouter = express.Router();

    // ------------------------------------------------------------------------------
    // ******************************************************************************
    // ************ START NON AUTHENTICATED REQUIRED SECTION ************************
    // ******************************************************************************
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // NON AUTHENTICATED Users requests
    // ------------------------------------------------------------------------------
    apiRouter.get('/fixoldproductsarray', globalFunctions.fixOldProductsArray);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // NON AUTHENTICATED General / Other requests
    // ------------------------------------------------------------------------------
    apiRouter.post('/authenticate', globalFunctions.authenticateEndpoints);
    apiRouter.get('/getallcategories', globalFunctions.getAllCategories);
    apiRouter.post('/getallproducts', globalFunctions.getAllProducts);
    apiRouter.post('/getproductsbycategorycode/:category_code', globalFunctions.getProductsByCategoryCode);
    apiRouter.get('/getsingleproductbycode/:product_code', globalFunctions.getSingleProductByCode);
    apiRouter.post('/managemailinglist', globalFunctions.manageMailingList);

    // COMMENT THESE ENDPOINTS OUT
    // apiRouter.post('/migrateoldusers', globalFunctions.migrateOldUsers);
    // apiRouter.post('/migrateoldproducts', globalFunctions.migrateOldProducts);
    // apiRouter.post('/migrateoldimages', globalFunctions.migrateOldImages);
    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // ******************************************************************************
    // ************ END NON AUTHENTICATED REQUIRED SECTION **************************
    // ******************************************************************************
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // Route middleware to verify tokens sent up with http and https requests
    apiRouter.use(function(req, res, next) {
        // check header or url parameters or post parameters for token
        let token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, superSecret, function(err, decoded) {
                if (err) {
                    return res.status(403).json({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            return res.status(403).json({
                success: false,
                message: 'No token provided.'
            });
        }
    });
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    apiRouter.get('/', function(req, res) {
        res.json({ message: 'I grant you 3 wishes.'});
    });
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // ******************************************************************************
    // *************** START REQUIRED AUTHENTICATION SECTION ************************
    // *********** ANY CODE BELOW THIS LINE WILL REQUIRE AUTHENTICATION *************
    // ******************************************************************************
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // AUTHENTICATED Users requests
    // ------------------------------------------------------------------------------
    apiRouter.post('/getallusers', usersLogic.getAllUsers);
    apiRouter.post('/getuserinfo', usersLogic.getUserInfo);
    apiRouter.put('/changemypassword/:user_id', usersLogic.changeMyPassword);
    apiRouter.put('/updateuseraccountstatus/:user_id', usersLogic.updateUserAccountStatus);
    apiRouter.post('/deleteuseraccount/:user_id', usersLogic.deleteUserAccount);
    apiRouter.get('/getloginhistoryreports/:user_id', usersLogic.getLoginHistoryReports);
    apiRouter.post('/singleloginhistoryreport/:user_id', usersLogic.singleLoginHistoryReports);
    apiRouter.get('/getuseraccounts/:user_id', usersLogic.getUserAccounts);
    apiRouter.post('/searchuseraccounts/:user_id', usersLogic.searchUserAccounts);
    apiRouter.get('/getuserpricepoint/:user_code', usersLogic.getUserPricePoint);
    apiRouter.post('/createnewuseraccount', usersLogic.newUser);
    apiRouter.post('/createnewpublicuser', usersLogic.newPublicUser);
    apiRouter.put('/updateuseraccount/:user_id', usersLogic.updateUserAccount);
    apiRouter.put('/updatepricemarkup/:user_id', usersLogic.updatePriceMarkup);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // REQUIRED AUTHENTICATED General / Other requests
    // ------------------------------------------------------------------------------
    apiRouter.get('/getmedialibrary', globalFunctions.getMediaLibrary);
    apiRouter.post('/getpagedmedialibrary', globalFunctions.getPagedImageLibrary);
    apiRouter.get('/getmasterpricerate', globalFunctions.getMasterPriceRate);
    apiRouter.post('/uploadmedia', globalFunctions.uploadToMediaLibrary);
    apiRouter.put('/updateimagemedia/:image_id', globalFunctions.updateImageMedia);
    apiRouter.post('/deleteimagemedia/:image_id', globalFunctions.deleteImageMedia);
    apiRouter.post('/createcategory/:user_id', globalFunctions.createNewCategory);
    apiRouter.post('/createproduct/:user_id', globalFunctions.createNewProduct);
    apiRouter.post('/changesinglecategorydata', globalFunctions.changeSingleCategoryData);
    apiRouter.post('/changesingleproductdata', globalFunctions.changeSingleProductData);
    apiRouter.post('/deleteproduct/:user_id', globalFunctions.deleteProduct);
    apiRouter.post('/deletecategory/:user_id', globalFunctions.deleteCategory);
    apiRouter.post('/getadmincategories/:user_id', globalFunctions.getAdminCategories);
    apiRouter.post('/mastersiterateincrease/:user_id', globalFunctions.masterSiteRateIncrease);
    apiRouter.post('/searchproductsuri', globalFunctions.searchProducts);
    apiRouter.post('/getmailinglistemails/:user_id', globalFunctions.getMailingListEmails);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    return apiRouter;

};
