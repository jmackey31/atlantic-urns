let gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin'),
    clean = require('gulp-clean'),
    cache = require('gulp-cache'),
    del = require('del'),
    runSequence = require('run-sequence'),
    gulpConcat = require('gulp-concat'),
    jsMin = require('gulp-jsmin'),
    jshint = require('gulp-jshint');

// Project directories and destinations
let sassInput = 'public/assets/scss/**/*.scss',
    sassOutput = 'public/assets/css',
    mainSassFile = 'public/assets/scss/main.all.scss',
    publicDist = 'public/dist',
    publicAppScripts = 'public/app/**/*.js',
    slickAjaxLoader = './bower_components/slick-carousel/slick/ajax-loader.gif';

// Bower JS Files
let bowerJsFiles = [
        './bower_components/jquery/dist/jquery.js',
        './bower_components/jquery-ui/jquery-ui.js',
        './bower_components/angular/angular.js',
        './bower_components/angular-resource/angular-resource.js',
        './bower_components/angular-cookies/angular-cookies.js',
        './bower_components/angular-sanitize/angular-sanitize.js',
        './bower_components/angular-touch/angular-touch.js',
        './bower_components/angular-ui-router/release/angular-ui-router.js',
        './bower_components/angular-route/angular-route.js',
        './bower_components/angular-animate/angular-animate.js',
        './bower_components/slick-carousel/slick/slick.js',
        './bower_components/angular-slick-carousel/dist/angular-slick.js',
        './bower_components/bootstrap/dist/js/bootstrap.js',
        './bower_components/moment/moment.js'
    ],
    jsForBundle = [
        'public/dist/js/00_vendor.min.js',
        'public/dist/js/main.min.js'
    ],
    jsLintFiles = [
        'public/app/controllers/searchCtrl.js'
    ],
    bowerCssFiles = [
        './bower_components/jquery-ui/themes/base/jquery-ui.css',
        './bower_components/font-awesome/css/font-awesome.css',
        './bower_components/slick-carousel/slick/slick.css',
        './bower_components/slick-carousel/slick/slick-theme.css',
        './bower_components/bootstrap/dist/css/bootstrap.css'
    ],
    cssForBundle = [
        'public/dist/css/00_vendor.min.css',
        'public/dist/css/main.all.min.css'
    ],
    fontsDir = [
        './bower_components/slick-carousel/slick/fonts/**/*',
        './bower_components/bootstrap/fonts/**/*',
        './bower_components/font-awesome/fonts/**/*'
    ];

// Start copying third party libs ----------------------------------------------------
gulp.task('copythirdpartyjs', function () {
    return gulp.src(bowerJsFiles)
        .pipe(gulpConcat('00_vendor.js'))
        .pipe(jsMin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(publicDist + '/js'));
});

gulp.task('copythirdpartycss', function () {
    gulp.src(bowerCssFiles)
        .pipe(gulpConcat('00_vendor.css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(publicDist + '/css'));
});

gulp.task('bundleallcss', function () {
    gulp.src(cssForBundle)
        .pipe(gulpConcat('bundle.min.css'))
        .pipe(cssnano())
        .pipe(gulp.dest(publicDist + '/css'));
});

// fonts task
gulp.task('fonts', function () {
    return gulp.src(fontsDir)
        .pipe(gulp.dest(publicDist + '/fonts'))
});

// fonts task
gulp.task('fontstoassets', function () {
    return gulp.src(fontsDir)
        .pipe(gulp.dest('public/assets/fonts'))
});

// slick icon task
gulp.task('iconloader', function () {
    return gulp.src(slickAjaxLoader)
        .pipe(gulp.dest(publicDist + '/css'));
});

// Place font-awesome icons into the dist/css directory
gulp.task('fafonts', function () {
    return gulp.src('./bower_components/slick-carousel/slick/fonts/**/*')
        .pipe(gulp.dest(publicDist + '/css/fonts'));
});
// End copying third party libs ----------------------------------------------------

// Compile project sass
gulp.task('sass', function () {
    return gulp.src(sassInput)
        .pipe(sass())
        .pipe(gulp.dest(sassOutput));
});

gulp.task('minifystyles', function () {
    return gulp.src(mainSassFile)
        .pipe(sass())
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(publicDist + '/css'));
});

gulp.task('buildappscripts', function () {
    return gulp.src(publicAppScripts)
        .pipe(gulpConcat('main.js'))
        .pipe(jsMin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(publicDist + '/js'));
});

gulp.task('bundlealljs', function () {
    gulp.src(jsForBundle)
        .pipe(gulpConcat('bundle.min.js'))
        .pipe(jsMin())
        .pipe(gulp.dest(publicDist + '/js'));
});

// Optimizing Images
gulp.task('images', function () {
    return gulp.src('public/assets/images/**/*.+(png|jpg|jpeg|gif|svg)')
    // Caching images that ran through imagemin
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest(publicDist + '/images'))
});

gulp.task('lintjs', function () {
    return gulp.src(jsLintFiles)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Setup project by copying all of the necessary bower_components into the app.
gulp.task('build', function (callback) {
    runSequence(
        [
            'copythirdpartycss',
            'fonts',
            'fontstoassets',
            'fafonts',
            'iconloader',
            'sass',
            'images',
            'minifystyles',
            'bundleallcss',
            'copythirdpartyjs',
            'buildappscripts',
            'bundlealljs'
        ],
        callback
    )
});

gulp.task('watch', function () {
    gulp.watch(sassInput, ['sass']);
});

gulp.task('clean', function () {
    return del.sync(publicDist);
});

gulp.task('default', function (callback) {
    runSequence(['build', 'watch'],
        callback
    )
});
