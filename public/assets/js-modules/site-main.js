$(document).ready(function() {
    // console.log('Global helper');
    // var globalHelper = $('#global-uri-helper').text();
    var currentEnv;
    var globalHelper = location.hostname;
    var protocalHelper = 'http://';
    var serverAdmin = $('#server-admin').text().trim();
    var publicPageRouteHelper;
    var privatePageRouteHelper;
    var pageRouteHelper;
    var categoriesRemoval = false;
    console.log('User Access Level');
    console.log(serverAdmin);
    console.log('-------------');
    // HTTP Call Helpers and vars
    var localQueryHelper = 'http://142.147.16.59/tacc/ta_mvc/index.php/';
    var liveQueryHelper = 'http://tacc.setechsupport.net/ta_mvc/index.php/';
    // *********************************************************************
    // siteUrlHelper = globalHelper+'index.php/';
    siteUrlHelper = globalHelper;
    // console.log(siteUrlHelper);
    // console.log('---------------- End Global helper');
    // ----------------------------------------
    // if (siteUrlHelper == 'http://142.147.16.59/triangle-atlantic-ci_site/index.php/') {
    if (siteUrlHelper == '142.147.16.59') {
        // LOCAL URI DEVELOPMENT HELPERS
        console.log('You are working locally!');
        console.log('siteUrlHelper');
        console.log(siteUrlHelper);
        currentEnv = 'localEnv';
        console.log('currentEnv = ' + currentEnv);
        // set the site URL Helper to it's proper mappings
        siteUrlHelper = localQueryHelper;
        // set the page route helpers
        publicPageRouteHelper = protocalHelper+globalHelper+'/tacc/';
        privatePageRouteHelper = protocalHelper+globalHelper+'/tacc/ta_mvc/index.php/';
        // Reset tge
        // var imageHelper = window.location.protocol + '//' + window.location.host + '/triangle-atlantic-ci_site';
        var imageHelper = window.location.protocol + '//' + window.location.host + '/tacc';
    	  // console.log('imageHelper');
    	  // console.log(imageHelper);
        // console.log('---------------------------');
        // ----------------------------------------
    } else {
        // LIVE URI HELPERS
        console.log('You are working on the LIVE site!');
        console.log(siteUrlHelper);
        var imageHelper = window.location.protocol + '//' + window.location.host;
    	  // console.log('imageHelper');
    	  // console.log(imageHelper);
        currentEnv = 'liveEnv';
        console.log('currentEnv = ' + currentEnv);
        // set the site URL Helper to it's proper mappings
        siteUrlHelper = liveQueryHelper;
        // set the page route helpers
        publicPageRouteHelper = protocalHelper+globalHelper+'/';
        privatePageRouteHelper = protocalHelper+globalHelper+'/ta_mvc/index.php/';
        console.log('---------------------------');
    }
    // ----------------------------------------
    // var vm = this.body;
    var vm = this;
    vm.vars = {
        categoryWrapper: document.querySelector('#catalog-column-wrapper'),
        publicPageRoutes: {
            home: document.querySelector('#ta-home'),
            catalog: document.querySelector('#ta-catalog'),
            showProducts: document.querySelector('#ta-show-products'),
            showSingle: document.querySelector('#ta-show-single'),
            allCaskets: document.querySelector('#all-caskets-column-wrapper'),
            contactus: document.querySelector('#ta-contact-us'),
            links: document.querySelector('#ta-links'),
            login: document.querySelector('#ta-login')
        },
        privatePageRoutes: {
            mainMenu: document.querySelector('#ta-main-menu'),
            changeAdmin: document.querySelector('#ta-change-admin'),
            imageLibrary: document.querySelector('#ta-image-library-thumb-block'),
            showAdminProducts: document.querySelector('#ta-admin-show-products'),
            adminShowSingle: document.querySelector('#ta-admin-show-single'),
            addItemWrapper: document.querySelector('#ta-add-item-wrapper'),
            addCategoryWrapper: document.querySelector('#ta-add-category-wrapper'),
            adminEditMasterInfo: document.querySelector('#ta-admin-edit-master-info-wrapper'),
            adminRemoveACategory: document.querySelector('#ta-admin-remove-category'),
            adminManageUserAccounts: document.querySelector('#ta-admin-manage-user-accounts'),
            adminGetUserAccountHistory: document.querySelector('#ta-admin-user-account-history')
        },
        getUriParameters: function(search_string) {
            var parse = function(params, pairs) {
                var pair = pairs[0];
                var parts = pair.split('=');
                var key = decodeURIComponent(parts[0]);
                var value = decodeURIComponent(parts.slice(1).join('='));

                // Handle multiple parameters of the same name
                if (typeof params[key] === "undefined") {
                    params[key] = value;
                } else {
                    params[key] = [].concat(params[key], value);
                }

                return pairs.length == 1 ? params : parse(params, pairs.slice(1));
            }

            // Get rid of leading ?
            return search_string.length == 0 ? {} : parse({}, search_string.substr(1).split('&'));
        }
    };
    // ----------------------------------------
    // Get categories data
    function getCategories() {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_categories';
        } else {
            query = liveQueryHelper+'get_json_categories';
        }
        $.getJSON(query, function(data) {
            if (data) {
                // console.log('success');
                // if the element is present populate the categories grid
                var categoriesGrid = $('#catalog-column-wrapper');
                var categoriesWrapper = $('#categories-column-wrapper');
                var categoriesMenu = $('#catalog-column-ul');
                if (categoriesGrid.length > 0) {
                    // console.log('poplate the categories grid');
                    // console.log(categoriesGrid.length);
                    $.each(data, function(i, val) {
                        categoriesGrid.append(
                        		'<div class="col-sm-6 col-md-4 product-thumb-wrapper">' +
                        				'<div class="thumbnail">' +
                                    '<a href="show_prods.php?type='+val.cat_code+'" class="catalog-image-anchor">' +
                  						         '<img src="'+imageHelper+'/ecatalog/'+val.thmb_img+'" alt="'+val.cat_desc+'">' +
                                    '</a>' +
                        						'<div class="caption">' +
                        								'<h3>'+val.cat_desc+'</h3>' +
                        								'<p>' +
                        										val.cat_desc +
                        										'<br>' +
                        										'<hr>' +
                        										// '<span>'+val.cat_code+'</span>' +
                        								'</p>' +
                        								'<p>' +
                        										'<a href="show_prods.php?type='+val.cat_code+'" class="btn btn-primary" role="button">View</a>' +
                        								'</p>' +
                        						'</div>' +
                        				'</div>' +
                        		'</div>'
                        );
                    });
                } else {
                  // console.log('No need to populate the categories grid.');
                }

                if (categoriesWrapper.length > 0) {
                    // console.log('poplate the categories menu wrapper.');
                    // console.log(categoriesWrapper.length);
                    $.each(data, function(i, val) {
                        // console.log(val.cat_desc);
                        categoriesMenu.append(
                            // '<li role="presentation"><a href="'+siteUrlHelper+'show_prods?type='+val.cat_code+'">'+val.cat_desc+'</a></li>'
                            '<li role="presentation"><a href="show_prods.php?type='+val.cat_code+'">'+val.cat_desc+'</a></li>'
                        );
                    });
                    categoriesMenu.append(
                        // '<li role="presentation"><a href="'+siteUrlHelper+'allcaskets">Show All Caskets</a></li>'
                        '<li role="presentation"><a href="allcaskets.php">Show All Caskets</a></li>'
                    );
                } else {
                  // console.log('No need to populate the categories menu.');
                }
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get categories data
    function getAdminCategories() {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_categories';
        } else {
            query = liveQueryHelper+'get_json_categories';
        }
        // $.getJSON(siteUrlHelper+'get_json_categories', function(data) {
        $.getJSON(query, function(data) {
            if (data) {
                // console.log('success');
                // if the element is present populate the categories grid
                var categoriesRemovalGrid = $('#admin-categories-removal-column-wrapper');
                var categoriesGrid = $('#admin-catalog-column-wrapper');
                var categoriesWrapper = $('#admin-categories-column-wrapper');
                var categoriesMenu = $('#admin-catalog-column-ul');
                var adminRemovalCategoriesMenu = $('#admin-removal-catalog-column-ul');
                if (categoriesGrid.length > 0) {
                    // console.log('poplate the categories grid');
                    // console.log(categoriesGrid.length);
                    if (serverAdmin === '1') {
                        $.each(data, function(i, val) {
                            categoriesGrid.append(
                            		'<div class="col-sm-6 col-md-4 product-thumb-wrapper">' +
                            				'<div class="thumbnail">' +
                                        '<a href="'+siteUrlHelper+'show_admin_prods?type='+val.cat_code+'" class="catalog-image-anchor">' +
                      						         '<img src="'+imageHelper+'/ecatalog/'+val.thmb_img+'" alt="'+val.cat_desc+'">' +
                                        '</a>' +
                            						// '<img src="'+imageHelper+'/ecatalog/'+val.thmb_img+'" alt="'+val.cat_desc+'">' +
                            						'<div class="caption">' +
                            								'<h3>'+val.cat_desc+'</h3>' +
                            								'<p>' +
                            										val.cat_desc +
                                                ' <button ' +
                                                  ' data-thumb="'+val.thmb_img+'"' +
                                                  ' data-description="'+val.cat_desc+'"' +
                                                  ' data-cat-code="'+val.cat_code+'"' +
                                                  ' class="btn btn-sm btn-warning edit-category">Edit</button>' +
                            										'<br>' +
                            										'<hr>' +
                            										// '<span>'+val.cat_code+'</span>' +
                            								'</p>' +
                            								'<p>' +
                            										'<a href="'+siteUrlHelper+'show_admin_prods?type='+val.cat_code+'" class="btn btn-primary" role="button">View</a>' +
                            								'</p>' +
                            						'</div>' +
                            				'</div>' +
                            		'</div>'
                            );
                        });
                    } else {
                        $.each(data, function(i, val) {
                            categoriesGrid.append(
                                '<div class="col-sm-6 col-md-4 product-thumb-wrapper">' +
                                    '<div class="thumbnail">' +
                                        '<a href="'+siteUrlHelper+'show_admin_prods?type='+val.cat_code+'" class="catalog-image-anchor">' +
                      						         '<img src="'+imageHelper+'/ecatalog/'+val.thmb_img+'" alt="'+val.cat_desc+'">' +
                                        '</a>' +
                                        // '<img src="'+imageHelper+'/ecatalog/'+val.thmb_img+'" alt="'+val.cat_desc+'">' +
                                        '<div class="caption">' +
                                            '<h3>'+val.cat_desc+'</h3>' +
                                            '<p>' +
                                                val.cat_desc +
                                                '<br>' +
                                                '<hr>' +
                                                // '<span>'+val.cat_code+'</span>' +
                                            '</p>' +
                                            '<p>' +
                                                '<a href="'+siteUrlHelper+'show_admin_prods?type='+val.cat_code+'" class="btn btn-primary" role="button">View</a>' +
                                            '</p>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>'
                            );
                        });
                    }
                } else {
                  // console.log('No need to populate the categories grid.');
                }

                if (categoriesWrapper.length > 0) {
                    // console.log('poplate the categories menu wrapper.');
                    // console.log(categoriesWrapper.length);

                    // Check to see if the user is a level 1 admin and that the categories removal varaiable is set to true.
                    if ((serverAdmin === '1') && (categoriesRemoval === true)) {
                        console.log('ok to remove categories');
                        $.each(data, function(i, val) {
                            adminRemovalCategoriesMenu.append(
                                '<li role="presentation"> ' +
                                    '<span>'+val.cat_desc+'</span> ' +
                                    '<span class="btn btn-sm btn-danger remove-category-button" data-cat_code='+val.cat_code+'>Remove</span> <hr>' +
                                '</li>'
                            );
                        });

                        $('.remove-category-button').click(function() {
                            var categoryToRemove = $(this)[0].dataset.cat_code;
                            console.log('categoryToRemove');
                            console.log(categoryToRemove);
                            $('#admin-remove-category-cat-code').val(categoryToRemove);
                            $('#remove-category-delete-submit-button').click();
                        });
                    } else {
                        console.log('Just another display of categories');
                        $.each(data, function(i, val) {
                            categoriesMenu.append(
                                '<li role="presentation"><a href="'+privatePageRouteHelper+'show_admin_prods?type='+val.cat_code+'">'+val.cat_desc+'</a></li>'
                            );
                        });
                    }
                    categoriesMenu.append(
                        '<li role="presentation"><a href="'+publicPageRouteHelper+'allcaskets.php">Show All Caskets</a></li>'
                    );
                    console.log('globalHelper from here dude:');
                    console.log(globalHelper);
                } else {
                  // console.log('No need to populate the categories menu.');
                }

                var editCategoryBlock = $('#edit-category-block');
                var editCatCode = $('#cat-code');
                var editProductName = $('#product-name');
                var editImageName = $('#image-name');
                var imageDisplayName = $('#image-display-name');
                var submitEditedCategory = $('#submit-button');
                var closeEditing = $('#close-category-editing');
                // var selectNewPhoto = $('.image_library_thumbnail img');
                $('.edit-category').click(function() {
                    var editCatObj = {
                        thumb: $(this)[0].dataset.thumb,
                        description: $(this)[0].dataset.description,
                        catCode: $(this)[0].dataset.catCode
                    };
                    console.log(editCatObj);
                    // console.log($(this));
                    // console.log($(this)[0].dataset.thumb);
                    // console.log($(this)[0].dataset.description);
                    // console.log($(this)[0].dataset.catCode);

                    // This variable needs to be set here so that it is in scope when the ".edit_category" button is clicked.
                    var selectNewPhoto = $('.image_library_thumbnail img');

                    // show "edit-category-block"
                    editCategoryBlock.show();
                    // Bind the selected product fields
                    editCatCode.val(editCatObj.catCode);
                    editProductName.val(editCatObj.description);
                    editImageName.val(editCatObj.thumb);

                    // Close editing block
                    closeEditing.click(function() {
                        editCategoryBlock.hide();
                    });

                    // Inject new image file string value into the edit category edit form
                    selectNewPhoto.click(function() {
                        var selectedImage = $(this);
                        var selectedImage = $(this)[0].dataset.fileName;
                        // console.log('selectedImage');
                        // console.log(selectedImage);
                        editImageName.val('images/'+selectedImage);
                        imageDisplayName.text(selectedImage);
                    });
                });
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get admin image library data
    function getAdminImageLibrary() {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_image_library';
        } else {
            query = liveQueryHelper+'get_json_image_library';
        }
        $.getJSON(query, function(data) {
            if (data) {
                // console.log('success');
                // console.log('imageHelper from func');
                // console.log(imageHelper);
                if (serverAdmin === '1') {
                    libraryDiv = $('#library-div');
                    $.each(data, function(i, val) {
                        libraryDiv.append(
                            '<div class="col-xs-6 col-md-3">' +
                                '<div class="image_library_thumbnail">' +
                                  '<img data-file-name="'+val.file_name+'" src="'+imageHelper+'/ecatalog/images/'+val.file_name+'" alt="'+val.image_name+'"/>' +
                                '</div>' +
                                '<span class="label label-info">'+val.image_caption+'</span>' +
                            '</div>'
                        );
                    });

                    // Allow the adding of product / item images once this loads ---------------------
                    // New product images
                    var newProductImage = $('#new-product-image');
                    // Select new photo
                    var selectNewPhoto = $('.image_library_thumbnail img');
                    // Editing product images
                    var productImageName = $('#product-image-name');
                    selectNewPhoto.click(function() {
                        var editCatObj = {
                            fileName: $(this)[0].dataset.fileName
                        };
                        // console.log(editCatObj);

                        // Inject new image file string value into the edit category edit form
                        newProductImage.val('images/'+editCatObj.fileName);
                    });

                    selectNewPhoto.click(function() {
                        var newImageForEditField = $(this)[0].dataset.fileName
                        // console.log(editCatObj);

                        // Inject new image file string value into the edit category edit form
                        productImageName.val('images/'+newImageForEditField);
                        $('#product-image-display-name').text('images/'+newImageForEditField);
                    });
                    // End Allow the adding of product / item images once this loads -----------------
                }
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for a specific category
    function getSpecificCategory(cat_code) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_category';
        } else {
            query = liveQueryHelper+'get_json_category';
        }
        console.log('Grab the data by "category code"');
        // Get data by category code "cat_code"
        $.ajax({
            // url: siteUrlHelper+'get_json_category',
            url: query,
            dataType: 'json',
            data: {
                'cat_code': cat_code
            }
        })
        .then(function(data) {
            console.log(data);
            if (data) {
                $.each(data, function(i, val) {
                    $('#products-column-wrapper').append(
                    		'<div class="col-sm-6 col-md-4 short-thumbnail-wrapper">' +
                    				'<a href="show_single.php?id='+val.id+'" class="thumbnail">' +
                    						'<img src="'+imageHelper+'/ecatalog/'+val.thumb_img+'" alt="'+val.Name+'">' +
                    						'<div class="caption">' +
                    								'<h4>'+val.Name+'</h4>' +
                    						'</div>' +
                    				'</a>' +
                    		'</div>'
                    );
                });
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for a user's history
    function getAdminUserAccountHistoryData(adminusermanage) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_admin_user_account_history';
        } else {
            query = liveQueryHelper+'get_admin_user_account_history';
        }
        $.ajax({
            url: query,
            dataType: 'json',
            data: {
                'adminusermanage': adminusermanage
            }
        })
        .then(function(data) {
            // console.log(data);
            var numberOfUserLogins = data.length;
            if (data) {
                if (serverAdmin === '1') {
                    $.each(data, function(i, val) {
                        // Utilize Moment.js to format the datas.
                        // var day = moment(val.login_evt_dt, 'MM-DD-YYYY');
                        $('#admin-user-account-history-table').append(
            								'<tr> ' +
            										'<td>'+val.login_evt_dt+'</td> ' +
            										'<td>'+val.acct_status+'</td> ' +
            										'<td>'+val.acct_status+'</td> ' +
            								'</tr>'
                        );
                    });
                } else {
                    console.log('Could not get users history due to your access level');
                }
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not get users history.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for an admin level specific category
    function getAdminSpecificCategorySuperAdmin(cat_code) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_category';
        } else {
            query = liveQueryHelper+'get_json_category';
        }
        console.log('Grab the data by "category code"');
        // Get data by category code "cat_code"
        $.ajax({
            // url: siteUrlHelper+'get_json_category',
            url: query,
            dataType: 'json',
            data: {
                'cat_code': cat_code
            }
        })
        .then(function(data) {
            // console.log(data);
            if (data) {
                $.each(data, function(i, val) {
                    $('#admin-products-column-wrapper').append(
                    		'<div class="col-sm-6 col-md-4 short-thumbnail-wrapper">' +
                    				'<a href="admin_show_single?id='+val.id+'" class="thumbnail">' +
                    						'<img src="'+imageHelper+'/ecatalog/'+val.thumb_img+'" alt="'+val.Name+'">' +
                    						'<div class="caption">' +
                    								'<h4>'+val.Name+'</h4>' +
                    						'</div>' +
                    				'</a>' +
                            '<div class="btn-group">' +
                                '<span ' +
                                    'data-id="'+val.id+'" '+
                                    'data-thumb_img="'+val.thumb_img+'" '+
                                    'data-Name="'+val.Name+'" ' +
                                    'data-Interior="'+val.Interior+'" ' +
                                    'data-base_price="'+val.base_price+'" ' +
                                    'data-cat_code="'+val.cat_code+'" ' +
                                    'data-color="'+val.color+'" ' +
                                    'data-finish="'+val.finish+'" ' +
                                    'data-image="'+val.image+'" ' +
                                    'data-material="'+val.material+'" ' +
                                    'data-notes="'+val.notes+'" ' +
                                    'data-prod_desc="'+val.prod_desc+'" ' +
                                'class="btn btn-primary edit-admin-product">Edit</span>' +
                                '<span ' +
                                    'data-id="'+val.id+'" ' +
                                'class="btn btn-warning remove-admin-product">Delete</span>' +
                                '<span ' +
                                    'data-id="'+val.id+'" ' +
                                    'data-cat_code="'+val.cat_code+'" ' +
                                'class="btn btn-danger final-removal-option">Really Delete</span>' +
                                '<span class="btn btn-default cancel-removal-option">Cancel Delete</span>' +
                            '</div>' +
                            '<hr>' +
                    		'</div>'
                    );
                });

                // Declare the "Edit" and "Remove" button variables
                var editAdminProduct = $('.edit-admin-product');
                var removeAdminProduct = $('.remove-admin-product');
                var finalRemovalCommand = $('.final-removal-option');
                var cancelDelete = $('.cancel-removal-option');

                // Form fields for Editing
                var productId = $('#product-id');
                var productName = $('#product-name');
                var productCatCode = $('#product-cat-code');
                var productColor = $('#product-color');
                var productMaterial = $('#product-material');
                var productModelStyle = $('#product-model-style');
                var productBasePrice = $('#product-base-price');
                var productInterior = $('#product-interior');
                var productImageName = $('#product-image-name');
                var productDeleteCatCode = $('#product-delete-cat-code');

                // Edit a product
                editAdminProduct.click(function() {
                    // open the edit block
                    $('#edit-admin-product-block').show();
                    // close the edit block
                    $('#close-category-editing').click(function() {
                        $('#edit-admin-product-block').hide();
                    });
                    // var selectedProduct = $(this);
                    var selectedProduct = {
                        id: $(this)[0].dataset.id,
                        name: $(this)[0].dataset.name,
                        thumb_img: $(this)[0].dataset.thumb_img,
                        interior: $(this)[0].dataset.interior,
                        base_price: $(this)[0].dataset.base_price,
                        cat_code: $(this)[0].dataset.cat_code,
                        color: $(this)[0].dataset.color,
                        finish: $(this)[0].dataset.finish,
                        image: $(this)[0].dataset.image,
                        material: $(this)[0].dataset.material,
                        notes: $(this)[0].dataset.notes,
                        prod_desc: $(this)[0].dataset.prod_desc
                    };
                    console.log('selectedProduct');
                    console.log(selectedProduct);
                    productId.val(selectedProduct.id);
                    productName.val(selectedProduct.name);
                    productCatCode.val(selectedProduct.cat_code);
                    productColor.val(selectedProduct.color);
                    productMaterial.val(selectedProduct.material);
                    productModelStyle.val(selectedProduct.prod_desc);
                    productBasePrice.val(selectedProduct.base_price);
                    productInterior.val(selectedProduct.interior);
                    var imgString = selectedProduct.thumb_img;
                    var checkImageString = imgString.includes('images/');
                    if (checkImageString) {
                        console.log('The image string already has the "images/" directory extension so do not add it.');
                        $('#product-image-display-name').text(selectedProduct.thumb_img);
                        productImageName.val(selectedProduct.thumb_img);
                    } else {
                        console.log('Add the the "images/" directory to the image file.');
                        $('#product-image-display-name').text('images/'+selectedProduct.thumb_img);
                        productImageName.val('images/'+selectedProduct.thumb_img);
                    }
                });

                // Remove a product
                removeAdminProduct.click(function() {
                    console.log('Time to remove products');
                    finalRemovalCommand.show();
                    finalRemovalCommand.click(function() {
                        var productDeleteId = $('#product-delete-id');
                        var deleteSubmitForm = $('#delete-submit-button');
                        var productIdForDelete = $(this)[0].dataset.id;
                        console.log('productIdForDelete');
                        console.log(productIdForDelete);
                        productDeleteId.val(productIdForDelete);
                        productDeleteCatCode.val($(this)[0].dataset.cat_code);
                        // submit the delete form
                        deleteSubmitForm.click();
                    });
                    cancelDelete.show();
                    cancelDelete.click(function() {
                        cancelDelete.hide();
                        finalRemovalCommand.hide();
                    });
                });
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for an admin master user
    function adminUpdateMasterInfo(cat_code) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_user_account_data';
        } else {
            query = liveQueryHelper+'get_user_account_data';
        }
        $.ajax({
            url: query,
            dataType: 'json'
        })
        .then(function(data) {
            // console.log(data);
            if (data) {
                // Bind data to the form fields
                $('#master-info-contact-id').val(data[0].id);
                $('#master-info-contact-person').val(data[0].contact_person);
                $('#master-info-company-name').val(data[0].Fac_name);
                $('#master-info-address').val(data[0].addr);
                $('#master-info-address2').val(data[0].addr2);
                $('#master-info-city').val(data[0].city);
                $('#master-info-state').val(data[0].state);
                $('#master-info-zip-code').val(data[0].zip);
                $('#master-info-phone').val(data[0].phone);
                $('#master-info-email').val(data[0].email);
                $('#master-info-web-address').val(data[0].web);
                $('#master-info-fax').val(data[0].fax);
            }
        });
    }
    // ----------------------------------------
    // Get a product by id
    function getProductById(id) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_product';
        } else {
            query = liveQueryHelper+'get_json_product';
        }
        $.ajax({
            // url: siteUrlHelper+'get_json_product',
            url: query,
            dataType: 'json',
            data: {
                'id': id
            }
        })
        .then(function(data) {
            // console.log(data);
            if (data) {
                var productImageWrapper = $('#single-product-wrapper');
                var productImage = '<img src="'+imageHelper+'/ecatalog/'+data[0].image+'" alt="'+data[0].Name+'" />';
                productImageWrapper.html(productImage);
                var productUl = $('#product-details-ul');
                // Interior
                // Name
                // base_price
                // cat_code
                // color
                // finish
                // material
                // notes
                // prod_desc
                var productDetails = '<li><strong>Name:</strong> '+data[0].Name+'</li> ' +
                                		 '<li><strong>Material:</strong> '+data[0].material+'</li> ' +
                                		 '<li><strong>Finish:</strong> '+data[0].finish+'</li> ' +
                                		 '<li><strong>Interior:</strong> '+data[0].Interior+'</li> ';
                productUl.html(productDetails);
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get an admin product by id
    function getAdminProductById(id) {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_admin_json_product';
        } else {
            query = liveQueryHelper+'get_admin_json_product';
        }
        $.ajax({
            url: query,
            dataType: 'json',
            data: {
                'id': id
            }
        })
        .then(function(data) {
            console.log(data);
            if (data) {
                var productImageWrapper = $('#admin-single-product-wrapper');
                var productImage = '<img src="'+imageHelper+'/ecatalog/'+data[0].image+'" alt="'+data[0].Name+'" />';
                productImageWrapper.html(productImage);
                var productUl = $('#admin-product-details-ul');
                // Interior
                // Name
                // base_price
                // cat_code
                // color
                // finish
                // base_price
                // material
                // notes
                // prod_desc
                var productDetails = '<li><strong>Name:</strong> '+data[0].Name+'</li> ' +
                                		 '<li><strong>Material:</strong> '+data[0].material+'</li> ' +
                                		 '<li><strong>Finish:</strong> '+data[0].finish+'</li> ' +
                                		 '<li><strong>Price:</strong> $'+data[0].base_price+'</li> ' +
                                		 '<li><strong>Interior:</strong> '+data[0].Interior+'</li> ';
                productUl.html(productDetails);
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for all caskets
    function getAllCaskets() {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_json_all_caskets';
        } else {
            query = liveQueryHelper+'get_json_all_caskets';
        }
        $.ajax({
            url: query,
            dataType: 'json'
        })
        .then(function(data) {
            // console.log(data);
            if (data) {
                $.each(data, function(i, val) {
                    $('#all-caskets-column-wrapper').append(
                    		'<div class="col-sm-6 col-md-4 short-thumbnail-wrapper">' +
                    				'<a href="show_single.php?id='+val.id+'" class="thumbnail">' +
                    						'<img src="'+imageHelper+'/ecatalog/'+val.image+'" alt="'+val.Name+'">' +
                    						'<div class="caption">' +
                    								'<h4>'+val.Name+'</h4>' +
                    						'</div>' +
                    				'</a>' +
                    		'</div>'
                    );
                });
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // Get data for all admin users
    function getAdminUserAccountsData() {
        var query;
        if (currentEnv === 'localEnv') {
            query = localQueryHelper+'get_all_users_account_data';
        } else {
            query = liveQueryHelper+'get_all_users_account_data';
        }
        $.ajax({
            url: query,
            dataType: 'json'
        })
        .then(function(data) {
            // console.log(data);
            if (data) {
                if (serverAdmin === '1') {
                    $.each(data, function(i, val) {
                        $('#admin-user-accounts-table').append(
            								'<tr> ' +
            										'<td>'+val.Fac_name+'</td> ' +
            										'<td><a class="btn btn-sm btn-default" href="'+privatePageRouteHelper+'useraccounthistory?adminusermanage='+val.id+'">History</a></td> ' +
            										'<td>'+val.acct_status+'</td> ' +
            										'<td> ' +
                                    '<div id="user-status-block-'+val.id+'" class="user-status-block-wrapper" data-user-id="'+val.id+'"> ' +
                                        '<select class="target-account-status"> ' +
                                        		'<option value="novalue">Change Status</option> ' +
                                        		'<option value="active">Active</option> ' +
                                        		'<option value="inactive">Inactive</option> ' +
                                        '</select> ' +
                                        '<button class="btn btn-sm btn-default change-account-status" data-changed-user-id="'+val.id+'">GO!</button> ' +
                                    '</div> ' +
                                '</td> ' +
            										'<td><span class="btn btn-sm btn-danger remove-user-account" data-user-id="'+val.id+'">Delete Account</span></td> ' +
            								'</tr>'
                        );
                    });
                    var targetAccountStatus = $('.user-status-block-wrapper');
                    var goChangeAccountStatus = $('.change-account-status');
                    var removeUserAccount = $('.remove-user-account');
                    // var changeAccountStatus = $('.change-account-status');
                    goChangeAccountStatus.click(function() {
                        var currentSelectedUserId = $(this).parent()[0].dataset.userId;
                        // console.log('currentSelectedUserId');
                        // console.log(currentSelectedUserId);

                        var currentSelectedStatus = $(this).parent()[0].children[0].value;
                        // console.log('currentSelectedStatus');
                        // console.log(currentSelectedStatus);

                        // set the vaules to the account status update form and submit it
                        $('#user-account-status-user-id').val(currentSelectedUserId);
                        $('#user-account-status').val(currentSelectedStatus);
                        $('#user-account-status-submit-button').click();
                    });

                    removeUserAccount.click(function() {
                        var currentDeleteId = $(this)[0].dataset.userId;
                        // console.log(currentDeleteId);

                        // set the vaules to the delete form and submit it
                        $('#user-delete-id').val(currentDeleteId);
                        $('#user-account-delete-submit-button').click();
                    });
                } else {
                    console.log('Could not get users due to your access level');
                }
            } else {
                var errObject = {
                    success: false,
                    message: 'Could not find any categories.',
                    err: err
                };
                // console.log('failure');
                // console.log(errObject);
            }
        });
    }
    // ----------------------------------------
    // ----------------------------------------
    // INITIALIZE PAGE COMPONENTS based on if their page wrapper id returns true.
    // ----------------------------------------
    // START PUBLIC PAGES
    // ----------------------------------------
    if (vm.vars.publicPageRoutes.catalog) {
        getCategories();
        // console.log('Catalog Page is active!');
    }
    // ----------------------------------------
    if (vm.vars.publicPageRoutes.showProducts) {
        console.log('Show products Page is active!');

        var params = vm.vars.getUriParameters(location.search);
        var foundUriParam = params['type'];
        // Grab all categories to populate the menu. **** Cache this later  ****
        getCategories();
        // Get specific category
        console.log(foundUriParam);
        getSpecificCategory(foundUriParam);
    }
    // ----------------------------------------
    if (vm.vars.publicPageRoutes.showSingle) {
        console.log('Show single product Page is active!');

        var params = vm.vars.getUriParameters(location.search);
        var foundUriParam = params['id'];
        console.log('foundUriParam');
        console.log(foundUriParam);

        // Grab all categories to populate the menu. **** Cache this later  ****
        getCategories();
        // Get product by id
        getProductById(foundUriParam);
    }
    // ----------------------------------------
    if (vm.vars.publicPageRoutes.allCaskets) {
        // Grab all categories to populate the menu. **** Cache this later  ****
        getCategories();
        getAllCaskets();
    }
    // ----------------------------------------
    if (vm.vars.publicPageRoutes.login) {
        console.log('Login Page is active!');
        var loginSubmitButton = $('#contact-submit');
        var loginUsername = $('#contact-username');
        var loginPassword = $('#contact-password');
        // create the click function for submitting the data
        loginSubmitButton.click(function() {
            // validate each field (Do this later)

            // collect the data and place it into an object
            var loginObj = {
                username: loginUsername.val(),
                password: loginPassword.val()
            };
            console.log('loginObj');
            console.log(loginObj);
            // make the http request to login
            var query;
            if (currentEnv === 'localEnv') {
                query = localQueryHelper+'login_validation';
            } else {
                query = liveQueryHelper+'login_validation';
            }
            $.ajax({
                // url: siteUrlHelper+'get_json_product',
                url: query,
                dataType: 'json',
                data: {
                    'loginObj': loginObj
                }
            })
            .then(function(data) {
                console.log(data);
                // if (data) {
                //
                // } else {
                //     var errObject = {
                //         success: false,
                //         message: 'Could not find any categories.',
                //         err: err
                //     };
                //     console.log('failure');
                //     console.log(errObject);
                // }
            });
        });
    }
    // ----------------------------------------
    // ----------------------------------------
    // END PUBLIC PAGES
    // ----------------------------------------
    // ----------------------------------------
    // START PRIVATE PAGES
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.mainMenu) {
        console.log('Private Main Menu" is active!');
        getAdminCategories();
        $('#main-menu-all-caskets-div').append(
            '<a class="btn btn-info" href="'+publicPageRouteHelper+'allcaskets.php">Show All Caskets</a> <br><br>'
        );
        // $('#main-menu-all-caskets-div').append(
        //     '<a href="allcaskets.php">Show All Caskets</a> <br><br>'
        // );
    }
    // ----------------------------------------
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.imageLibrary) {
        console.log('Private Image Library" is active!');
        getAdminImageLibrary();
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.addItemWrapper) {
        console.log('Add Product / Item" is active!');
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.addCategoryWrapper) {
        console.log('Add Category" is active!');
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.showAdminProducts) {
        console.log('Show Admin Products" is active!');
        var params = vm.vars.getUriParameters(location.search);
        var foundUriParam = params['type'];
        console.log(foundUriParam);
        getAdminSpecificCategorySuperAdmin(foundUriParam);
        getAdminCategories();
        getAdminImageLibrary();
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.adminShowSingle) {
        console.log('Show and Admin single product Page is active!');

        var params = vm.vars.getUriParameters(location.search);
        var foundUriParam = params['id'];
        console.log('foundUriParam');
        console.log(foundUriParam);

        // Grab all categories to populate the menu. **** Cache this later  ****
        getAdminCategories();
        // Get product by id
        getAdminProductById(foundUriParam);
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.adminEditMasterInfo) {
        console.log('Edit your admin information');

        // Update an Admin by id
        adminUpdateMasterInfo();
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.adminRemoveACategory) {
        console.log('Admin Remove a Category.');
        categoriesRemoval = true;
        // Grab all categories to populate the menu. **** Cache this later  ****
        getAdminCategories();
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.adminManageUserAccounts) {
        getAdminUserAccountsData();
    }
    // ----------------------------------------
    if (vm.vars.privatePageRoutes.adminGetUserAccountHistory) {
        console.log('Get User Account History');

        var params = vm.vars.getUriParameters(location.search);
        var foundUriParam = params['adminusermanage'];
        console.log(foundUriParam);
        getAdminUserAccountHistoryData(foundUriParam);
    }
    // ----------------------------------------
    // END PRIVATE PAGES
    // ----------------------------------------
    // ----------------------------------------
    // ----------------------------------------

});
