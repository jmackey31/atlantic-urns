'use strict';

angular.module('fileReaderDir', [])
  // image upload directive
  .directive("fileread", [function ($rootScope) {
      return {
          scope: {
              fileread: "="
          },
          link: function (scope, element, attributes) {
              element.bind("change", function (changeEvent) {
                  var reader = new FileReader();
                  reader.onload = function (loadEvent) {
                      // console.log('loadEvent');
                      // console.log(loadEvent);
                      scope.$apply(function () {
                          // scope.fileread = loadEvent.target.result;
                          scope.fileread = loadEvent;
                      });
                  }
                  reader.readAsDataURL(changeEvent.target.files[0]);
              });
          }
      }
  }
]);
