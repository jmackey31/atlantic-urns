angular.module('formFactory', [])
    .factory('FormFieldValidationFactory', function() {

        return {
            // ____________________________________________________

            validateAnyValue: function(valueToCheck) {
                if (
                    (valueToCheck !== null) &&
                    (valueToCheck !== 'null') &&
                    (valueToCheck !== '0') &&
                    (valueToCheck !== 0) &&
                    (valueToCheck !== '') &&
                    (valueToCheck !== 'none') &&
                    (valueToCheck !== undefined) &&
                    (valueToCheck !== 'undefined') &&
                    (valueToCheck !== 'empty') &&
                    (valueToCheck !== false) &&
                    (valueToCheck !== 'false') &&
                    (valueToCheck !== 'Select State') &&
                    (valueToCheck !== 'Select City')
                  ) {
                    // console.log('valueToCheck returned TRUE see it here:');
                    // console.log(valueToCheck);
                    return true;
                } else {
                    // console.log('valueToCheck returned FALSE see it here:');
                    // console.log(valueToCheck);
                    return false;
                }
            },

            // ____________________________________________________

            // Check That Field is not empty function
            checkFieldNotEmpty: function(fieldVal) {
                if ((fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal !== null)) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Check That Field is not empty function minimum of 3 charaters
            checkFieldNotEmptyAndMin3Characters: function(fieldVal) {
                if ((fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal.length > 2) &&
                    (fieldVal !== null)) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Check That Field is not empty function minimum of 8 charaters
            checkFieldNotEmptyAndMin8Characters: function(fieldVal) {
                if ((fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal.length > 7) &&
                    (fieldVal !== null)) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Check that an email address is valid
            emailRegex: function(fieldVal) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(fieldVal);
            },

            checkValidEmail: function(fieldVal) {
                if (this.emailRegex(fieldVal) != false) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Phone number regex function
            phoneNumberRegex: function(fieldVal) {
                var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                return re.test(fieldVal);
            },

            // Validate that 10 digit phone number is true
            checkValidPhoneNumber: function(fieldVal) {
                if ((this.phoneNumberRegex(fieldVal) != false) &&
                    (fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal !== null)) {
                    return true;
                }
                else {
                    return false;
                }
            },

            // ____________________________________________________

            // Validate that field is a number integer
            checkNumberValue: function(fieldVal) {
                if ((Number.isInteger(fieldVal)) &&
                    (fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal !== null)) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Validate that field is a number integer
            checkNumberPriceValue: function(fieldVal) {
                console.log(fieldVal);
                console.log('fieldVal');
                var newNumberVal = Number(fieldVal);
                if (newNumberVal === 0) {
                    console.log('This value is 0');
                    // Just allow this to pass through
                    return true;
                } else if ((Number.isInteger(newNumberVal)) &&
                    (newNumberVal !== '') &&
                    (newNumberVal !== undefined) &&
                    (newNumberVal !== null)) {
                    return true;
                } else {
                    console.log('This just wont pass.');
                    return false;
                }
            },

            // ____________________________________________________

            // Check Zip Code String Length
            checkZipCode: function(fieldVal) {
                var zipVal = fieldVal;
                if (((/^\s*\d{5}\s*$/.test(zipVal)) != false) &&
                    (fieldVal !== '') &&
                    (fieldVal !== undefined) &&
                    (fieldVal !== null)) {
                    return true;
                } else {
                    return false;
                }
            },

            // ____________________________________________________

            // Check that Password fields are identical, match values and length
            identicalPasswords: function(fieldVal, fieldValMatch) {
                var val1 = fieldVal,
                    val2 = fieldValMatch;

                function checkPasswordLength(valToCheck) {
                    var checkMe = valToCheck;
                    if (checkMe.length >= 8) {
                        return true;
                    } else {
                        return false;
                    }
                }

                if (val2 == val1) {
                    if ((this.checkFieldNotEmpty(fieldVal)) &&
                        (checkPasswordLength(fieldVal))) {
                        return true;
                    }
                } else {
                    return false;
                }
            }
            // ____________________________________________________

        };

    });
