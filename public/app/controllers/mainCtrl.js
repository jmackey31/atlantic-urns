angular.module('mainCtrl', ['httpService','formFactory'])
    .controller('mainController', function(
      $rootScope,
      $scope,
      $state,
      $stateParams,
      $window,
      $location,
      Auth,
      HttpService,
      FormFieldValidationFactory,
      $http,
      $sce
    ) {

        // ----------------------------------------------------------------------------
        var vm = this;

        vm.vars = {
            // get info if a person is logged in
            loggedIn: Auth.isLoggedIn(),
            currentState: $state.$current.path[0].self.name,
            userId: $window.localStorage.getItem('userId'),
            firstname: $window.localStorage.getItem('firstname'),
            lastname: $window.localStorage.getItem('lastname'),
            userType: $window.localStorage.getItem('userType'),
            username: $window.localStorage.getItem('username'),
            user_email: $window.localStorage.getItem('email'),
            access_level: $window.localStorage.getItem('accesslevel'),
            user_type_1_active: false,
            user_type_2_3_active: false,
            user_type_4_active: false,
            user_type_5_active: false,
            check_user_type: function() {
                // console.log(this.access_level);
                // console.log('Type of user logged in above:');
                if (this.access_level === '1') {
                    this.user_type_1_active = true;
                } else if ((this.access_level === '2') || (this.access_level === '3')) {
                    this.user_type_2_3_active = true;
                } else if (this.access_level === '4') {
                    this.user_type_4_active = true;
                } else if (this.access_level === '5') {
                    this.user_type_5_active = true;
                }
            },
            siteMessage: false,
            siteClass: false,
            processing: false,
            contactFormActive: true,
            formActive: true,
            accountStatusUpdate: false,
            accountDelete: false,
            editCategory: false,
            editProduct: false,
            editImageInfo: false,
            temporaryImageId: null,
            deleteProduct: false,
            deleteCategory: false,
            optOutOption: false,
            sideBarNav: false,
            currentCustomerProductPrice: false,
            adminCategoryObj: {},
            adminProductObj: {},
            priceAdjustments: {},
            siteMasterObj: {},
            userTypeObj: {
                admin: 'admin',
                salesRep: 'salesRep',
                funeralHome: 'funeralHome'
            },
            uploadImageObj: {},
            taUser: {},
            usersObj: {},
            funeralHome: {},
            salesRep: {},
            publicUser: {},
            categoryObj: {},
            productObj: {},
            searchProductsObj: {},
            taValidate: FormFieldValidationFactory,
            todaysFormattedDate: moment().format('L'),
            yesterdaysFormattedDate: moment().subtract(1, 'days').format('L')
        };

        // ----------------------------------------------------------------------------
        vm.toggleSideBarNav = function() {
            if (vm.vars.sideBarNav === true) {
                vm.vars.sideBarNav = false;
                $('.sidebar-nav-custom-class').removeClass('sidebar-nav-active');
            } else {
                vm.vars.sideBarNav = true;
                $('.sidebar-nav-custom-class').addClass('sidebar-nav-active');
            }
        };

        // ----------------------------------------------------------------------------
        vm.activateDropdownNav = function() {
            // console.log('vm.vars.navsUlActive var:');
            // console.log(vm.vars.navsUlActive);
            if (vm.vars.navsUlActive) {
                vm.vars.navsUlActive = false;
            } else {
                vm.vars.navsUlActive = true;
            }
        };

        // ----------------------------------------------------------------------------
        // User navigations arrays
        vm.user1Array = [
            {
                link: 'main-menu',
                display: 'Main Menu'
            },
            {
                link: 'search-products',
                display: 'Search Products'
            },
            {
                link: 'add-item',
                display: 'Add Item'
            },
            {
                link: 'add-cat',
                display: 'Add Category'
            },
            {
                link: 'create-sales-rep',
                display: 'Create Sales Rep'
            },
            {
                link: 'create-funeral-home',
                display: 'Create Funeral home'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                link: 'remove-category',
                display: 'Remove A Category'
            },
            {
                link: 'manage-user-accounts',
                display: 'Manage User Accounts'
            },
            {
                link: 'master-price-increase',
                display: 'Rate / Price Increase'
            },
            {
                link: 'login-reports',
                display: 'Login Reports'
            },
            {
                link: 'upload-media',
                display: 'Upload Media Library'
            },
            {
                link: 'change-admin-password',
                display: 'Change Administrator Password'
            },
            {
                link: 'list-user-credentials',
                display: 'View User Credentials'
            },
            {
                link: 'mailing-list-emails',
                display: 'Mailing List Emails'
            }
        ];
        vm.user2And3Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                display: 'Price Management',
                link: 'price-managment'
            },
            // {
            //     display: 'Retail Price Report',
            //     link: 'retail-price-report'
            // },
            // {
            //     display: 'Selection Room Management',
            //     link: 'selection-room-management'
            // },
            {
                display: 'Public Account Setup',
                link: 'account-setup'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user4Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user5Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];

        // ----------------------------------------------------------------------------
        vm.scrollPageToTop = function() {
            var siteHeader = $('.site-header');
            $('html, body').animate({
                scrollTop: siteHeader.offset().top
            }, 1000);
        };
        vm.scrollPageToUpperSection = function() {
            $('html, body').animate({
                scrollTop: 0
                // scrollTop: upperSection.offset().top
            }, 500);
        };

        // ----------------------------------------------------------------------------
        vm.initMainSlider = function() {
            $(document).ready(function() {
                $('.triangle-main-slider').slick({
                    arrows: true,
                    dots: true,
                    infinite: true,
                    speed: 500,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    slidesToShow: 1
                });
            });
        };

        // ----------------------------------------------------------------------------
        vm.getAllUsers = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (vm.vars.loggedIn) {
                var postInfo = {
                    obj: true,
                    uri: 'getallusers',
                    user_id: vm.vars.userId
                };
                // console.log(postInfo);

                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        // console.log(data.data);
                        if (data.data.success) {
                            vm.allUsersObj = data.data.allusers;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            } else {
                vm.vars.siteMessage = 'Please log in to view this page.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.dummySalesRep = function() {
            console.log('Create that dummy sales rep.');
            vm.vars.salesRep = {
                address_1: '1234 Rentrium Road',
                address_2: 'Suite 81',
                city: 'Newport News',
                contact_person: 'Ron Donaldson',
                email: 'rondon55@gmail.com',
                password: 'rondon55',
                phone: 5553338888,
                state: 'VA',
                username: 'rondon55',
                zip: 37488
            };
        };
        vm.createSalesRep = function(salesRepObj) {
            // console.log(salesRepObj);
            // console.log('New salesRepObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var salesRepReady = false;
            var contact_person,
                phone,
                address_1,
                city,
                state,
                zip,
                username,
                password,
                email = false;

            if (salesRepObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidPhoneNumber(salesRepObj.phone)) {
                    phone = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Phone Number" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.address_1)) {
                    address_1 = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Address" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(salesRepObj.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.state)) {
                    state = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "State" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.city)) {
                    city = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "City" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkZipCode(salesRepObj.zip)) {
                    zip = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Zip" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(salesRepObj.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(salesRepObj.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (phone === true) &&
                    (email === true) &&
                    (address_1 === true) &&
                    (state === true) &&
                    (city === true) &&
                    (zip === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    salesRepReady = true;
                }

                if (salesRepReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewuseraccount',
                        salesRepObj: salesRepObj,
                        usernameCheck: salesRepObj.username,
                        account_type: 'salesrep',
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.salesRep = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.createPublicUser = function(publicUser) {
            // console.log(publicUser);
            // console.log('New publicUser');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var publicUserReady = false;
            var contact_person,
                username,
                password,
                email = false;

            if (publicUser) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(publicUser.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(publicUser.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(publicUser.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(publicUser.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (email === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    publicUserReady = true;
                }

                if (publicUserReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewpublicuser',
                        publicUser: publicUser,
                        usernameCheck: publicUser.username,
                        account_type: 'public_user',
                        user_id: vm.vars.userId,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo above:');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.publicUser = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.dummyFuneralHome = function() {
            console.log('Create that dummy funeral home.');
            vm.vars.funeralHome = {
                address_1: '205 Park Ave',
                address_2: 'Apt 4',
                city: 'Elkhart',
                contact_person: 'Bonny Henderson',
                email: 'bonnybonny@gmail.com',
                fac_name: 'Bonny Bonny Funeral Home',
                password: 'bonnybonny',
                phone: 3733384444,
                state: 'IN',
                username: 'bonnybonny',
                zip: 46516
            };
        };
        vm.createFuneralHome = function(funeralHomeObj) {
            // console.log(funeralHomeObj);
            // console.log('New funeralHomeObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var funeralHomeReady = false;
            var contact_person,
                fac_name,
                phone,
                address_1,
                city,
                state,
                zip,
                username,
                password,
                email = false;

            if (funeralHomeObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.fac_name)) {
                    fac_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Funeral Home Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidPhoneNumber(funeralHomeObj.phone)) {
                    phone = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Phone Number" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.address_1)) {
                    address_1 = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Address" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(funeralHomeObj.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.state)) {
                    state = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "State" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.city)) {
                    city = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "City" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkZipCode(funeralHomeObj.zip)) {
                    zip = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Zip" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(funeralHomeObj.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(funeralHomeObj.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (fac_name === true) &&
                    (phone === true) &&
                    (email === true) &&
                    (address_1 === true) &&
                    (state === true) &&
                    (city === true) &&
                    (zip === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    funeralHomeReady = true;
                }

                if (funeralHomeReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewuseraccount',
                        funeralHomeObj: funeralHomeObj,
                        usernameCheck: funeralHomeObj.username,
                        account_type: 'funeral_home',
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.funeralHome = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.anyUserLogin = function(userInfo) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.taValidate.validateAnyValue(userInfo.username)) {
                vm.vars.siteMessage = '"Username" field is required!  Please type your username.';
                vm.vars.siteClass = 'alert alert-danger';
            } else if (!vm.vars.taValidate.validateAnyValue(userInfo.password)) {
                vm.vars.siteMessage = '"Password" field is required!';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // console.log(userInfo);
                // console.log('userInfo above:');
                vm.vars.processing = true;

                var formattedDate = vm.vars.todaysFormattedDate;

                Auth.login(userInfo.username, userInfo.password, formattedDate)
                    .then(function(data) {
                        // console.log(data);
                        vm.vars.processing = false;
                        // if a user successfully logs in, redirect to users page
                        if (data.data.success) {
                            // console.log('go to the search page');
                            $location.path('/main-menu');
                            // --------------------------------
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // // ----------------------------------------------------------------------------
        // vm.contactTa = function(contactObj) {
        //     // console.log(contactObj);
        //     var name, email, phone, message;
        //     var formSendReady = false;
        //     // validate contact form
        //     // Clear the success and failure vars
        //     vm.vars.successMessage = false;
        //     vm.vars.errorMessage = false;
        //     if (contactObj !== undefined) {
        //         if (vm.vars.taValidate.checkFieldNotEmpty(contactObj.name)) {
        //             name = contactObj.name;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your name.';
        //         }
        //         if (vm.vars.taValidate.checkValidEmail(contactObj.email)) {
        //             email = contactObj.email;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your email address.';
        //         }
        //         if (contactObj.phone) {
        //             if (vm.vars.taValidate.checkValidPhoneNumber(contactObj.phone)) {
        //                 phone = contactObj.phone;
        //             } else {
        //                 vm.vars.errorMessage = 'Please fix your phone number.';
        //             }
        //         }
        //         if (vm.vars.taValidate.checkFieldNotEmpty(contactObj.message)) {
        //             message = contactObj.message;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your message.';
        //         }
        //     } else {
        //         vm.vars.errorMessage = 'Please fill out the form completely.';
        //     }
        //     // If all checks out
        //     if ((name !== undefined) && (email !== undefined) && (message !== undefined)) {
        //         formSendReady = true;
        //     }
        //
        //     if (formSendReady) {
        //         var postInfo = {
        //             obj: true,
        //             uri: 'contactta',
        //             name: name,
        //             email: email,
        //             phone: phone,
        //             message: message
        //         };
        //         // console.log(postInfo);
        //         HttpService.postRequest(postInfo)
        //             .then(function(data) {
        //                 // console.log(data);
        //                 if (data.data.success) {
        //                     vm.vars.successMessage = data.data.message;
        //                 } else {
        //                     // console.log('error dude.');
        //                     // console.log(data);
        //                     vm.vars.errorMessage = data.data.message;
        //                 }
        //                 // close the contact form
        //                 vm.vars.contactFormActive = false;
        //             });
        //     }
        // };

        // ----------------------------------------------------------------------------
        vm.getUserAdminInfo = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to view this page.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    obj: true,
                    uri: 'getuserinfo',
                    user_id: vm.vars.userId
                };
                // console.log(postInfo);
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.userObj = data.data.userObj;
                            vm.vars.usersObj = data.data.userObj;
                            vm.vars.priceAdjustments = data.data.userObj;
                            vm.vars.optOutOption = vm.vars.priceAdjustments.opt_out_base_price_increases;
                            vm.vars.siteMasterObj = data.data.siteMasterObj;
                            console.log(vm.vars.optOutOption);
                            console.log('vm.vars.optOutOption above:');
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.newUserObj = {};
        vm.changeAdminPassword = function(newUserObj) {
            // console.log(newUserObj);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (newUserObj)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if ((newUserObj.new_password) && (newUserObj.matched_password)) {
                    if ((vm.vars.taValidate.validateAnyValue(newUserObj.new_password)) &&
                        (vm.vars.taValidate.identicalPasswords(newUserObj.new_password, newUserObj.matched_password))) {
                        var putInfo = {
                            uri: 'changemypassword',
                            param: vm.vars.userId,
                            newPassword: newUserObj.new_password
                        };
                        // console.log(putInfo);
                        HttpService.putRequest(putInfo)
                            .then(function(data) {
                                if (data.data.success) {
                                    vm.userObj = data.data.userObj;
                                    vm.vars.siteMessage = data.data.message;
                                    vm.vars.siteClass = 'alert alert-success';
                                } else {
                                    vm.vars.siteMessage = data.data.message;
                                    vm.vars.siteClass = 'alert alert-danger';
                                }
                            });
                    } else {
                        vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                } else {
                    vm.vars.siteMessage = 'Please completely fill out this form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.uploadToImageLibrary = function(imageObj) {
            // console.log(imageObj);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (imageObj)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if ((imageObj.media_thumbnail) &&
                    (vm.vars.taValidate.validateAnyValue(imageObj.image_name)) &&
                    (vm.vars.taValidate.validateAnyValue(imageObj.image_caption))) {
                    var postInfo = {
                        obj: true,
                        uri: 'uploadmedia',
                        imageFile: imageObj.media_thumbnail.raw_file.target.result,
                        imageObj: imageObj
                    };
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                // console.log(data.data);
                                vm.vars.uploadImageObj = {};
                                // // reload the page
                                // location.reload();
                                vm.getImageLibrary();
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please fill out this form completely.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.getImageLibrary = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    singleEndPoint: true,
                    uri: 'getmedialibrary'
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.mediaObj = data.data.gallery;
                            vm.imageObjPagination = data.data.pagination;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.cancelEditing = function() {
            // console.log('Cancel image editing.');
            vm.vars.editImageInfo = false;

            // Clear the image values
            vm.vars.temporaryImageId = null;
            vm.vars.uploadImageObj.image_name = '';
            vm.vars.uploadImageObj.image_caption = '';
        };
        // ----------------------------------------------------------------------------
        vm.editImageMedia = function(image) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            // console.log(image);
            // console.log('image above:');

            // Hide the image input field
            vm.vars.editImageInfo = true;

            // Bind the values to the image
            vm.vars.temporaryImageId = image._id;
            vm.vars.uploadImageObj.image_name = image.image_name;
            vm.vars.uploadImageObj.image_caption = image.image_caption;
        };

        // ----------------------------------------------------------------------------
        vm.updateImageMedia = function(image) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var putInfo = {
                    uri: 'updateimagemedia',
                    param: vm.vars.temporaryImageId,
                    image_name: vm.vars.uploadImageObj.image_name,
                    image_caption: vm.vars.uploadImageObj.image_caption
                };
                // console.log(putInfo);
                // console.log('putInfo');
                HttpService.putRequest(putInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.vars.uploadImageObj = {};
                            // // reload the page
                            // location.reload();
                            vm.getImageLibrary();
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-success';
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.removeImageMedia = function(imageId) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    paramObj: true,
                    uri: 'deleteimagemedia',
                    param: imageId
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.getImageLibrary();
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-success';
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getPagedImageLibrary = function(pageInfo) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    obj: true,
                    uri: 'getpagedmedialibrary',
                    pageInfo: pageInfo
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.mediaObj = data.data.gallery;
                            vm.imageObjPagination = data.data.pagination;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.cancelAnyMangement = function() {
            vm.vars.accountDelete = false;
            vm.vars.accountStatusUpdate = false;
        };
        // ----------------------------------------------------------------------------
        vm.showAccountToDelete = function(account) {
            vm.cancelAnyMangement();
            // console.log(account);
            // console.log('the user account above.');
            vm.vars.accountDelete = {
                user_id: account._id,
                username: account.username
            };
        };
        vm.finalDeleteUserAccount = function(account) {
            // console.log(account);
            // console.log('account object to use for server.');
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (account)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if (vm.vars.taValidate.validateAnyValue(account.user_id)) {
                    var postInfo = {
                        paramObj: true,
                        uri: 'deleteuseraccount',
                        param: vm.vars.userId,
                        id_to_delete_on: account.user_id
                    };
                    // console.log(postInfo);
                    // console.log('postInfo for status change.');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.cancelAnyMangement();
                                vm.getUserAccounts();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.viewPrepUserStatus = function(account) {
            vm.cancelAnyMangement();
            // console.log(account);
            // console.log('the user account above.');
            vm.vars.accountStatusUpdate = {
                user_id: account._id,
                username: account.username,
                account_status: String(account.account_status)
            };
        };
        vm.changeUserStatus = function(account) {
            // console.log(account);
            // console.log('account object to use for server.');
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (account)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if (vm.vars.taValidate.validateAnyValue(account.account_status)) {
                    var putInfo = {
                        uri: 'updateuseraccountstatus',
                        param: vm.vars.userId,
                        id_to_update_on: account.user_id,
                        account_status: parseInt(account.account_status)
                    };
                    // console.log(putInfo);
                    // console.log('putInfo for status change.');
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                // vm.allUserAccountsArray = data.data.allUserAccounts;
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.cancelAnyMangement();
                                vm.getUserAccounts();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.getSingleUserLoginReport = function() {
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    paramObj: true,
                    uri: 'singleloginhistoryreport',
                    param: vm.vars.userId,
                    id_to_report: $stateParams.user_id
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.singleUserLoginReport = data.data.singlereport;
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getUserLoginReports = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    paramString: true,
                    uri: 'getloginhistoryreports',
                    param: vm.vars.userId
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.loginReportsArray = data.data.allreports;
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getUserAccounts = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    paramString: true,
                    uri: 'getuseraccounts',
                    param: vm.vars.userId,
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.allUserAccountsArray = data.data.allUserAccounts;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getCategories = function() {
            // console.log('Get all categories');
            var getInfo = {
                singleEndPoint: true,
                uri: 'getallcategories'
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.allProductCategories = data.data.allProductCategories;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAdminCategories = function() {
            // console.log('Get all categories');
            var postInfo = {
                paramObj: true,
                uri: 'getadmincategories',
                param: vm.vars.userId
            };
            // console.log(postInfo);
            // console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.adminCategories = data.data.adminCategories;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getProductsByCategoryCode = function() {
            var postInfo = {
                paramObj: true,
                uri: 'getproductsbycategorycode',
                param: $stateParams.category_code,
                page: 1
            };
            // console.log(postInfo);
            // console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.productsFoundByCode = data.data.allProducts;
                        vm.allCategoryProductsPagination = data.data.pagination;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAllCategoryProductsPaged = function(pageInfo) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var postInfo = {
                paramObj: true,
                uri: 'getproductsbycategorycode',
                param: $stateParams.category_code,
                page: pageInfo
            };
            // console.log(postInfo);
            // console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.productsFoundByCode = data.data.allProducts;
                        vm.allCategoryProductsPagination = data.data.pagination;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getSingleProductByCode = function() {
            console.log('getSingleProductByCode func fired.');
            var getInfo = {
                paramString: true,
                uri: 'getsingleproductbycode',
                param: $stateParams.product_code
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.activeSingleProduct = data.data.activeSingleProduct;
                        // vm.vars.usersObj = data.data.userObj;
                        // console.log(vm.vars.usersObj);
                        // console.log('vm.vars.usersObj above:');

                        function getYourProductPrice(base, markup) {
                            console.log(base);
                            console.log('base basic');
                            console.log(markup);
                            console.log('markup basic');
                            if (markup === undefined) {
                                markup = 0;
                            }
                            var yourPrice;
                            var percentageCalculation = base * markup / 100;
                            // console.log(percentageCalculation);
                            // console.log('percentageCalculation');
                            yourPrice = Number(base) + Number(percentageCalculation);
                            // console.log(yourPrice);
                            // console.log('yourPrice');

                            return yourPrice;
                        }

                        function getFuneralHomeProductPrice(base, sitemastermarkup) {
                            console.log(base);
                            console.log('base basic');
                            console.log(sitemastermarkup);
                            console.log('sitemastermarkup basic');
                            if (sitemastermarkup === undefined) {
                                sitemastermarkup = 0;
                            }
                            var yourPrice;
                            var percentageCalculation = base * sitemastermarkup / 100;
                            // console.log(percentageCalculation);
                            // console.log('percentageCalculation');
                            yourPrice = Number(base) + Number(percentageCalculation);
                            // console.log(yourPrice);
                            // console.log('yourPrice');

                            return yourPrice;
                        }

                        function getYourProductPriceWithSiteMaster(base, markup, sitemastermarkup) {
                            console.log(base);
                            console.log('base advanced sitemaster');
                            console.log(markup);
                            console.log('markup advanced sitemaster');
                            console.log(sitemastermarkup);
                            console.log('sitemastermarkup advanced sitemaster');
                            if (markup === undefined) {
                                markup = 0;
                            }
                            if (sitemastermarkup === undefined) {
                                sitemastermarkup = 0;
                            }
                            var yourPrice;
                            // var percentageCalculation = base * markup / 100;
                            // var siteMasterPercentageCalculation = base * sitemastermarkup / 100;
                            // combine both percentages into 1 percentage sum before calculating the price
                            var overallMarkup = markup + sitemastermarkup;
                            var percentageCalculation = base * overallMarkup / 100;
                            var siteMasterPercentageCalculation = base * sitemastermarkup / 100;
                            yourPrice = Number(base) + Number(percentageCalculation) + Number(siteMasterPercentageCalculation);

                            return yourPrice;
                        }
                        // vm.vars.currentCustomerProductPrice = 77;
                        if (vm.vars.access_level === '5') {
                            console.log('PUBLIC user viewing price.  Please waitâ€¦');
                            function customerViewPrice() {
                                // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                                // console.log(vm.vars.currentCustomerProductPrice);
                                // console.log('vm.vars.currentCustomerProductPrice');

                                console.log(vm.vars.priceAdjustments);
                                console.log('vm.vars.priceAdjustments above:');

                                // go and get the price point
                                var getInfo = {
                                    paramString: true,
                                    uri: 'getuserpricepoint',
                                    param: vm.vars.usersObj.parent_account.user_code
                                };
                                // console.log(getInfo);
                                // console.log('getInfo above:');
                                HttpService.getRequest(getInfo)
                                    .then(function(data) {
                                        if (data.data.success) {
                                            vm.userPriceObj = data.data.userPriceObj;
                                            vm.siteMasterProductRate = data.data.siteMasterObj.master_product_price_markup;
                                            // if site master markup is > 0 do this.
                                            if (vm.siteMasterProductRate > 0) {
                                                console.log('Add the site master pricing.');
                                                var xBasePrice = vm.activeSingleProduct.base_price;
                                                var xUserPriceRounding = vm.userPriceObj.price_rounding;
                                                var xSiteMasterMarkup = vm.siteMasterProductRate;
                                                vm.vars.currentCustomerProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                                console.log(vm.vars.currentCustomerProductPrice);
                                                console.log('vm.vars.currentCustomerProductPrice above:');
                                            } else {
                                                console.log('Normal pricing.  Would rarely happen.');
                                                vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.userPriceObj.price_rounding);
                                            }
                                        } else {
                                            vm.vars.siteMessage = data.data.message;
                                            vm.vars.siteClass = 'alert alert-danger';
                                            vm.currentCustomerProductPrice = 'N/A';
                                            console.log(vm.vars.currentCustomerProductPrice);
                                            console.log('vm.vars.currentCustomerProductPrice above:');
                                        }
                                    });
                            }
                            // Set a short Timeout for pagination elements
                            function startCustomerPriceTimer() {
                                var customerPriceTimer = window.setTimeout(
                                    customerViewPrice, 1000
                                );
                            }
                            startCustomerPriceTimer();
                        } else if ((vm.vars.access_level === '3') ||
                                   (vm.vars.access_level === '2')) {
                            console.log('Level 2 or 3 users viewing price.');
                            // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                            // console.log(vm.vars.currentCustomerProductPrice);
                            // console.log('vm.vars.currentCustomerProductPrice');

                            // go and get the price point
                            var getInfo = {
                                singleEndPoint: true,
                                uri: 'getmasterpricerate',
                            };
                            // console.log(getInfo);
                            // console.log('getInfo above:');
                            HttpService.getRequest(getInfo)
                                .then(function(data) {
                                    if (data.data.success) {
                                        // vm.userPriceObj = data.data.userPriceObj;
                                        vm.siteMasterProductRate = data.data.siteMasterObj.master_product_price_markup;
                                        // if site master markup is > 0 do this.
                                        var xBasePrice = vm.activeSingleProduct.base_price;
                                        // var xUserPriceRounding = vm.userPriceObj.price_rounding;
                                        var xUserPriceRounding = vm.vars.usersObj.price_rounding;
                                        var xSiteMasterMarkup = vm.siteMasterProductRate;
                                        if (vm.siteMasterProductRate > 0) {
                                            console.log('Add the site master pricing.');
                                            vm.yourCustomersProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                            vm.vars.currentCustomerProductPrice = getFuneralHomeProductPrice(xBasePrice, xSiteMasterMarkup);
                                            console.log(vm.vars.currentCustomerProductPrice);
                                            console.log('vm.vars.currentCustomerProductPrice above:');
                                        } else {
                                            console.log('Normal pricing.  Would rarely happen.');
                                            // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.userPriceObj.price_rounding);
                                            vm.yourCustomersProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                            vm.vars.currentCustomerProductPrice = vm.activeSingleProduct.base_price;
                                        }
                                    } else {
                                        vm.vars.siteMessage = data.data.message;
                                        vm.vars.siteClass = 'alert alert-danger';
                                        vm.currentCustomerProductPrice = 'N/A';
                                        console.log(vm.vars.currentCustomerProductPrice);
                                        console.log('vm.vars.currentCustomerProductPrice above last option:');
                                    }
                                });
                        } else {
                            // console.log('admin user viewing price.');
                            vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                            console.log(vm.vars.currentCustomerProductPrice);
                            console.log('vm.vars.currentCustomerProductPrice not a level 5, 3, or 2');
                        }
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAllProducts = function() {
            var postInfo = {
                obj: true,
                uri: 'getallproducts',
                page: 1
            };
            // console.log(postInfo);
            // console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.allProducts = data.data.allProducts;
                        vm.allProductsPagination = data.data.pagination;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAllProductsPaged = function(pageInfo) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var postInfo = {
                obj: true,
                uri: 'getallproducts',
                page: pageInfo
            };
            console.log(postInfo);
            console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.allProducts = data.data.allProducts;
                        vm.allProductsPagination = data.data.pagination;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.closeEditCategory = function() {
            // Show the category edit block
            vm.vars.editCategory = false;

            // Empty the category form
            vm.vars.adminCategoryObj = {};
        };

        // ----------------------------------------------------------------------------
        vm.editCategory = function(category) {
            // console.log(category);
            // console.log('category to edit above:');

            // Show the category edit block
            vm.vars.editCategory = true;

            // Bind the category to the edit form
            vm.vars.adminCategoryObj = category;

            // Scroll to the top of the page
            vm.scrollPageToUpperSection();
        };

        // ----------------------------------------------------------------------------
        vm.adminEditCategory = function(category) {
            // console.log(category);
            // console.log('category to edit above:');

            var formReady = false;

            if (category) {
                if ((vm.vars.taValidate.validateAnyValue(category.category_description)) &&
                    (vm.vars.taValidate.validateAnyValue(category.category_image))) {
                    formReady = true;
                } else {
                    vm.vars.siteMessage = 'Please fill out the form completely.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                vm.vars.siteMessage = 'Please fill out the form completely.';
                vm.vars.siteClass = 'alert alert-danger';
            }

            if (formReady === true) {
                var postInfo = {
                    obj: true,
                    uri: 'changesinglecategorydata',
                    category: category,
                    user_id: vm.vars.userId
                };
                console.log(postInfo);
                console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.closeEditCategory();
                            vm.getCategories();
                            // console.log(data.data);
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.closeEditProduct = function() {
            // Show the product edit block
            vm.vars.editProduct = false;

            // Empty the product form
            vm.vars.adminProductObj = {};
        };

        // ----------------------------------------------------------------------------
        vm.editProduct = function(product) {
            console.log(product);
            console.log('product to edit above:');

            // Show the product edit block
            vm.vars.editProduct = true;

            // Bind the product to the edit form
            vm.vars.adminProductObj = product;

            // Scroll to the top of the page
            vm.scrollPageToUpperSection();
        };

        // ----------------------------------------------------------------------------
        vm.adminEditProduct = function(product) {
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;
            console.log(product);
            console.log('product to edit above:');

            var formReady = false;
            var productReady = false;
            var category_code,
                interior,
                product_name,
                base_price,
                color_finish,
                thumbnail_image,
                material = false;

            if (product) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(product.category_code)) {
                    category_code = true;
                } else {
                    vm.vars.siteMessage = 'This product was not issued a "Category Code"';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.interior)) {
                    interior = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Interior" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.product_name)) {
                    product_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Product Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.base_price)) {
                    base_price = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Base Price" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.color_finish)) {
                    color_finish = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Color / Finish" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.thumbnail_image)) {
                    thumbnail_image = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Photo Image" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.material)) {
                    material = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Material" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((category_code === true) &&
                    (interior === true) &&
                    (product_name === true) &&
                    (base_price === true) &&
                    (color_finish === true) &&
                    (thumbnail_image === true) &&
                    (material === true)) {
                    productReady = true;
                }

                if (productReady === true) {
                    var postInfo = {
                        obj: true,
                        uri: 'changesingleproductdata',
                        product: product,
                        user_id: vm.vars.userId
                    };
                    console.log(postInfo);
                    console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.closeEditProduct();
                                vm.getCategories();
                                vm.getProductsByCategoryCode();
                                // console.log(data.data);
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.addProductImageString = function(imageObj) {
            console.log(imageObj);
            console.log('imageObj');
            vm.vars.adminProductObj.image_name = imageObj.image_name;
            vm.vars.adminProductObj.thumbnail_image = imageObj.file_name;
        };
        vm.dummyProductObj = function() {
            console.log('Create that dummy product.');
            vm.vars.productObj = {
                model_style: 'Hard Soft Exterior Style',
                material: 'Furr Ball',
                thumbnail_image: 'tacc-photo-1500004018885.jpg',
                image_name: 'Soft Shell Animal',
                color_finish: 'Gold and Grey',
                base_price: '2200',
                product_name: 'Furry Shelled Animal',
                interior: 'Soft shell grey plush',
                category_code: 'category_1500254635000670794'
            };
        };
        vm.createProduct = function(productObj) {
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var productReady = false;
            var category_code,
                // product_description,
                interior,
                product_name,
                base_price,
                color_finish,
                // image_name,
                thumbnail_image,
                material = false;
                // notes = false;

            if (productObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(productObj.category_code)) {
                    category_code = true;
                } else {
                    vm.vars.siteMessage = 'This product was not issued a "Category Code"';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.interior)) {
                    interior = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Interior" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.product_name)) {
                    product_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Product Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.base_price)) {
                    base_price = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Base Price" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.color_finish)) {
                    color_finish = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Color / Finish" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(vm.vars.adminProductObj.thumbnail_image)) {
                    thumbnail_image = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Photo Image" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.material)) {
                    material = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Material" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((category_code === true) &&
                    // (product_description === true) &&
                    (interior === true) &&
                    (product_name === true) &&
                    (base_price === true) &&
                    (color_finish === true) &&
                    (thumbnail_image === true) &&
                    (material === true)) {
                    productReady = true;
                }

                if (productReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;

                    var postInfo = {
                        paramObj: true,
                        uri: 'createproduct',
                        param: vm.vars.userId,
                        todaysFormattedDate: vm.vars.todaysFormattedDate,
                        productObj: productObj
                    };
                    // push the image into the object
                    if (vm.vars.adminProductObj.thumbnail_image) {
                        postInfo.productObj.image_name = vm.vars.adminProductObj.image_name;
                        postInfo.productObj.thumbnail_image = vm.vars.adminProductObj.thumbnail_image;
                    }
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.productObj = {};
                                vm.scrollPageToUpperSection();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.addCategoryImageString = function(imageObj) {
            console.log(imageObj);
            console.log('imageObj');
            vm.vars.categoryObj.category_image = imageObj.image_name;
            vm.vars.categoryObj.file_name = imageObj.file_name;
            vm.vars.adminCategoryObj.category_image = imageObj.file_name;
            vm.vars.adminCategoryObj.image_name = imageObj.image_name;
            vm.vars.adminProductObj.thumbnail_image = imageObj.file_name;
            vm.vars.adminProductObj.image_name = imageObj.image_name;
            vm.vars.productObj.thumbnail_image = imageObj.file_name;
            vm.vars.productObj.image_name = imageObj.image_name;
        };
        vm.createCategory = function(categoryObj) {
            // console.log(categoryObj);
            // console.log('categoryObj');
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                if ((categoryObj.category_description) && (categoryObj.file_name)) {
                var postInfo = {
                    paramObj: true,
                    uri: 'createcategory',
                    param: vm.vars.userId,
                    todaysFormattedDate: vm.vars.todaysFormattedDate,
                    categoryObj: categoryObj
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-success';
                            vm.vars.categoryObj = {};
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
                } else {
                    // Form is not valid
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.searchProducts = function(searchProductsObj) {
            // console.log(searchProductsObj);
            // console.log('New searchProductsObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var searchReady = false;

            if (searchProductsObj) {
                // validate fields.  Really just a check to see if any of these fields exist.
                if ((vm.vars.taValidate.validateAnyValue(searchProductsObj.product_name)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.material)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.color_finish)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.interior))) {
                    searchReady = true;
                } else {
                    vm.vars.siteMessage = 'Please fill out at least 1 input field to search on.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                if (searchReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'searchproductsuri',
                        searchProductsObj: searchProductsObj,
                        access_level: vm.vars.access_level
                    };
                    // console.log(postInfo);
                    // console.log('postInfo search object above.');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the form object
                                // vm.vars.searchProductsObj = {};
                                vm.returnedProductsArr = data.data.products;
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.returnedProductsArr = {};
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.searchUserAccounts = function(usersObj) {
            console.log(usersObj);
            console.log('usersObj above:');
            console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;
            var searchReady = false;

            if (usersObj) {
                // validate fields.  Really just a check to see if any of these fields exist.
                if ((vm.vars.taValidate.validateAnyValue(usersObj.access_level)) ||
                    (vm.vars.taValidate.validateAnyValue(usersObj.account_status)) ||
                    (vm.vars.taValidate.validateAnyValue(usersObj.username))) {
                    searchReady = true;
                } else {
                    vm.vars.siteMessage = 'Please fill out at least 1 input field to search on.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                if (searchReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        paramObj: true,
                        uri: 'searchuseraccounts',
                        param: vm.vars.userId,
                        usersObj: usersObj,
                        access_level: vm.vars.access_level
                    };
                    console.log(postInfo);
                    console.log('postInfo search object above.');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.allUserAccountsArray = data.data.allUserAccounts;
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.returnedProductsArr = {};
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.editCurrentUsersInfo = function(usersObj) {
            // console.log(usersObj);
            // console.log('New usersObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var editUserObjReady = false;
            var contact_person = false;

            if (usersObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(usersObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if (contact_person === true) {
                    // Form ready for updating
                    editUserObjReady = true;
                }

                if (editUserObjReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var putInfo = {
                        uri: 'updateuseraccount',
                        param: vm.vars.userId,
                        usersObj: usersObj,
                        account_type: vm.vars.usersObj.account_type,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(putInfo);
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.callDeleteProduct = function() {
            vm.vars.deleteProduct = true;
        };
        // ----------------------------------------------------------------------------
        vm.cancelDelete = function() {
            vm.vars.deleteProduct = false;
        };
        // ----------------------------------------------------------------------------
        vm.deleteProduct = function(product_code) {
            // console.log(product_code);
            // console.log('product_code above:');

            // activate loading icon
            vm.vars.processing = true;
            var postInfo = {
                paramObj: true,
                uri: 'deleteproduct',
                param: vm.vars.userId,
                product_code: product_code
            };
            // console.log(postInfo);
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    // Stop the processing icon
                    vm.vars.processing = false;
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                        vm.cancelDelete();
                        vm.getProductsByCategoryCode();
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                        vm.scrollPageToUpperSection();
                    }
                });
        };
        // ----------------------------------------------------------------------------
        vm.callDeleteCategory = function() {
            vm.vars.deleteCategory = true;
        };
        // ----------------------------------------------------------------------------
        vm.cancelDeleteCategory = function() {
            vm.vars.deleteCategory = false;
        };
        // ----------------------------------------------------------------------------
        vm.deleteCategory = function(category_id) {
            // console.log(cateogry_code);
            // console.log('cateogry_code above:');

            // activate loading icon
            vm.vars.processing = true;
            var postInfo = {
                paramObj: true,
                uri: 'deletecategory',
                param: vm.vars.userId,
                category_id: category_id
            };
            // console.log(postInfo);
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    // Stop the processing icon
                    vm.vars.processing = false;
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                        vm.cancelDeleteCategory();
                        vm.getAdminCategories();
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                        vm.scrollPageToUpperSection();
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.editPriceMarkup = function(priceObj) {
            // console.log(priceObj);
            // console.log('New priceObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (priceObj) {
                // validate fields
                if (FormFieldValidationFactory.checkNumberPriceValue(priceObj.price_rounding)) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var putInfo = {
                        uri: 'updatepricemarkup',
                        param: vm.vars.userId,
                        price_rounding: priceObj.price_rounding,
                        opt_out_base_price_increases: priceObj.opt_out_base_price_increases,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(putInfo);
                    // console.log('putInfo above: ');
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.priceAdjustments = {};
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.editMasterPriceMarkup = function(priceObj) {
            console.log(priceObj);
            console.log('New priceObj');
            console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (priceObj) {
                // validate fields
                if (FormFieldValidationFactory.checkNumberPriceValue(priceObj.master_product_price_markup)) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        paramObj: true,
                        uri: 'mastersiterateincrease',
                        param: vm.vars.userId,
                        master_product_price_markup: priceObj.master_product_price_markup,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo above: ');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.priceAdjustments = {};
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.unsubscribeEmailFromMailingList = function(email) {
            // console.log(email);
            // console.log('email raw above:');
            if (email) {
                if (vm.vars.taValidate.checkValidEmail(email.email_address)) {
                    var postInfo = {
                        obj: true,
                        uri: 'managemailinglist',
                        email: email.email_address,
                        user_wants_subscription: false,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please enter a valid email address.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                vm.vars.siteMessage = 'Please enter a valid email address.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.initializePrintLogic = function() {
            // hide remaining DOM elements
            $('.site-footer').hide();
            $('#footerwrap').hide();
        };
        // ----------------------------------------------------------------------------
        vm.getMailingListEmails = function() {
            console.log('Get mailing list emails.');
            var postInfo = {
                paramString: true,
                uri: 'getmailinglistemails',
                param: vm.vars.userId
            };
            console.log(postInfo);
            console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.subscribedEmailsList = data.data.subscribedEmailsList;
                        vm.unSubscribedEmailsList = data.data.unSubscribedEmailsList;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };
        // ----------------------------------------------------------------------------
        // ----------------------------------------------------------------------------
        // console.log(vm);
})
// Filter / Cut and limit words/strings.
.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                        value = value.substr(0, lastspace);
                }
        }

        return value + (tail || ' â€¦');
    };
});
