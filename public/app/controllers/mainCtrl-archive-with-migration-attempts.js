angular.module('mainCtrl', ['httpService','formFactory'])
    .controller('mainController', function(
      $rootScope,
      $scope,
      $state,
      $stateParams,
      $window,
      $location,
      Auth,
      HttpService,
      FormFieldValidationFactory,
      $http,
      $sce
    ) {

        // ----------------------------------------------------------------------------
        var vm = this;

        vm.vars = {
            // get info if a person is logged in
            loggedIn: Auth.isLoggedIn(),
            currentState: $state.$current.path[0].self.name,
            userId: $window.localStorage.getItem('userId'),
            firstname: $window.localStorage.getItem('firstname'),
            lastname: $window.localStorage.getItem('lastname'),
            userType: $window.localStorage.getItem('userType'),
            username: $window.localStorage.getItem('username'),
            user_email: $window.localStorage.getItem('email'),
            access_level: $window.localStorage.getItem('accesslevel'),
            user_type_1_active: false,
            user_type_2_3_active: false,
            user_type_4_active: false,
            user_type_5_active: false,
            check_user_type: function() {
                // console.log(this.access_level);
                // console.log('Type of user logged in above:');
                if (this.access_level === '1') {
                    this.user_type_1_active = true;
                } else if ((this.access_level === '2') || (this.access_level === '3')) {
                    this.user_type_2_3_active = true;
                } else if (this.access_level === '4') {
                    this.user_type_4_active = true;
                } else if (this.access_level === '5') {
                    this.user_type_5_active = true;
                }
            },
            siteMessage: false,
            siteClass: false,
            processing: false,
            contactFormActive: true,
            formActive: true,
            accountStatusUpdate: false,
            accountDelete: false,
            editCategory: false,
            editProduct: false,
            deleteProduct: false,
            deleteCategory: false,
            optOutOption: false,
            currentCustomerProductPrice: false,
            adminCategoryObj: {},
            adminProductObj: {},
            priceAdjustments: {},
            siteMasterObj: {},
            userTypeObj: {
                admin: 'admin',
                salesRep: 'salesRep',
                funeralHome: 'funeralHome'
            },
            uploadImageObj: {},
            taUser: {},
            usersObj: {},
            funeralHome: {},
            salesRep: {},
            publicUser: {},
            categoryObj: {},
            productObj: {},
            searchProductsObj: {},
            taValidate: FormFieldValidationFactory,
            todaysFormattedDate: moment().format('L'),
            yesterdaysFormattedDate: moment().subtract(1, 'days').format('L')
        };

        // ----------------------------------------------------------------------------
        vm.activateDropdownNav = function() {
            // console.log('vm.vars.navsUlActive var:');
            // console.log(vm.vars.navsUlActive);
            if (vm.vars.navsUlActive) {
                vm.vars.navsUlActive = false;
            } else {
                vm.vars.navsUlActive = true;
            }
        };

        // ----------------------------------------------------------------------------
        // User navigations arrays
        vm.user1Array = [
            {
				        link: 'main-menu',
                display: 'Main Menu'
            },
            {
                link: 'add-item',
                display: 'Add Item'
            },
            {
                link: 'add-cat',
                display: 'Add Category'
            },
            {
                link: 'create-sales-rep',
                display: 'Create Sales Rep'
            },
            // {
            //     link: 'edit-page-text',
            //     display: 'Edit Page Text'
            // },
            {
                link: 'create-funeral-home',
                display: 'Create Funeral home'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                link: 'remove-category',
                display: 'Remove A Category'
            },
            {
                link: 'manage-user-accounts',
                display: 'Manage User Accounts'
            },
            {
                link: 'login-reports',
                display: 'Login Reports'
            },
            {
                link: 'upload-media',
                display: 'Upload Media Library'
            },
            {
                link: 'change-admin-password',
                display: 'Change Administrator Password'
            },
            {
                link: 'list-user-credentials',
                display: 'View User Credentials'
            }
        ];
        vm.user2And3Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                display: 'Price Management',
                link: 'price-managment'
            },
            // {
            //     display: 'Retail Price Report',
            //     link: 'retail-price-report'
            // },
            // {
            //     display: 'Selection Room Management',
            //     link: 'selection-room-management'
            // },
            {
                display: 'Public Account Setup',
                link: 'account-setup'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user4Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user5Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];

        // ----------------------------------------------------------------------------
        vm.scrollPageToTop = function() {
            var siteHeader = $('.site-header');
            $('html, body').animate({
                scrollTop: siteHeader.offset().top
            }, 1000);
        };
        vm.scrollPageToUpperSection = function() {
            $('html, body').animate({
                scrollTop: 0
                // scrollTop: upperSection.offset().top
            }, 500);
        };

        // ----------------------------------------------------------------------------
        vm.initMainSlider = function() {
            $(document).ready(function() {
                $('.triangle-main-slider').slick({
                    arrows: true,
                    dots: true,
                    infinite: true,
                    speed: 500,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    slidesToShow: 1
                });
            });
        };

        // ----------------------------------------------------------------------------
        vm.getAllUsers = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (vm.vars.loggedIn) {
                var postInfo = {
                    obj: true,
                    uri: 'getallusers',
                    user_id: vm.vars.userId
                };
                // console.log(postInfo);

                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        // console.log(data.data);
                        if (data.data.success) {
                            vm.allUsersObj = data.data.allusers;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            } else {
                vm.vars.siteMessage = 'Please log in to view this page.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.dummySalesRep = function() {
            console.log('Create that dummy sales rep.');
            vm.vars.salesRep = {
                address_1: '1234 Rentrium Road',
                address_2: 'Suite 81',
                city: 'Newport News',
                contact_person: 'Ron Donaldson',
                email: 'rondon55@gmail.com',
                password: 'rondon55',
                phone: 5553338888,
                state: 'VA',
                username: 'rondon55',
                zip: 37488
            };
        };
        vm.createSalesRep = function(salesRepObj) {
            // console.log(salesRepObj);
            // console.log('New salesRepObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var salesRepReady = false;
            var contact_person,
                phone,
                address_1,
                city,
                state,
                zip,
                username,
                password,
                email = false;

            if (salesRepObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidPhoneNumber(salesRepObj.phone)) {
                    phone = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Phone Number" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.address_1)) {
                    address_1 = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Address" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(salesRepObj.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.state)) {
                    state = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "State" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(salesRepObj.city)) {
                    city = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "City" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkZipCode(salesRepObj.zip)) {
                    zip = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Zip" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(salesRepObj.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(salesRepObj.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (phone === true) &&
                    (email === true) &&
                    (address_1 === true) &&
                    (state === true) &&
                    (city === true) &&
                    (zip === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    salesRepReady = true;
                }

                if (salesRepReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewuseraccount',
                        salesRepObj: salesRepObj,
                        usernameCheck: salesRepObj.username,
                        account_type: 'salesrep',
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.salesRep = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.createPublicUser = function(publicUser) {
            // console.log(publicUser);
            // console.log('New publicUser');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var publicUserReady = false;
            var contact_person,
                username,
                password,
                email = false;

            if (publicUser) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(publicUser.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(publicUser.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(publicUser.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(publicUser.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (email === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    publicUserReady = true;
                }

                if (publicUserReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewpublicuser',
                        publicUser: publicUser,
                        usernameCheck: publicUser.username,
                        account_type: 'public_user',
                        user_id: vm.vars.userId,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo above:');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.publicUser = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.dummyFuneralHome = function() {
            console.log('Create that dummy funeral home.');
            vm.vars.funeralHome = {
                address_1: '205 Park Ave',
                address_2: 'Apt 4',
                city: 'Elkhart',
                contact_person: 'Bonny Henderson',
                email: 'bonnybonny@gmail.com',
                fac_name: 'Bonny Bonny Funeral Home',
                password: 'bonnybonny',
                phone: 3733384444,
                state: 'IN',
                username: 'bonnybonny',
                zip: 46516
            };
        };
        vm.createFuneralHome = function(funeralHomeObj) {
            // console.log(funeralHomeObj);
            // console.log('New funeralHomeObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var funeralHomeReady = false;
            var contact_person,
                fac_name,
                phone,
                address_1,
                city,
                state,
                zip,
                username,
                password,
                email = false;

            if (funeralHomeObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.fac_name)) {
                    fac_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Funeral Home Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidPhoneNumber(funeralHomeObj.phone)) {
                    phone = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Phone Number" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.address_1)) {
                    address_1 = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Address" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkValidEmail(funeralHomeObj.email)) {
                    email = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Email" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.state)) {
                    state = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "State" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(funeralHomeObj.city)) {
                    city = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "City" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkZipCode(funeralHomeObj.zip)) {
                    zip = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Zip" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(funeralHomeObj.username)) {
                    username = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Username" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.checkFieldNotEmptyAndMin8Characters(funeralHomeObj.password)) {
                    password = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Password" field. 8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((contact_person === true) &&
                    (fac_name === true) &&
                    (phone === true) &&
                    (email === true) &&
                    (address_1 === true) &&
                    (state === true) &&
                    (city === true) &&
                    (zip === true) &&
                    (username === true) &&
                    (password === true)) {
                    // Form ready for posting
                    funeralHomeReady = true;
                }

                if (funeralHomeReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'createnewuseraccount',
                        funeralHomeObj: funeralHomeObj,
                        usernameCheck: funeralHomeObj.username,
                        account_type: 'funeral_home',
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the Funeral Home Object
                                vm.vars.funeralHome = {};
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.anyUserLogin = function(userInfo) {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.taValidate.validateAnyValue(userInfo.username)) {
                vm.vars.siteMessage = '"Username" field is required!  Please type your username.';
                vm.vars.siteClass = 'alert alert-danger';
            } else if (!vm.vars.taValidate.validateAnyValue(userInfo.password)) {
                vm.vars.siteMessage = '"Password" field is required!';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // console.log(userInfo);
                // console.log('userInfo above:');
                vm.vars.processing = true;

                var formattedDate = vm.vars.todaysFormattedDate;

                Auth.login(userInfo.username, userInfo.password, formattedDate)
                    .then(function(data) {
                        // console.log(data);
                        vm.vars.processing = false;
                        // if a user successfully logs in, redirect to users page
                        if (data.data.success) {
                            // console.log('go to the search page');
                            $location.path('/main-menu');
                            // --------------------------------
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // // ----------------------------------------------------------------------------
        // vm.contactTa = function(contactObj) {
        //     // console.log(contactObj);
        //     var name, email, phone, message;
        //     var formSendReady = false;
        //     // validate contact form
        //     // Clear the success and failure vars
        //     vm.vars.successMessage = false;
        //     vm.vars.errorMessage = false;
        //     if (contactObj !== undefined) {
        //         if (vm.vars.taValidate.checkFieldNotEmpty(contactObj.name)) {
        //             name = contactObj.name;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your name.';
        //         }
        //         if (vm.vars.taValidate.checkValidEmail(contactObj.email)) {
        //             email = contactObj.email;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your email address.';
        //         }
        //         if (contactObj.phone) {
        //             if (vm.vars.taValidate.checkValidPhoneNumber(contactObj.phone)) {
        //                 phone = contactObj.phone;
        //             } else {
        //                 vm.vars.errorMessage = 'Please fix your phone number.';
        //             }
        //         }
        //         if (vm.vars.taValidate.checkFieldNotEmpty(contactObj.message)) {
        //             message = contactObj.message;
        //         } else {
        //             vm.vars.errorMessage = 'Please fix your message.';
        //         }
        //     } else {
        //         vm.vars.errorMessage = 'Please fill out the form completely.';
        //     }
        //     // If all checks out
        //     if ((name !== undefined) && (email !== undefined) && (message !== undefined)) {
        //         formSendReady = true;
        //     }
        //
        //     if (formSendReady) {
        //         var postInfo = {
        //             obj: true,
        //             uri: 'contactta',
        //             name: name,
        //             email: email,
        //             phone: phone,
        //             message: message
        //         };
        //         // console.log(postInfo);
        //         HttpService.postRequest(postInfo)
        //             .then(function(data) {
        //                 // console.log(data);
        //                 if (data.data.success) {
        //                     vm.vars.successMessage = data.data.message;
        //                 } else {
        //                     // console.log('error dude.');
        //                     // console.log(data);
        //                     vm.vars.errorMessage = data.data.message;
        //                 }
        //                 // close the contact form
        //                 vm.vars.contactFormActive = false;
        //             });
        //     }
        // };

        // ----------------------------------------------------------------------------
        vm.getUserAdminInfo = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to view this page.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    obj: true,
                    uri: 'getuserinfo',
                    user_id: vm.vars.userId
                };
                // console.log(postInfo);
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.userObj = data.data.userObj;
                            vm.vars.usersObj = data.data.userObj;
                            vm.vars.priceAdjustments = data.data.userObj;
                            vm.vars.optOutOption = vm.vars.priceAdjustments.opt_out_base_price_increases;
                            vm.vars.siteMasterObj = data.data.siteMasterObj;
                            console.log(vm.vars.optOutOption);
                            console.log('vm.vars.optOutOption above:');
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.newUserObj = {};
        vm.changeAdminPassword = function(newUserObj) {
            // console.log(newUserObj);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (newUserObj)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if ((newUserObj.new_password) && (newUserObj.matched_password)) {
                    if ((vm.vars.taValidate.validateAnyValue(newUserObj.new_password)) &&
                        (vm.vars.taValidate.identicalPasswords(newUserObj.new_password, newUserObj.matched_password))) {
                        var putInfo = {
                            uri: 'changemypassword',
                            param: vm.vars.userId,
                            newPassword: newUserObj.new_password
                        };
                        // console.log(putInfo);
                        HttpService.putRequest(putInfo)
                            .then(function(data) {
                                if (data.data.success) {
                                    vm.userObj = data.data.userObj;
                                    vm.vars.siteMessage = data.data.message;
                                    vm.vars.siteClass = 'alert alert-success';
                                } else {
                                    vm.vars.siteMessage = data.data.message;
                                    vm.vars.siteClass = 'alert alert-danger';
                                }
                            });
                    } else {
                        vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                } else {
                    vm.vars.siteMessage = 'Please completely fill out this form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.uploadToImageLibrary = function(imageObj) {
            // console.log(imageObj);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (imageObj)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if ((imageObj.media_thumbnail) &&
                    (vm.vars.taValidate.validateAnyValue(imageObj.image_name)) &&
                    (vm.vars.taValidate.validateAnyValue(imageObj.image_caption))) {
                    var postInfo = {
                        obj: true,
                        uri: 'uploadmedia',
                        imageFile: imageObj.media_thumbnail.raw_file.target.result,
                        imageObj: imageObj
                    };
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.uploadImageObj = {};
                                // // reload the page
                                // location.reload();
                                vm.getImageLibrary();
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please fill out this form completely.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.getImageLibrary = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    singleEndPoint: true,
                    uri: 'getmedialibrary'
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.mediaObj = data.data.mediaObj;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.cancelAnyMangement = function() {
            vm.vars.accountDelete = false;
            vm.vars.accountStatusUpdate = false;
        };
        // ----------------------------------------------------------------------------
        vm.showAccountToDelete = function(account) {
            vm.cancelAnyMangement();
            // console.log(account);
            // console.log('the user account above.');
            vm.vars.accountDelete = {
                user_id: account._id,
                username: account.username
            };
        };
        vm.finalDeleteUserAccount = function(account) {
            // console.log(account);
            // console.log('account object to use for server.');
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (account)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if (vm.vars.taValidate.validateAnyValue(account.user_id)) {
                    var postInfo = {
                        paramObj: true,
                        uri: 'deleteuseraccount',
                        param: vm.vars.userId,
                        id_to_delete_on: account.user_id
                    };
                    // console.log(postInfo);
                    // console.log('postInfo for status change.');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.cancelAnyMangement();
                                vm.getUserAccounts();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.viewPrepUserStatus = function(account) {
            vm.cancelAnyMangement();
            // console.log(account);
            // console.log('the user account above.');
            vm.vars.accountStatusUpdate = {
                user_id: account._id,
                username: account.username,
                account_status: String(account.account_status)
            };
        };
        vm.changeUserStatus = function(account) {
            // console.log(account);
            // console.log('account object to use for server.');
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn) && (account)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                // validate user object props
                if (vm.vars.taValidate.validateAnyValue(account.account_status)) {
                    var putInfo = {
                        uri: 'updateuseraccountstatus',
                        param: vm.vars.userId,
                        id_to_update_on: account.user_id,
                        account_status: parseInt(account.account_status)
                    };
                    // console.log(putInfo);
                    // console.log('putInfo for status change.');
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                // vm.allUserAccountsArray = data.data.allUserAccounts;
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.cancelAnyMangement();
                                vm.getUserAccounts();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please ensure that your new password fields match.  8 character minimum.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.getSingleUserLoginReport = function() {
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var postInfo = {
                    paramObj: true,
                    uri: 'singleloginhistoryreport',
                    param: vm.vars.userId,
                    id_to_report: $stateParams.user_id
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.singleUserLoginReport = data.data.singlereport;
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getUserLoginReports = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    paramString: true,
                    uri: 'getloginhistoryreports',
                    param: vm.vars.userId
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.loginReportsArray = data.data.allreports;
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getUserAccounts = function() {
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (!vm.vars.loggedIn) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                var getInfo = {
                    paramString: true,
                    uri: 'getuseraccounts',
                    param: vm.vars.userId,
                };
                // console.log(getInfo);
                // console.log('getInfo');
                HttpService.getRequest(getInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.allUserAccountsArray = data.data.allUserAccounts;
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.getCategories = function() {
            // console.log('Get all categories');
            var getInfo = {
                singleEndPoint: true,
                uri: 'getallcategories'
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.allProductCategories = data.data.allProductCategories;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAdminCategories = function() {
            // console.log('Get all categories');
            var postInfo = {
                paramObj: true,
                uri: 'getadmincategories',
                param: vm.vars.userId
            };
            // console.log(postInfo);
            // console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.adminCategories = data.data.adminCategories;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getProductsByCategoryCode = function() {
            var getInfo = {
                paramString: true,
                uri: 'getproductsbycategorycode',
                param: $stateParams.category_code
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.productsFoundByCode = data.data.productsFoundByCode;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getSingleProductByCode = function() {
            console.log('getSingleProductByCode func fired.');
            var getInfo = {
                paramString: true,
                uri: 'getsingleproductbycode',
                param: $stateParams.product_code
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.activeSingleProduct = data.data.activeSingleProduct;
                        // vm.vars.usersObj = data.data.userObj;
                        // console.log(vm.vars.usersObj);
                        // console.log('vm.vars.usersObj above:');

                        function getYourProductPrice(base, markup) {
                            console.log(base);
                            console.log('base basic');
                            console.log(markup);
                            console.log('markup basic');
                            if (markup === undefined) {
                                markup = 0;
                            }
                            var yourPrice;
                            var percentageCalculation = base * markup / 100;
                            // console.log(percentageCalculation);
                            // console.log('percentageCalculation');
                            yourPrice = Number(base) + Number(percentageCalculation);
                            // console.log(yourPrice);
                            // console.log('yourPrice');

                            return yourPrice;
                        }

                        function getFuneralHomeProductPrice(base, sitemastermarkup) {
                            console.log(base);
                            console.log('base basic');
                            console.log(sitemastermarkup);
                            console.log('sitemastermarkup basic');
                            if (sitemastermarkup === undefined) {
                                sitemastermarkup = 0;
                            }
                            var yourPrice;
                            var percentageCalculation = base * sitemastermarkup / 100;
                            // console.log(percentageCalculation);
                            // console.log('percentageCalculation');
                            yourPrice = Number(base) + Number(percentageCalculation);
                            // console.log(yourPrice);
                            // console.log('yourPrice');

                            return yourPrice;
                        }

                        function getYourProductPriceWithSiteMaster(base, markup, sitemastermarkup) {
                            console.log(base);
                            console.log('base advanced sitemaster');
                            console.log(markup);
                            console.log('markup advanced sitemaster');
                            console.log(sitemastermarkup);
                            console.log('sitemastermarkup advanced sitemaster');
                            if (markup === undefined) {
                                markup = 0;
                            }
                            if (sitemastermarkup === undefined) {
                                sitemastermarkup = 0;
                            }
                            var yourPrice;
                            // var percentageCalculation = base * markup / 100;
                            // var siteMasterPercentageCalculation = base * sitemastermarkup / 100;
                            // combine both percentages into 1 percentage sum before calculating the price
                            var overallMarkup = markup + sitemastermarkup;
                            var percentageCalculation = base * overallMarkup / 100;
                            var siteMasterPercentageCalculation = base * sitemastermarkup / 100;
                            yourPrice = Number(base) + Number(percentageCalculation) + Number(siteMasterPercentageCalculation);

                            return yourPrice;
                        }
                        // vm.vars.currentCustomerProductPrice = 77;
                        if (vm.vars.access_level === '5') {
                            console.log('PUBLIC user viewing price.  Please wait…');
                            function customerViewPrice() {
                                // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                                // console.log(vm.vars.currentCustomerProductPrice);
                                // console.log('vm.vars.currentCustomerProductPrice');

                                console.log(vm.vars.priceAdjustments);
                                console.log('vm.vars.priceAdjustments above:');

                                // go and get the price point
                                var getInfo = {
                                    paramString: true,
                                    uri: 'getuserpricepoint',
                                    param: vm.vars.usersObj.parent_account.user_code
                                };
                                // console.log(getInfo);
                                // console.log('getInfo above:');
                                HttpService.getRequest(getInfo)
                                    .then(function(data) {
                                        if (data.data.success) {
                                            vm.userPriceObj = data.data.userPriceObj;
                                            vm.siteMasterProductRate = data.data.siteMasterObj.master_product_price_markup;
                                            // if site master markup is > 0 do this.
                                            if (vm.siteMasterProductRate > 0) {
                                                console.log('Add the site master pricing.');
                                                var xBasePrice = vm.activeSingleProduct.base_price;
                                                var xUserPriceRounding = vm.userPriceObj.price_rounding;
                                                var xSiteMasterMarkup = vm.siteMasterProductRate;
                                                vm.vars.currentCustomerProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                                console.log(vm.vars.currentCustomerProductPrice);
                                                console.log('vm.vars.currentCustomerProductPrice above:');
                                            } else {
                                                console.log('Normal pricing.  Would rarely happen.');
                                                vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.userPriceObj.price_rounding);
                                            }
                                        } else {
                                            vm.vars.siteMessage = data.data.message;
                                            vm.vars.siteClass = 'alert alert-danger';
                                            vm.currentCustomerProductPrice = 'N/A';
                                            console.log(vm.vars.currentCustomerProductPrice);
                                            console.log('vm.vars.currentCustomerProductPrice above:');
                                        }
                                    });
                            }
                            // Set a short Timeout for pagination elements
                            function startCustomerPriceTimer() {
                                var customerPriceTimer = window.setTimeout(
                                    customerViewPrice, 1000
                                );
                            }
                            startCustomerPriceTimer();
                        } else if ((vm.vars.access_level === '3') ||
                                   (vm.vars.access_level === '2')) {
                            console.log('Level 2 or 3 users viewing price.');
                            // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                            // console.log(vm.vars.currentCustomerProductPrice);
                            // console.log('vm.vars.currentCustomerProductPrice');

                            // go and get the price point
                            var getInfo = {
                                singleEndPoint: true,
                                uri: 'getmasterpricerate',
                            };
                            // console.log(getInfo);
                            // console.log('getInfo above:');
                            HttpService.getRequest(getInfo)
                                .then(function(data) {
                                    if (data.data.success) {
                                        // vm.userPriceObj = data.data.userPriceObj;
                                        vm.siteMasterProductRate = data.data.siteMasterObj.master_product_price_markup;
                                        // if site master markup is > 0 do this.
                                        var xBasePrice = vm.activeSingleProduct.base_price;
                                        // var xUserPriceRounding = vm.userPriceObj.price_rounding;
                                        var xUserPriceRounding = vm.vars.usersObj.price_rounding;
                                        var xSiteMasterMarkup = vm.siteMasterProductRate;
                                        if (vm.siteMasterProductRate > 0) {
                                            console.log('Add the site master pricing.');
                                            vm.yourCustomersProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                            vm.vars.currentCustomerProductPrice = getFuneralHomeProductPrice(xBasePrice, xSiteMasterMarkup);
                                            console.log(vm.vars.currentCustomerProductPrice);
                                            console.log('vm.vars.currentCustomerProductPrice above:');
                                        } else {
                                            console.log('Normal pricing.  Would rarely happen.');
                                            // vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.userPriceObj.price_rounding);
                                            vm.yourCustomersProductPrice = getYourProductPriceWithSiteMaster(xBasePrice, xUserPriceRounding, xSiteMasterMarkup);
                                            vm.vars.currentCustomerProductPrice = vm.activeSingleProduct.base_price;
                                        }
                                    } else {
                                        vm.vars.siteMessage = data.data.message;
                                        vm.vars.siteClass = 'alert alert-danger';
                                        vm.currentCustomerProductPrice = 'N/A';
                                        console.log(vm.vars.currentCustomerProductPrice);
                                        console.log('vm.vars.currentCustomerProductPrice above last option:');
                                    }
                                });
                        } else {
                            // console.log('admin user viewing price.');
                            vm.vars.currentCustomerProductPrice = getYourProductPrice(vm.activeSingleProduct.base_price, vm.vars.usersObj.price_rounding);
                            console.log(vm.vars.currentCustomerProductPrice);
                            console.log('vm.vars.currentCustomerProductPrice not a level 5, 3, or 2');
                        }
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.getAllProducts = function() {
            var getInfo = {
                singleEndPoint: true,
                uri: 'getallproducts'
            };
            // console.log(getInfo);
            // console.log('getInfo');
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.allProducts = data.data.allProducts;
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.closeEditCategory = function() {
            // Show the category edit block
            vm.vars.editCategory = false;

            // Empty the category form
            vm.vars.adminCategoryObj = {};
        };

        // ----------------------------------------------------------------------------
        vm.editCategory = function(category) {
            // console.log(category);
            // console.log('category to edit above:');

            // Show the category edit block
            vm.vars.editCategory = true;

            // Bind the category to the edit form
            vm.vars.adminCategoryObj = category;

            // Scroll to the top of the page
            vm.scrollPageToUpperSection();
        };

        // ----------------------------------------------------------------------------
        vm.adminEditCategory = function(category) {
            // console.log(category);
            // console.log('category to edit above:');

            var formReady = false;

            if (category) {
                if ((vm.vars.taValidate.validateAnyValue(category.category_description)) &&
                    (vm.vars.taValidate.validateAnyValue(category.category_image))) {
                    formReady = true;
                } else {
                    vm.vars.siteMessage = 'Please fill out the form completely.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                vm.vars.siteMessage = 'Please fill out the form completely.';
                vm.vars.siteClass = 'alert alert-danger';
            }

            if (formReady === true) {
                var postInfo = {
                    obj: true,
                    uri: 'changesinglecategorydata',
                    category: category,
                    user_id: vm.vars.userId
                };
                console.log(postInfo);
                console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.closeEditCategory();
                            vm.getCategories();
                            // console.log(data.data);
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                        }
                    });
            }
        };

        // ----------------------------------------------------------------------------
        vm.closeEditProduct = function() {
            // Show the product edit block
            vm.vars.editProduct = false;

            // Empty the product form
            vm.vars.adminProductObj = {};
        };

        // ----------------------------------------------------------------------------
        vm.editProduct = function(product) {
            console.log(product);
            console.log('product to edit above:');

            // Show the product edit block
            vm.vars.editProduct = true;

            // Bind the product to the edit form
            vm.vars.adminProductObj = product;

            // Scroll to the top of the page
            vm.scrollPageToUpperSection();
        };

        // ----------------------------------------------------------------------------
        vm.adminEditProduct = function(product) {
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;
            console.log(product);
            console.log('product to edit above:');

            var formReady = false;
            var productReady = false;
            var category_code,
                interior,
                product_name,
                base_price,
                color_finish,
                thumbnail_image,
                material = false;

            if (product) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(product.category_code)) {
                    category_code = true;
                } else {
                    vm.vars.siteMessage = 'This product was not issued a "Category Code"';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.interior)) {
                    interior = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Interior" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.product_name)) {
                    product_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Product Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.base_price)) {
                    base_price = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Base Price" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.color_finish)) {
                    color_finish = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Color / Finish" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.thumbnail_image)) {
                    thumbnail_image = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Photo Image" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(product.material)) {
                    material = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Material" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((category_code === true) &&
                    (interior === true) &&
                    (product_name === true) &&
                    (base_price === true) &&
                    (color_finish === true) &&
                    (thumbnail_image === true) &&
                    (material === true)) {
                    productReady = true;
                }

                if (productReady === true) {
                    var postInfo = {
                        obj: true,
                        uri: 'changesingleproductdata',
                        product: product,
                        user_id: vm.vars.userId
                    };
                    console.log(postInfo);
                    console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.closeEditProduct();
                                vm.getCategories();
                                vm.getProductsByCategoryCode();
                                // console.log(data.data);
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.addProductImageString = function(imageObj) {
            console.log(imageObj);
            console.log('imageObj');
            vm.vars.adminProductObj.image_name = imageObj.image_name;
            vm.vars.adminProductObj.thumbnail_image = imageObj.file_name;
        };
        vm.dummyProductObj = function() {
            console.log('Create that dummy product.');
            vm.vars.productObj = {
                model_style: 'Hard Soft Exterior Style',
                material: 'Furr Ball',
                thumbnail_image: 'tacc-photo-1500004018885.jpg',
                image_name: 'Soft Shell Animal',
                color_finish: 'Gold and Grey',
                base_price: '2200',
                product_name: 'Furry Shelled Animal',
                interior: 'Soft shell grey plush',
                category_code: 'category_1500254635000670794'
            };
        };
        vm.createProduct = function(productObj) {
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var productReady = false;
            var category_code,
                // product_description,
                interior,
                product_name,
                base_price,
                color_finish,
                // image_name,
                thumbnail_image,
                material = false;
                // notes = false;

            if (productObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(productObj.category_code)) {
                    category_code = true;
                } else {
                    vm.vars.siteMessage = 'This product was not issued a "Category Code"';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.interior)) {
                    interior = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Interior" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.product_name)) {
                    product_name = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Product Name" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.base_price)) {
                    base_price = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Base Price" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.color_finish)) {
                    color_finish = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Color / Finish" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(vm.vars.adminProductObj.thumbnail_image)) {
                    thumbnail_image = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Photo Image" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
                if (FormFieldValidationFactory.validateAnyValue(productObj.material)) {
                    material = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Material" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if ((category_code === true) &&
                    // (product_description === true) &&
                    (interior === true) &&
                    (product_name === true) &&
                    (base_price === true) &&
                    (color_finish === true) &&
                    (thumbnail_image === true) &&
                    (material === true)) {
                    productReady = true;
                }

                if (productReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;

                    var postInfo = {
                        paramObj: true,
                        uri: 'createproduct',
                        param: vm.vars.userId,
                        todaysFormattedDate: vm.vars.todaysFormattedDate,
                        productObj: productObj
                    };
                    // push the image into the object
                    if (vm.vars.adminProductObj.thumbnail_image) {
                        postInfo.productObj.image_name = vm.vars.adminProductObj.image_name;
                        postInfo.productObj.thumbnail_image = vm.vars.adminProductObj.thumbnail_image;
                    }
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.productObj = {};
                                vm.scrollPageToUpperSection();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.addCategoryImageString = function(imageObj) {
            console.log(imageObj);
            console.log('imageObj');
            vm.vars.categoryObj.category_image = imageObj.image_name;
            vm.vars.categoryObj.file_name = imageObj.file_name;
            vm.vars.adminCategoryObj.category_image = imageObj.file_name;
            vm.vars.adminCategoryObj.image_name = imageObj.image_name;
            vm.vars.adminProductObj.thumbnail_image = imageObj.file_name;
            vm.vars.adminProductObj.image_name = imageObj.image_name;
            vm.vars.productObj.thumbnail_image = imageObj.file_name;
            vm.vars.productObj.image_name = imageObj.image_name;
        };
        vm.createCategory = function(categoryObj) {
            // console.log(categoryObj);
            // console.log('categoryObj');
            // console.log('Get a single loging report for '+ $stateParams.user_id);
            // clear the errors
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if ((!vm.vars.loggedIn)) {
                vm.vars.siteMessage = 'Please log in to perform this action.';
                vm.vars.siteClass = 'alert alert-danger';
            } else {
                if ((categoryObj.category_description) && (categoryObj.file_name)) {
                var postInfo = {
                    paramObj: true,
                    uri: 'createcategory',
                    param: vm.vars.userId,
                    todaysFormattedDate: vm.vars.todaysFormattedDate,
                    categoryObj: categoryObj
                };
                // console.log(postInfo);
                // console.log('postInfo');
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-success';
                            vm.vars.categoryObj = {};
                            vm.scrollPageToUpperSection();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteClass = 'alert alert-danger';
                            vm.scrollPageToUpperSection();
                        }
                    });
                } else {
                    // Form is not valid
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            }
        };

        // ----------------------------------------------------------------------------
        vm.searchProducts = function(searchProductsObj) {
            // console.log(searchProductsObj);
            // console.log('New searchProductsObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var searchReady = false;

            if (searchProductsObj) {
                // validate fields.  Really just a check to see if any of these fields exist.
                if ((vm.vars.taValidate.validateAnyValue(searchProductsObj.product_name)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.material)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.color_finish)) ||
                    (vm.vars.taValidate.validateAnyValue(searchProductsObj.interior))) {
                    searchReady = true;
                } else {
                    vm.vars.siteMessage = 'Please fill out at least 1 input field to search on.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                if (searchReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        obj: true,
                        uri: 'searchproductsuri',
                        searchProductsObj: searchProductsObj,
                        access_level: vm.vars.access_level
                    };
                    // console.log(postInfo);
                    // console.log('postInfo search object above.');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Clear out the form object
                                // vm.vars.searchProductsObj = {};
                                vm.returnedProductsArr = data.data.products;
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.returnedProductsArr = {};
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.editCurrentUsersInfo = function(usersObj) {
            // console.log(usersObj);
            // console.log('New usersObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            var editUserObjReady = false;
            var contact_person = false;

            if (usersObj) {
                // validate fields
                if (FormFieldValidationFactory.validateAnyValue(usersObj.contact_person)) {
                    contact_person = true;
                } else {
                    vm.vars.siteMessage = 'Please fix the "Contact Person" field.';
                    vm.vars.siteClass = 'alert alert-danger';
                }

                // determine if all fields are ready
                if (contact_person === true) {
                    // Form ready for updating
                    editUserObjReady = true;
                }

                if (editUserObjReady === true) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var putInfo = {
                        uri: 'updateuseraccount',
                        param: vm.vars.userId,
                        usersObj: usersObj,
                        account_type: vm.vars.usersObj.account_type,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(putInfo);
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.callDeleteProduct = function() {
            vm.vars.deleteProduct = true;
        };
        // ----------------------------------------------------------------------------
        vm.cancelDelete = function() {
            vm.vars.deleteProduct = false;
        };
        // ----------------------------------------------------------------------------
        vm.deleteProduct = function(product_code) {
            // console.log(product_code);
            // console.log('product_code above:');

            // activate loading icon
            vm.vars.processing = true;
            var postInfo = {
                paramObj: true,
                uri: 'deleteproduct',
                param: vm.vars.userId,
                product_code: product_code
            };
            // console.log(postInfo);
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    // Stop the processing icon
                    vm.vars.processing = false;
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                        vm.cancelDelete();
                        vm.getProductsByCategoryCode();
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                        vm.scrollPageToUpperSection();
                    }
                });
        };
        // ----------------------------------------------------------------------------
        vm.callDeleteCategory = function() {
            vm.vars.deleteCategory = true;
        };
        // ----------------------------------------------------------------------------
        vm.cancelDeleteCategory = function() {
            vm.vars.deleteCategory = false;
        };
        // ----------------------------------------------------------------------------
        vm.deleteCategory = function(category_id) {
            // console.log(cateogry_code);
            // console.log('cateogry_code above:');

            // activate loading icon
            vm.vars.processing = true;
            var postInfo = {
                paramObj: true,
                uri: 'deletecategory',
                param: vm.vars.userId,
                category_id: category_id
            };
            // console.log(postInfo);
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    // Stop the processing icon
                    vm.vars.processing = false;
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                        vm.cancelDeleteCategory();
                        vm.getAdminCategories();
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                        vm.scrollPageToUpperSection();
                    }
                });
        };

        // ----------------------------------------------------------------------------
        vm.editPriceMarkup = function(priceObj) {
            // console.log(priceObj);
            // console.log('New priceObj');
            // console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (priceObj) {
                // validate fields
                if (FormFieldValidationFactory.checkNumberPriceValue(priceObj.price_rounding)) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var putInfo = {
                        uri: 'updatepricemarkup',
                        param: vm.vars.userId,
                        price_rounding: priceObj.price_rounding,
                        opt_out_base_price_increases: priceObj.opt_out_base_price_increases,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(putInfo);
                    // console.log('putInfo above: ');
                    HttpService.putRequest(putInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.priceAdjustments = {};
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.editMasterPriceMarkup = function(priceObj) {
            console.log(priceObj);
            console.log('New priceObj');
            console.log('----------------');
            // Clear out the error messages
            vm.vars.siteMessage = false;
            vm.vars.siteClass = false;

            if (priceObj) {
                // validate fields
                if (FormFieldValidationFactory.checkNumberPriceValue(priceObj.master_product_price_markup)) {
                    // activate loading icon
                    vm.vars.processing = true;
                    var postInfo = {
                        paramObj: true,
                        uri: 'mastersiterateincrease',
                        param: vm.vars.userId,
                        master_product_price_markup: priceObj.master_product_price_markup,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo above: ');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            // Stop the processing icon
                            vm.vars.processing = false;
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                                vm.vars.priceAdjustments = {};
                                // Get the user's infomation once again when the process has finished.
                                vm.getUserAdminInfo();
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                                vm.scrollPageToUpperSection();
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please completely fill out the form.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                // Form is not valid
                vm.vars.siteMessage = 'Please completely fill out the form.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.unsubscribeEmailFromMailingList = function(email) {
            // console.log(email);
            // console.log('email raw above:');
            if (email) {
                if (vm.vars.taValidate.checkValidEmail(email.email_address)) {
                    var postInfo = {
                        obj: true,
                        uri: 'managemailinglist',
                        email: email.email_address,
                        user_wants_subscription: false,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please enter a valid email address.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                vm.vars.siteMessage = 'Please enter a valid email address.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };
        // ----------------------------------------------------------------------------
        vm.initializePrintLogic = function() {
            // hide remaining DOM elements
            $('.site-footer').hide();
            $('#footerwrap').hide();
        };

        // ----------------------------------------------------------------------------
        vm.oldUsersArr = [
          {
            "old_id": "11",
            "username": "paula574",
            "password": "3837744657",
            "account_type": "2",
            "fac_name": "New Style Family Inc",
            "contact_person": "Paula Right",
            "email": "me@papy.com",
            "phone": "3837744657",
            "fax": "",
            "address_1": "334 Jumper Road",
            "address_2": "Suite 987",
            "city": "Miami",
            "state": "FL",
            "zip": "29383",
            "web": "www.papy.com",
            "price_rounding": 0,
            "parent_account": "0",
            "account_status": 777
          }, {
            "old_id": "41",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Massey Funeral Home",
            "contact_person": "Rigy Massey",
            "email": "masseyfh@aol.com",
            "phone": "919-269-6600",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Zebulon",
            "state": "NC",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "41",
            "account_status": 777
          }, {
            "old_id": "46",
            "username": "carltonsvc",
            "password": "9192316120",
            "account_type": "2",
            "fac_name": "Carlton L. Gray Funeral & Cremation Svc",
            "contact_person": "Carlton L. Gray",
            "email": "carltongray@bellsouth.net",
            "phone": "919-231-6120",
            "fax": "",
            "address_1": "2810 Kidd Road",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27610",
            "web": "www.carltonlgrayfunerals.com",
            "price_rounding": 0,
            "parent_account": "46",
            "account_status": 777
          }, {
            "old_id": "48",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Charlton and Groome Funeral Home",
            "contact_person": "Joey Charlton",
            "email": "jacharlton1965@aol.com",
            "phone": " 540-932-3600",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Fishersville",
            "state": "VA",
            "zip": "0",
            "web": "www.charltongroomefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "48",
            "account_status": 777
          }, {
            "old_id": "49",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Charlton and Groome Funeral Home",
            "contact_person": "Joey Charlton",
            "email": "jacharlton1965@aol.com",
            "phone": " 540-932-3600",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Fishersville",
            "state": "VA",
            "zip": "0",
            "web": "www.charltongroomefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "49",
            "account_status": 777
          }, {
            "old_id": "50",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Rutledge & Bigham Funeral Home",
            "contact_person": "",
            "email": "rutledgeinc@bellsouth.net",
            "phone": "704-873-3611",
            "fax": "",
            "address_1": "",
            "address_2": "603 south Center Street",
            "city": "Statesville",
            "state": "NC",
            "zip": "28677",
            "web": "www.rutledgeandbigham.com",
            "price_rounding": 0,
            "parent_account": "50",
            "account_status": 777
          }, {
            "old_id": "52",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Clements Funeral Service",
            "contact_person": "",
            "email": "KC20542",
            "phone": "919-286-1224",
            "fax": "",
            "address_1": "1105 Board Street",
            "address_2": "",
            "city": "Durham ",
            "state": "Nor",
            "zip": "27705",
            "web": "www.clementsfuneralservice.com",
            "price_rounding": 0,
            "parent_account": "52",
            "account_status": 777
          }, {
            "old_id": "53",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Clements Funeral Service",
            "contact_person": "",
            "email": "KC20542",
            "phone": "919-286-1224",
            "fax": "",
            "address_1": "1105 Board Street",
            "address_2": "",
            "city": "Durham ",
            "state": "Nor",
            "zip": "27705",
            "web": "www.clementsfuneralservice.com",
            "price_rounding": 0,
            "parent_account": "52",
            "account_status": 777
          }, {
            "old_id": "55",
            "username": "massey456",
            "password": "6789rigy",
            "account_type": "3",
            "fac_name": "Massey Funeral Home",
            "contact_person": "Rigy Massey",
            "email": "masseyfh@aol.com",
            "phone": "919-269-6600",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Zebulon",
            "state": "NC",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "41",
            "account_status": 777
          }, {
            "old_id": "57",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Holloway Memorial Funeral Home",
            "contact_person": "",
            "email": "",
            "phone": "919-598-8496",
            "fax": "",
            "address_1": "2502 NC Highway 55",
            "address_2": "",
            "city": "Durham ",
            "state": "Nor",
            "zip": "27713",
            "web": "www.HollowayMemorial.com",
            "price_rounding": "5",
            "parent_account": "57",
            "account_status": 777
          }, {
            "old_id": "58",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Holloway Memorial Funeral Home",
            "contact_person": "",
            "email": "",
            "phone": "919-598-8496",
            "fax": "",
            "address_1": "2502 NC Highway 55",
            "address_2": "",
            "city": "Durham ",
            "state": "Nor",
            "zip": "27713",
            "web": "www.HollowayMemorial.com",
            "price_rounding": "5",
            "parent_account": "57",
            "account_status": 777
          }, {
            "old_id": "60",
            "username": "",
            "password": "",
            "account_type": "4",
            "fac_name": "",
            "contact_person": "Thomas McDonald ",
            "email": "thomas@triangleatlantic.com",
            "phone": "",
            "fax": "",
            "address_1": "South Carolina Sales Representative",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "60",
            "account_status": 777
          }, {
            "old_id": "61",
            "username": "",
            "password": "",
            "account_type": "4",
            "fac_name": "",
            "contact_person": "Keith Presley",
            "email": "Keith@triangleatlantic.com",
            "phone": "",
            "fax": "",
            "address_1": "Customer Service Representative",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "61",
            "account_status": 777
          }, {
            "old_id": "62",
            "username": "",
            "password": "",
            "account_type": "4",
            "fac_name": "",
            "contact_person": "Jonathan Presley",
            "email": "jonathan@triangleatlantic.com",
            "phone": "",
            "fax": "",
            "address_1": "Customer Service Representative",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "62",
            "account_status": 777
          }, {
            "old_id": "68",
            "username": "paula111",
            "password": "patterson123",
            "account_type": "2",
            "fac_name": "Living Waters FH\/C",
            "contact_person": "Paula Patterson",
            "email": "padpatterson@gmail.com",
            "phone": "864-439-7141",
            "fax": "",
            "address_1": "529 Spartanburg Hwy S.",
            "address_2": "",
            "city": "Lyman",
            "state": "SC",
            "zip": "29365",
            "web": "www.livingwatersfh.com",
            "price_rounding": 0,
            "parent_account": "68",
            "account_status": 777
          }, {
            "old_id": "69",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Living Waters FH\/C",
            "contact_person": "Paula Patterson",
            "email": "padpatterson@gmail.com",
            "phone": "864-439-7141",
            "fax": "",
            "address_1": "529 Spartanburg Hwy S.",
            "address_2": "",
            "city": "Lyman",
            "state": "SC",
            "zip": "29365",
            "web": "www.livingwatersfh.com",
            "price_rounding": 0,
            "parent_account": "68",
            "account_status": 777
          }, {
            "old_id": "70",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Fort Mill Funeral Home",
            "contact_person": "Jeff McWatters",
            "email": "fmfh@comporium.net",
            "phone": "803-548-2900",
            "fax": "",
            "address_1": "120 Massey St.",
            "address_2": "",
            "city": "Ft. Mill",
            "state": "S.C",
            "zip": "29715",
            "web": "www.fortmillfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "70",
            "account_status": 777
          }, {
            "old_id": "72",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "The Serenity Mortuary",
            "contact_person": "Shannon N. DeLoach",
            "email": "serenitymortuary@hotmail.com",
            "phone": "843-225-7800",
            "fax": "843-225-7803",
            "address_1": "209 Old Trolley Road",
            "address_2": "",
            "city": "Summerville",
            "state": "SC ",
            "zip": "29485",
            "web": "",
            "price_rounding": 0,
            "parent_account": "72",
            "account_status": "stored"
          }, {
            "old_id": "73",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "The Serenity Mortuary",
            "contact_person": "Shannon DeLoach",
            "email": "serenitymortuary@hotmail.com",
            "phone": "843-225-7800",
            "fax": "",
            "address_1": "209 Old Trolley Rd",
            "address_2": "",
            "city": "Summerville",
            "state": "SC ",
            "zip": "29485",
            "web": "",
            "price_rounding": 0,
            "parent_account": "72",
            "account_status": "stored"
          }, {
            "old_id": "74",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "setech test",
            "contact_person": "setech test",
            "email": "",
            "phone": "919 555 1212",
            "fax": "",
            "address_1": "101 Any Street",
            "address_2": "",
            "city": "Raleigh",
            "state": "nc",
            "zip": "27511",
            "web": "",
            "price_rounding": 0,
            "parent_account": "74",
            "account_status": 777
          }, {
            "old_id": "76",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Roberson Funeral Home",
            "contact_person": "",
            "email": "robersonfuneralhome@aol.com",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "76",
            "account_status": 777
          }, {
            "old_id": "80",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Whiting Funeral Home",
            "contact_person": "Chris Stone",
            "email": "chris@whitingsfuneralhome.com",
            "phone": "757-229-3011",
            "fax": "",
            "address_1": "7005 Pocahontas Trail",
            "address_2": "",
            "city": "Williamsburg",
            "state": "VA",
            "zip": "23185",
            "web": "www.whitingsfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "80",
            "account_status": 777
          }, {
            "old_id": "81",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Whiting Funeral Home",
            "contact_person": "Chris Stone",
            "email": "chris@whitingsfuneralhome.com",
            "phone": "757-229-3011",
            "fax": "",
            "address_1": "7005 Pocahontas Trail",
            "address_2": "",
            "city": "Williamsburg",
            "state": "VA",
            "zip": "23185",
            "web": "www.whitingsfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "80",
            "account_status": 777
          }, {
            "old_id": "82",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Fort Mill Funeral Home",
            "contact_person": "Jeff McWatters",
            "email": "fmfh@comporium.net",
            "phone": "803-548-2900",
            "fax": "",
            "address_1": "120 Massey St.",
            "address_2": "",
            "city": "Fort Mill",
            "state": "SC",
            "zip": "29715",
            "web": "",
            "price_rounding": 0,
            "parent_account": "82",
            "account_status": 777
          }, {
            "old_id": "84",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Fort Mill Funeral Home",
            "contact_person": "Jeff McWatters",
            "email": "fmfh@comporium.net",
            "phone": "803-548-2900",
            "fax": "",
            "address_1": "120 Massey St.",
            "address_2": "",
            "city": "Fort Mill",
            "state": "SC",
            "zip": "29715",
            "web": "",
            "price_rounding": 0,
            "parent_account": "82",
            "account_status": 777
          }, {
            "old_id": "85",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "West & Dunn Funeral Homes",
            "contact_person": "Norman Dunn",
            "email": "westanddunn@embarqmail.com",
            "phone": "910-594-1004  ",
            "fax": "910-594-0015",
            "address_1": "503 Raleigh Street ",
            "address_2": "",
            "city": "Newton Grove",
            "state": "NC",
            "zip": "28366",
            "web": "www.westanddunn.com",
            "price_rounding": 0,
            "parent_account": "85",
            "account_status": 777
          }, {
            "old_id": "86",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "West & Dunn Funeral Homes",
            "contact_person": "Norman Dunn",
            "email": "westanddunn@embarqmail.com",
            "phone": "910-594-1004  ",
            "fax": "910-594-0015",
            "address_1": "503 Raleigh Street ",
            "address_2": "",
            "city": "Newton Grove",
            "state": "NC",
            "zip": "28366",
            "web": "www.westanddunn.com",
            "price_rounding": 0,
            "parent_account": "85",
            "account_status": 777
          }, {
            "old_id": "87",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Paul Funeral Home",
            "contact_person": "Bobby Hodges",
            "email": "bobby@paulfuneralhome.com",
            "phone": "(252) 946-4144",
            "fax": "",
            "address_1": "900 John Small Ave",
            "address_2": "",
            "city": "Washington ",
            "state": "NC",
            "zip": "27889",
            "web": "www.paulfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "87",
            "account_status": 777
          }, {
            "old_id": "88",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Paul Funeral Home",
            "contact_person": "Bobby Hodges",
            "email": "bobby@paulfuneralhome.com",
            "phone": "(252) 946-4144",
            "fax": "",
            "address_1": "900 John Small Ave",
            "address_2": "",
            "city": "Washington ",
            "state": "NC",
            "zip": "27889",
            "web": "www.paulfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "87",
            "account_status": 777
          }, {
            "old_id": "89",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hillsman-Hix Funeral Home",
            "contact_person": "Jim Hix",
            "email": "jhix@tds.net",
            "phone": "(804) 561-2521",
            "fax": "",
            "address_1": "16409 Court Street",
            "address_2": "",
            "city": "Amelia Court House",
            "state": "VA",
            "zip": "23002",
            "web": "www.hillsmanhix.com",
            "price_rounding": 0,
            "parent_account": "89",
            "account_status": 777
          }, {
            "old_id": "90",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hillsman-Hix Funeral Home",
            "contact_person": "Jim Hix",
            "email": "jhix@tds.net",
            "phone": "(804) 561-2521",
            "fax": "",
            "address_1": "16409 Court Street",
            "address_2": "",
            "city": "Amelia Court House",
            "state": "VA",
            "zip": "23002",
            "web": "www.hillsmanhix.com",
            "price_rounding": 0,
            "parent_account": "89",
            "account_status": 777
          }, {
            "old_id": "91",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Kahlert Funerals & Cremations",
            "contact_person": "Ted Kahlert",
            "email": "kahlertfunerals@yahoo.com",
            "phone": "(910) 743-3333",
            "fax": "",
            "address_1": "308 Main Street",
            "address_2": "",
            "city": "Maysville",
            "state": "NC",
            "zip": "28555",
            "web": "",
            "price_rounding": 0,
            "parent_account": "91",
            "account_status": 777
          }, {
            "old_id": "92",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Kahlert Funerals & Cremations",
            "contact_person": "Ted Kahlert",
            "email": "kahlertfunerals@yahoo.com",
            "phone": "(910) 743-3333",
            "fax": "",
            "address_1": "308 Main Street",
            "address_2": "",
            "city": "Maysville",
            "state": "NC",
            "zip": "28555",
            "web": "",
            "price_rounding": 0,
            "parent_account": "91",
            "account_status": 777
          }, {
            "old_id": "93",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joseph Jenkins Funeral Home",
            "contact_person": "Joseph Jenkins III",
            "email": "joe1sky@aol.com",
            "phone": "(804) 358-9177",
            "fax": "",
            "address_1": "2011 Grayland Ave",
            "address_2": "",
            "city": "Richmond",
            "state": "VA",
            "zip": "23220",
            "web": "jenkinjr.com",
            "price_rounding": 0,
            "parent_account": "93",
            "account_status": 777
          }, {
            "old_id": "94",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joseph Jenkins Funeral Home",
            "contact_person": "Joseph Jenkins III",
            "email": "joe1sky@aol.com",
            "phone": "(804) 358-9177",
            "fax": "",
            "address_1": "2011 Grayland Ave",
            "address_2": "",
            "city": "Richmond",
            "state": "VA",
            "zip": "23220",
            "web": "jenkinjr.com",
            "price_rounding": 0,
            "parent_account": "93",
            "account_status": 777
          }, {
            "old_id": "95",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Fonville & Dove Mortuary",
            "contact_person": "Johnnie Dove",
            "email": "fanddmort@embarqmail.com",
            "phone": "(252) 208-0252",
            "fax": "",
            "address_1": "815 N Queen Street",
            "address_2": "",
            "city": "Kinston",
            "state": "NC",
            "zip": "28503",
            "web": "www.fanddmortuary.com",
            "price_rounding": 0,
            "parent_account": "95",
            "account_status": 777
          }, {
            "old_id": "96",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Fonville & Dove Mortuary",
            "contact_person": "Johnnie Dove",
            "email": "fanddmort@embarqmail.com",
            "phone": "(252) 208-0252",
            "fax": "",
            "address_1": "815 N Queen Street",
            "address_2": "",
            "city": "Kinston",
            "state": "NC",
            "zip": "28503",
            "web": "www.fanddmortuary.com",
            "price_rounding": 0,
            "parent_account": "95",
            "account_status": 777
          }, {
            "old_id": "97",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "W. L. Fields Funeral Home",
            "contact_person": "William Fields",
            "email": "wlfieldsfh@aol.com",
            "phone": "(804) 478-4811",
            "fax": "",
            "address_1": "10814 Doyle Blvd.",
            "address_2": "",
            "city": "McKenney",
            "state": "VA",
            "zip": "23872",
            "web": "",
            "price_rounding": 0,
            "parent_account": "97",
            "account_status": 777
          }, {
            "old_id": "98",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "W. L. Fields Funeral Home",
            "contact_person": "William Fields",
            "email": "wlfieldsfh@aol.com",
            "phone": "(804) 478-4811",
            "fax": "",
            "address_1": "10814 Doyle Blvd.",
            "address_2": "",
            "city": "McKenney",
            "state": "VA",
            "zip": "23878",
            "web": "",
            "price_rounding": 0,
            "parent_account": "97",
            "account_status": 777
          }, {
            "old_id": "99",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Mayer Funeral Home",
            "contact_person": "Robert Mayer",
            "email": "mail@mayerfuneralhome.com",
            "phone": "843-546-4184",
            "fax": "",
            "address_1": "P.O. Box 2838",
            "address_2": "",
            "city": "Georgetown",
            "state": "SC",
            "zip": "29442",
            "web": "www.mayerfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "99",
            "account_status": 777
          }, {
            "old_id": "102",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Thomasson Watson Funeral Service",
            "contact_person": "D D Watson",
            "email": "dwatsonmortician@aol.com",
            "phone": "(540) 967-1890",
            "fax": "",
            "address_1": "117 West Street",
            "address_2": "",
            "city": "Louisa",
            "state": "VA",
            "zip": "23093",
            "web": "www.ddwatsonmortician.com",
            "price_rounding": 0,
            "parent_account": "102",
            "account_status": 777
          }, {
            "old_id": "103",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Thomasson Watson Funeral Service",
            "contact_person": "D D Watson",
            "email": "dwatsonmortician@aol.com",
            "phone": "(540) 967-1890",
            "fax": "",
            "address_1": "117 West Street",
            "address_2": "",
            "city": "Louisa",
            "state": "VA",
            "zip": "23093",
            "web": "www.ddwatsonmortician.com",
            "price_rounding": 0,
            "parent_account": "102",
            "account_status": 777
          }, {
            "old_id": "104",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Amos & Son Funeral Home",
            "contact_person": "Noah Peterson",
            "email": "amossonsfh@yahoo.com",
            "phone": "803-275-3315",
            "fax": "",
            "address_1": "412 Railroad Ave.",
            "address_2": "",
            "city": "Johnston",
            "state": "SC",
            "zip": "29832",
            "web": "",
            "price_rounding": 0,
            "parent_account": "104",
            "account_status": 777
          }, {
            "old_id": "105",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Amos & Son Funeral Home",
            "contact_person": "Noah Peterson",
            "email": "amossonsfh@yahoo.com",
            "phone": "803-275-3315",
            "fax": "",
            "address_1": "412 Railroad Ave.",
            "address_2": "",
            "city": "Johnston",
            "state": "SC",
            "zip": "29832",
            "web": "",
            "price_rounding": 0,
            "parent_account": "104",
            "account_status": 777
          }, {
            "old_id": "106",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "S. E. Thomas Funeral & Cremation Service",
            "contact_person": "Ann & Juanita",
            "email": "Sethomas03@aol.com",
            "phone": "336-475-1945",
            "fax": "336-475-8196",
            "address_1": "110 Highland Avenue",
            "address_2": "",
            "city": "Thomasville",
            "state": "NC",
            "zip": "27360",
            "web": "sethomasfuneralandcremationservices.com",
            "price_rounding": 0,
            "parent_account": "106",
            "account_status": 777
          }, {
            "old_id": "107",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Kelsey Funeral Home",
            "contact_person": "Cheryl",
            "email": "",
            "phone": "704-982-6313",
            "fax": "",
            "address_1": "217 Center Street",
            "address_2": "",
            "city": "Albemarle",
            "state": "NC",
            "zip": "28001",
            "web": "Www.kelseyfh.com",
            "price_rounding": 0,
            "parent_account": "107",
            "account_status": 777
          }, {
            "old_id": "108",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Lowe-Neddo Funeral Home",
            "contact_person": "Michael Neddo",
            "email": "Michael@LoweNeddo.com",
            "phone": "704-545-3553",
            "fax": "7045457299",
            "address_1": "4715 Margaret Wallace Rd",
            "address_2": "",
            "city": "Matthews",
            "state": "NC",
            "zip": "28105",
            "web": "Www.loweneddofuneralhome.com",
            "price_rounding": 0,
            "parent_account": "108",
            "account_status": 777
          }, {
            "old_id": "109",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Lowe-Neddo Funeral Home",
            "contact_person": "Michael Neddo",
            "email": "Michael@LoweNeddo.com",
            "phone": "704-545-3553",
            "fax": "7045457299",
            "address_1": "4715 Margaret Wallace Rd",
            "address_2": "",
            "city": "Matthews",
            "state": "NC",
            "zip": "28105",
            "web": "Www.loweneddofuneralhome.com",
            "price_rounding": 0,
            "parent_account": "108",
            "account_status": 777
          }, {
            "old_id": "110",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Givens Funeral Home",
            "contact_person": "Robert Givens",
            "email": "Rgivens9@verizon.net",
            "phone": "540-921-1650",
            "fax": "",
            "address_1": "110 Woodrum Street",
            "address_2": "",
            "city": "Pearisburg",
            "state": "Va",
            "zip": "24134",
            "web": "Www.givensfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "110",
            "account_status": 777
          }, {
            "old_id": "111",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Carter Funeral Home",
            "contact_person": "Phillip Smoak",
            "email": "Cfh@intrstar.net",
            "phone": "910 529 4001",
            "fax": "",
            "address_1": "111 North Ingold",
            "address_2": "",
            "city": "Garland",
            "state": "NC",
            "zip": "28441",
            "web": " www.carterfh.com",
            "price_rounding": 0,
            "parent_account": "111",
            "account_status": 777
          }, {
            "old_id": "112",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Carter Funeral Home",
            "contact_person": "Phillip Smoak",
            "email": "Cfh@intrstar.net",
            "phone": "910 529 4001",
            "fax": "",
            "address_1": "111 North Ingold",
            "address_2": "",
            "city": "Garland",
            "state": "NC",
            "zip": "28441",
            "web": " www.carterfh.com",
            "price_rounding": 0,
            "parent_account": "111",
            "account_status": 777
          }, {
            "old_id": "113",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Martin Funeral Home LLC",
            "contact_person": "John Martin",
            "email": "9435076@centurylink.net",
            "phone": "803-625-4402",
            "fax": "",
            "address_1": "161 Clarke Ave. South",
            "address_2": "",
            "city": "Estill",
            "state": "SC",
            "zip": "29918",
            "web": "www.martinfuneralhomellc.com",
            "price_rounding": 0,
            "parent_account": "113",
            "account_status": 777
          }, {
            "old_id": "114",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Martin Funeral Home LLC",
            "contact_person": "John Martin",
            "email": "9435076@centurylink.net",
            "phone": "803-625-4402",
            "fax": "",
            "address_1": "161 Clarke Ave. South",
            "address_2": "",
            "city": "Estill",
            "state": "SC",
            "zip": "29918",
            "web": "www.martinfuneralhomellc.com",
            "price_rounding": 0,
            "parent_account": "113",
            "account_status": 777
          }, {
            "old_id": "115",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Williford Funeral Home",
            "contact_person": "Williford",
            "email": "tom@willifordfuneralhome.com",
            "phone": "919-552-2211",
            "fax": "",
            "address_1": "201 East Acadeny Street",
            "address_2": "",
            "city": "Fuquay-Varina",
            "state": "NC",
            "zip": "27526",
            "web": "www.willifordfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "115",
            "account_status": 777
          }, {
            "old_id": "118",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Tri County Funeral Service",
            "contact_person": "Linda Williams",
            "email": "liwilliams@centurylink.net",
            "phone": "252-746-6070",
            "fax": "",
            "address_1": "5426 Front Lane",
            "address_2": "",
            "city": "Ayden ",
            "state": "NC",
            "zip": "28513",
            "web": "tricountyfuneralservices@yahoo.com",
            "price_rounding": 0,
            "parent_account": "118",
            "account_status": 777
          }, {
            "old_id": "119",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Tri County Funeral Service",
            "contact_person": "Linda Williams",
            "email": "liwilliams@centurylink.net",
            "phone": "252-746-6070",
            "fax": "",
            "address_1": "5426 Front Lane",
            "address_2": "",
            "city": "Ayden ",
            "state": "NC",
            "zip": "28513",
            "web": "tricountyfuneralservices@yahoo.com",
            "price_rounding": 0,
            "parent_account": "118",
            "account_status": 777
          }, {
            "old_id": "120",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "V.Y. Scott Funeral Homes",
            "contact_person": "Paul Wilson",
            "email": "p_wilson53@yahoo.com",
            "phone": "804-561-2022",
            "fax": "",
            "address_1": "14001 Patrick Henry Highway",
            "address_2": "",
            "city": "Amelia ",
            "state": "VA",
            "zip": "23002",
            "web": "",
            "price_rounding": 0,
            "parent_account": "120",
            "account_status": 777
          }, {
            "old_id": "121",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "V.Y. Scott Funeral Homes",
            "contact_person": "Paul Wilson",
            "email": "p_wilson53@yahoo.com",
            "phone": "804-561-2022",
            "fax": "",
            "address_1": "14001 Patrick Henry Highway",
            "address_2": "",
            "city": "Amelia ",
            "state": "VA",
            "zip": "23002",
            "web": "",
            "price_rounding": 0,
            "parent_account": "120",
            "account_status": 777
          }, {
            "old_id": "122",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hornes Funeral Home",
            "contact_person": "Denise Horne",
            "email": "hornesfuneralhome@centurylink.net",
            "phone": "(252) 753-2320",
            "fax": "",
            "address_1": "9171 West Marlboro Road",
            "address_2": "",
            "city": "Farmville",
            "state": "NC",
            "zip": "27828",
            "web": "",
            "price_rounding": 0,
            "parent_account": "122",
            "account_status": 777
          }, {
            "old_id": "123",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hornes Funeral Home",
            "contact_person": "Willie Horne",
            "email": "www.hornesfuneralhome@centurylink.net",
            "phone": "(252) 753-2320",
            "fax": "(252) 753-0038",
            "address_1": "9171 W. Marlboro Rd.",
            "address_2": "",
            "city": "Farmville",
            "state": "NC",
            "zip": "27828",
            "web": "www.hornesfh.com",
            "price_rounding": 0,
            "parent_account": "123",
            "account_status": 777
          }, {
            "old_id": "124",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hornes Funeral Home",
            "contact_person": "Willie Horne",
            "email": "www.hornesfuneralhome@centurylink.net",
            "phone": "(252) 753-2320",
            "fax": "(252) 753-0038",
            "address_1": "9171 W. Marlboro Rd.",
            "address_2": "",
            "city": "Farmville",
            "state": "NC",
            "zip": "27828",
            "web": "www.hornesfh.com",
            "price_rounding": 0,
            "parent_account": "123",
            "account_status": 777
          }, {
            "old_id": "125",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Mackie Funeral Home",
            "contact_person": "Ed Whisenant",
            "email": "Cordell@mackiefh.com",
            "phone": "828-396-3385",
            "fax": "",
            "address_1": "35 Duke Street",
            "address_2": "",
            "city": "Granite Falls ",
            "state": "Nor",
            "zip": "28630",
            "web": "Mackiefh.com",
            "price_rounding": 0,
            "parent_account": "125",
            "account_status": 777
          }, {
            "old_id": "127",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Thacker Brothers Funeral Home",
            "contact_person": "Bradley Howdyshell",
            "email": "Bradley@thackerbrothers.com",
            "phone": "434-286-2791",
            "fax": "",
            "address_1": "650 Valley Street",
            "address_2": "",
            "city": "Scottsville",
            "state": "Va",
            "zip": "24590",
            "web": "Thackerbrothers.com",
            "price_rounding": 0,
            "parent_account": "127",
            "account_status": 777
          }, {
            "old_id": "128",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "H.D. Pope Funeral Home",
            "contact_person": "Claude H. Anthony, Jr.",
            "email": "canthonyjr@gmail.com",
            "phone": "(252) 446-9696",
            "fax": "",
            "address_1": "325 Nash Street",
            "address_2": "",
            "city": "Rocky Mount",
            "state": "NC",
            "zip": "27804",
            "web": "www.hdpopefuneralhome.com",
            "price_rounding": "5",
            "parent_account": "128",
            "account_status": 777
          }, {
            "old_id": "129",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "H.D. Pope Funeral Home",
            "contact_person": "Claude H. Anthony, Jr.",
            "email": "canthonyjr@gmail.com",
            "phone": "(252) 446-9696",
            "fax": "",
            "address_1": "325 Nash Street",
            "address_2": "",
            "city": "Rocky Mount",
            "state": "NC",
            "zip": "27804",
            "web": "www.hdpopefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "128",
            "account_status": 777
          }, {
            "old_id": "130",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "H.D. Pope Funeral Home - Nashville",
            "contact_person": "Claude H. Anthony, Jr.",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "130",
            "account_status": 777
          }, {
            "old_id": "131",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "H.D. Pope Funeral Home - Nashville",
            "contact_person": "C. H. Anthony, Jr.",
            "email": "chanthonyjr@gmail.com",
            "phone": "(252) 459-6003",
            "fax": "",
            "address_1": "226 South Barnes Street",
            "address_2": "",
            "city": "Nashville",
            "state": "NC ",
            "zip": "27856",
            "web": "www.hdpopefuneralhome.com",
            "price_rounding": "5",
            "parent_account": "131",
            "account_status": 777
          }, {
            "old_id": "132",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "H.D. Pope Funeral Home - Nashville",
            "contact_person": "C. H. Anthony, Jr.",
            "email": "chanthonyjr@gmail.com",
            "phone": "(252) 459-6003",
            "fax": "",
            "address_1": "226 South Barnes Street",
            "address_2": "",
            "city": "Nashville",
            "state": "NC ",
            "zip": "27856",
            "web": "www.hdpopefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "131",
            "account_status": 777
          }, {
            "old_id": "133",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Ray Funeral & Cremation Service",
            "contact_person": "Jesse Ray",
            "email": "Rayandallenfs@aol.com",
            "phone": "828-252-5521",
            "fax": "",
            "address_1": "127 McDowell Street",
            "address_2": "",
            "city": "Asheville",
            "state": "NC",
            "zip": "28801",
            "web": "Rayandallenfuneralservice.com",
            "price_rounding": 0,
            "parent_account": "133",
            "account_status": 777
          }, {
            "old_id": "135",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Williford Funeral Home",
            "contact_person": "Williford",
            "email": "tom@willifordfuneralhome.com",
            "phone": "919-552-2211",
            "fax": "",
            "address_1": "201 East Acadeny Street",
            "address_2": "",
            "city": "Fuquay-Varina",
            "state": "NC",
            "zip": "27526",
            "web": "www.willifordfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "115",
            "account_status": 777
          }, {
            "old_id": "136",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "S. Jones Funeral & Cremation",
            "contact_person": "Shaun Jones",
            "email": "s.jonesfh@aol.com",
            "phone": "252-343-4552",
            "fax": "",
            "address_1": "115 South Dennis St.",
            "address_2": "",
            "city": "Enfield",
            "state": "NC",
            "zip": "27823",
            "web": "",
            "price_rounding": 0,
            "parent_account": "136",
            "account_status": 777
          }, {
            "old_id": "137",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "S. Jones Funeral & Cremation",
            "contact_person": "Shaun Jones",
            "email": "JNSH725@aol.com",
            "phone": "252-343-4552",
            "fax": "",
            "address_1": "115 South Dennis St.",
            "address_2": "",
            "city": "Enfield",
            "state": "NC",
            "zip": "27823",
            "web": "",
            "price_rounding": 0,
            "parent_account": "136",
            "account_status": 777
          }, {
            "old_id": "139",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Paul Funeral Home",
            "contact_person": "Shelia",
            "email": "Shelia@paulfuneralhome.com",
            "phone": "252 - 946 - 4144",
            "fax": "",
            "address_1": "900 John Small Ave",
            "address_2": "",
            "city": "Washington ",
            "state": "NC",
            "zip": "27889",
            "web": "www.paulfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "139",
            "account_status": 777
          }, {
            "old_id": "140",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Paul Funeral Home",
            "contact_person": "Shelia",
            "email": "Shelia@paulfuneralhome.com",
            "phone": "252 - 946 - 4144",
            "fax": "",
            "address_1": "900 John Small Ave",
            "address_2": "",
            "city": "Washington ",
            "state": "NC",
            "zip": "27889",
            "web": "www.paulfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "139",
            "account_status": 777
          }, {
            "old_id": "141",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Jenkins Funeral Home & Cremation Service",
            "contact_person": "Carl & Lori Jenkins",
            "email": "",
            "phone": "828-464-1555",
            "fax": "",
            "address_1": "408 Starttown Road",
            "address_2": "",
            "city": "Newton",
            "state": "Nc",
            "zip": "28658",
            "web": "Www.jenkinsfuneralhome.net",
            "price_rounding": 0,
            "parent_account": "141",
            "account_status": 777
          }, {
            "old_id": "142",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Saunders Funeral Home",
            "contact_person": "Willie Saunders",
            "email": "WillieSaunders@yahoo.com",
            "phone": "(910) 938 - 2151",
            "fax": "",
            "address_1": "210 Bell Fork Road",
            "address_2": "",
            "city": "Jacksonville",
            "state": "NC",
            "zip": "28540",
            "web": "SaundersFHinc@yahoo.com",
            "price_rounding": 0,
            "parent_account": "142",
            "account_status": 777
          }, {
            "old_id": "143",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Saunders Funeral Home",
            "contact_person": "Willie Saunders",
            "email": "",
            "phone": "(910) 938 - 2151",
            "fax": "",
            "address_1": "210 Bell Fork Road",
            "address_2": "",
            "city": "Jacksonville",
            "state": "NC",
            "zip": "28540",
            "web": "www.saundersfh.com",
            "price_rounding": 0,
            "parent_account": "142",
            "account_status": 777
          }, {
            "old_id": "145",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Betts and Son Funeral Home",
            "contact_person": "Betts",
            "email": "Bettsfh1@embraqmail.com",
            "phone": "919-691-7185",
            "fax": "",
            "address_1": "512 Lewis Street",
            "address_2": "",
            "city": "Oxford",
            "state": "NC",
            "zip": "27565",
            "web": "www.BettsAndSonFuneralHome.com",
            "price_rounding": 0,
            "parent_account": "145",
            "account_status": 777
          }, {
            "old_id": "146",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Betts and Son Funeral Home",
            "contact_person": "Betts",
            "email": "Bettsfh1@embraqmail.com",
            "phone": "919-691-7185",
            "fax": "",
            "address_1": "512 Lewis Street",
            "address_2": "",
            "city": "Oxford",
            "state": "NC",
            "zip": "27565",
            "web": "www.bettsandsonfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "145",
            "account_status": 777
          }, {
            "old_id": "148",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "H.M. Colvin Funeral Home",
            "contact_person": "Mitch Colvin",
            "email": "hmcolvin@aol.com",
            "phone": "910-488-6047",
            "fax": "",
            "address_1": "2010 Murchison Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28311",
            "web": "www.HMColvin.com",
            "price_rounding": 0,
            "parent_account": "148",
            "account_status": 777
          }, {
            "old_id": "149",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "H.M. Colvin Funeral Home",
            "contact_person": "Mitch Colvin",
            "email": "hmcolvin@aol.com",
            "phone": "910-488-6047",
            "fax": "",
            "address_1": "2010 Murchison Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28311",
            "web": "www.HMColvin.com",
            "price_rounding": 0,
            "parent_account": "148",
            "account_status": 777
          }, {
            "old_id": "150",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Jeffress Funeral Home",
            "contact_person": "Jeffers brookneal",
            "email": "Jeffressfh@aol.com",
            "phone": "434-376-2070",
            "fax": "",
            "address_1": "304 Lusardi Drive",
            "address_2": "",
            "city": "Brookneal",
            "state": "Va",
            "zip": "24528",
            "web": "www.jeffressfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "150",
            "account_status": 777
          }, {
            "old_id": "151",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Jeffress Funeral Home",
            "contact_person": "Jeffers brookneal",
            "email": "Jeffressfh@aol.com",
            "phone": "434-376-2070",
            "fax": "",
            "address_1": "304 Lusardi Drive",
            "address_2": "",
            "city": "Brookneal",
            "state": "Va",
            "zip": "24528",
            "web": "www.jeffressfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "150",
            "account_status": 777
          }, {
            "old_id": "152",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bear Funeral Home",
            "contact_person": "Will Bear",
            "email": "bearfh001@yahoo.com",
            "phone": "(540) 337-7188",
            "fax": "",
            "address_1": "14 Green Hill Ln",
            "address_2": "P.O. Box 602",
            "city": "Churchville",
            "state": "VA",
            "zip": "24421",
            "web": "bearfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "152",
            "account_status": 777
          }, {
            "old_id": "153",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Bear Funeral Homes",
            "contact_person": "Will Bear",
            "email": "",
            "phone": "(540) 337-7188",
            "fax": "",
            "address_1": "14 Green Hill Ln",
            "address_2": "",
            "city": "Churchville",
            "state": "VA",
            "zip": "24421",
            "web": "bearfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "152",
            "account_status": 777
          }, {
            "old_id": "154",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hunter-Odom Funeral Service",
            "contact_person": "Crystal",
            "email": "hofs01@yahoo.com",
            "phone": "252-977-3122",
            "fax": "",
            "address_1": "240 Atlantic Avenue",
            "address_2": "",
            "city": "Rocky Mount ",
            "state": "NC",
            "zip": "27802",
            "web": "www.hunterodomfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "154",
            "account_status": 777
          }, {
            "old_id": "155",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hunter-Odom Funeral Service",
            "contact_person": "Crystal",
            "email": "hofs01@yahoo.com",
            "phone": "252-977-3122",
            "fax": "",
            "address_1": "240 Atlantic Avenue",
            "address_2": "",
            "city": "Rocky Mount ",
            "state": "NC",
            "zip": "27802",
            "web": "www.hunterodomfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "154",
            "account_status": 777
          }, {
            "old_id": "175",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "S. E. Thomas Funeral & Cremation Service",
            "contact_person": "Ann & Juanita",
            "email": "Sethomas03@aol.com",
            "phone": "336-475-1945",
            "fax": "336-475-8196",
            "address_1": "110 Highland Avenue",
            "address_2": "",
            "city": "Thomasville",
            "state": "NC",
            "zip": "27360",
            "web": "sethomasfuneralandcremationservices.com",
            "price_rounding": 0,
            "parent_account": "106",
            "account_status": 777
          }, {
            "old_id": "182",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "J M Wilkerson Funeral Establishment, Inc.",
            "contact_person": "Shelton W. Smith",
            "email": "sheltonsmith@jmwilkersonsince1874.com",
            "phone": "(804) 732-8911",
            "fax": "",
            "address_1": "102 South Ave.",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "23803",
            "web": "www.jmwilkersonsince1874.com",
            "price_rounding": 0,
            "parent_account": "182",
            "account_status": 777
          }, {
            "old_id": "183",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "J M Wilkerson Funeral Establishment, Inc.",
            "contact_person": "Shelton W. Smith",
            "email": "sheltonsmith@jmwilkersonsince1874.com",
            "phone": "(804) 732-8911",
            "fax": "",
            "address_1": "102 South Ave.",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "23803",
            "web": "www.jmwilkersonsince1874.com",
            "price_rounding": 0,
            "parent_account": "182",
            "account_status": 777
          }, {
            "old_id": "186",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Dunn Funeral Home And Cremation Services, Inc.",
            "contact_person": "Bob & Brenda Moore-Dunn",
            "email": "dunnfh@gmail.com",
            "phone": "(910) 259-9400",
            "fax": "",
            "address_1": "810 W.Wilmington Street",
            "address_2": "",
            "city": "Burgaw",
            "state": "NC",
            "zip": "28425",
            "web": "www.dunnfuneralhomeandcremationservicesinc.com",
            "price_rounding": 0,
            "parent_account": "186",
            "account_status": 777
          }, {
            "old_id": "187",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Dunn Funeral Home And Cremation Services, Inc.",
            "contact_person": "Bob & Brenda Moore-Dunn",
            "email": "",
            "phone": "(910) 259-9400",
            "fax": "",
            "address_1": "810 W.Wilmington Street",
            "address_2": "",
            "city": "Burgaw",
            "state": "NC",
            "zip": "28425",
            "web": "www.dunn funeralhomeandcremationservicesinc.com",
            "price_rounding": 0,
            "parent_account": "186",
            "account_status": 777
          }, {
            "old_id": "188",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Carrons Funeral Home",
            "contact_person": "Carla King Horne",
            "email": "Cfhaka2@aol.com",
            "phone": "(252) 237-2169",
            "fax": "",
            "address_1": "325 E. Nash Street",
            "address_2": "",
            "city": "Wilson",
            "state": "NC",
            "zip": "27893",
            "web": "www.carronsfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "188",
            "account_status": 777
          }, {
            "old_id": "189",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Carrons Funeral Home",
            "contact_person": "Carla King Horne",
            "email": "Cfhaka2@aol.com",
            "phone": "(252) 237-2169",
            "fax": "",
            "address_1": "325 E. Nash Street",
            "address_2": "",
            "city": "Wilson",
            "state": "NC",
            "zip": "27893",
            "web": "www.carronsfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "188",
            "account_status": 777
          }, {
            "old_id": "190",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hilliard Funeral Home",
            "contact_person": "Donnie D. Johnson",
            "email": "hilliardfuneralhome@embarqmail.com",
            "phone": "(252) 437-2431",
            "fax": "",
            "address_1": "10206 N. US 301",
            "address_2": "",
            "city": "Whitakers",
            "state": "NC",
            "zip": "27891",
            "web": "www.hilliardfh.com",
            "price_rounding": 0,
            "parent_account": "190",
            "account_status": 777
          }, {
            "old_id": "191",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hilliard Funeral Home",
            "contact_person": "Donnie D. Johnson",
            "email": "hilliardfuneralhome@embarqmail.com",
            "phone": "(252) 437-2431",
            "fax": "",
            "address_1": "10206 N. US 301",
            "address_2": "",
            "city": "Whitakers",
            "state": "NC",
            "zip": "27891",
            "web": "www.hilliardfh.com",
            "price_rounding": 0,
            "parent_account": "190",
            "account_status": 777
          }, {
            "old_id": "192",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Weymouth Funeral Home",
            "contact_person": "Veronica A. Weymouth",
            "email": "weymouthfh@aol.com",
            "phone": "(757 930-2222",
            "fax": "",
            "address_1": "12746 Nettles Drive",
            "address_2": "",
            "city": "Newport News ",
            "state": "VA",
            "zip": "23606",
            "web": "www.weymouthfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "192",
            "account_status": 777
          }, {
            "old_id": "193",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Weymouth Funeral Home",
            "contact_person": "Veronica A. Weymouth",
            "email": "weymouthfh@aol.com",
            "phone": "(757 930-222",
            "fax": "",
            "address_1": "12746 Nettles Drive",
            "address_2": "",
            "city": "Newport News ",
            "state": "VA",
            "zip": "23606",
            "web": "www.weymouthfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "192",
            "account_status": 777
          }, {
            "old_id": "194",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Oris P. Jones Establishment",
            "contact_person": "Mary Johnson Fields",
            "email": "msmejohnson@msn.com",
            "phone": "(434) 447-7158",
            "fax": "",
            "address_1": "301 West Atlantic Street",
            "address_2": "",
            "city": "South Hill",
            "state": "VA",
            "zip": "23872",
            "web": "johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "194",
            "account_status": 777
          }, {
            "old_id": "195",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Oris P. Jones Establishment",
            "contact_person": "Mary Johnson Fields",
            "email": "msmejohnson@msn.com",
            "phone": "(434) 447-7158",
            "fax": "",
            "address_1": "301 West Atlantic Street",
            "address_2": "",
            "city": "South Hill",
            "state": "VA",
            "zip": "23872",
            "web": "johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "194",
            "account_status": 777
          }, {
            "old_id": "196",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joseph M. Johnson & Son Funeral Homes",
            "contact_person": "Joe M Johnson, Jr.",
            "email": "joe@johnsonservices.us",
            "phone": "(804) 478-4411",
            "fax": "",
            "address_1": "11107 Doyle Blvd. ",
            "address_2": "",
            "city": "McKenney",
            "state": "VA",
            "zip": "23872",
            "web": "johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "196",
            "account_status": 777
          }, {
            "old_id": "197",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joseph M. Johnson & Son Funeral Homes",
            "contact_person": "Joe M Johnson, Jr.",
            "email": "joe@johnsonservices.us",
            "phone": "(804) 478-4411",
            "fax": "",
            "address_1": "11107 Doyle Blvd. ",
            "address_2": "",
            "city": "McKenney",
            "state": "VA",
            "zip": "23872",
            "web": "johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "196",
            "account_status": 777
          }, {
            "old_id": "198",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joseph M. Johnson & Son Funeral Homes",
            "contact_person": "Joesph M. Johnson, Jr.",
            "email": "joe@johnsonservices.us",
            "phone": "(804) 863-4411",
            "fax": "",
            "address_1": "530 S. Sycamore Street",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "28303",
            "web": "www.johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "198",
            "account_status": 777
          }, {
            "old_id": "199",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joseph M. Johnson & Son Funeral Homes",
            "contact_person": "Joesph M. Johnson, Jr.",
            "email": "joe@johnsonservices.us",
            "phone": "(804) 863-4411",
            "fax": "",
            "address_1": "530 S. Sycamore Street",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "28303",
            "web": "www.johnsonservices.us",
            "price_rounding": 0,
            "parent_account": "198",
            "account_status": 777
          }, {
            "old_id": "200",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Graves Funeral Home",
            "contact_person": "Tommy Graves, III",
            "email": "gravesfhinc@yahoo.com",
            "phone": "(757) 622-1085",
            "fax": "",
            "address_1": "1631 Church Street",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.gravesfuneralhomeinc.com",
            "price_rounding": 0,
            "parent_account": "200",
            "account_status": 777
          }, {
            "old_id": "201",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Graves Funeral Home",
            "contact_person": "Tommy Graves, III",
            "email": "gravesfhinc@yahoo.com",
            "phone": "(757) 622-1085",
            "fax": "",
            "address_1": "1631 Church Street",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.gravesfuneralhomeinc.com",
            "price_rounding": 0,
            "parent_account": "200",
            "account_status": 777
          }, {
            "old_id": "202",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Beach Funeral & Cremation Services",
            "contact_person": "Kevin A. Sanderlin",
            "email": "ksanderlin@cox.net",
            "phone": "(757) 499-8999",
            "fax": "",
            "address_1": "4456 Bonney Road",
            "address_2": "",
            "city": "Virginia Beach",
            "state": "VA",
            "zip": "23462",
            "web": "www.beachfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "202",
            "account_status": 777
          }, {
            "old_id": "203",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Beach Funeral & Cremation Services",
            "contact_person": "Kevin A. Sanderlin",
            "email": "ksanderlin@cox.net",
            "phone": "(757) 499-8999",
            "fax": "",
            "address_1": "4456 Bonney Road",
            "address_2": "",
            "city": "Virginia Beach",
            "state": "VA",
            "zip": "23462",
            "web": "www.beachfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "202",
            "account_status": 777
          }, {
            "old_id": "204",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Pretlow & Sons Funeral Home",
            "contact_person": "Richard G. Pretlow",
            "email": "info@pretlowandsons.com",
            "phone": "(757) 543-9343",
            "fax": "",
            "address_1": "500 Liberty Street",
            "address_2": "",
            "city": "Chesapeake",
            "state": "VA",
            "zip": "23324",
            "web": "www.pretlowandsons.com",
            "price_rounding": 0,
            "parent_account": "204",
            "account_status": 777
          }, {
            "old_id": "205",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Pretlow & Sons Funeral Home",
            "contact_person": "Richard G. Pretlow",
            "email": "info@pretlowandsons.com",
            "phone": "(757) 543-9343",
            "fax": "",
            "address_1": "500 Liberty Street",
            "address_2": "",
            "city": "Chesapeake",
            "state": "VA",
            "zip": "23324",
            "web": "www.pretlowandsons.com",
            "price_rounding": 0,
            "parent_account": "204",
            "account_status": 777
          }, {
            "old_id": "206",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Riddick Funeral Service",
            "contact_person": "Paul Riddick, Jr.",
            "email": "pjpr76@aol.com",
            "phone": "(757) 855-9010",
            "fax": "",
            "address_1": "1225 Norview Ave.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23513",
            "web": "www.riddickfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "206",
            "account_status": 777
          }, {
            "old_id": "207",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Riddick Funeral Service",
            "contact_person": "Paul Riddick, Jr.",
            "email": "pjpr76@aol.com",
            "phone": "(757) 855-9010",
            "fax": "",
            "address_1": "1225 Norview Ave.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23513",
            "web": "www.riddickfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "206",
            "account_status": 777
          }, {
            "old_id": "208",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hale Funeral Home - Norfolk",
            "contact_person": "Christopher J. Wilson",
            "email": "halefuneralhome@hotmail.com",
            "phone": "(757) 622-2321",
            "fax": "",
            "address_1": "2100 Ballentine Blvd.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.halefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "208",
            "account_status": 777
          }, {
            "old_id": "209",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hale Funeral Home - Norfolk",
            "contact_person": "Christopher J. Wilson",
            "email": "halefuneralhome@hotmail.com",
            "phone": "(757) 622-2321",
            "fax": "",
            "address_1": "2100 Ballentine Blvd.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.halefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "208",
            "account_status": 777
          }, {
            "old_id": "210",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Community Funeral Home",
            "contact_person": "Steven G. Felton, Sr.",
            "email": "stevenfelton30@yahoo.com",
            "phone": "(757) 625-7415",
            "fax": "",
            "address_1": "1210 Ballentine Blvd.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.communityfh.com",
            "price_rounding": 0,
            "parent_account": "210",
            "account_status": 777
          }, {
            "old_id": "211",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Community Funeral Home",
            "contact_person": "Steven G. Felton, Sr.",
            "email": "stevenfelton30@yahoo.com",
            "phone": "(757) 625-7415",
            "fax": "",
            "address_1": "1210 Ballentine Blvd.",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.communityfh.com",
            "price_rounding": 0,
            "parent_account": "210",
            "account_status": 777
          }, {
            "old_id": "212",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Cooke Bros. Funeral Chapel",
            "contact_person": "Marco",
            "email": "marco@cookebros,com",
            "phone": "(757) 380-0251",
            "fax": "",
            "address_1": "1601 27th Street",
            "address_2": "",
            "city": "Newport News ",
            "state": "VA",
            "zip": "23607",
            "web": "www.cookebros.com",
            "price_rounding": 0,
            "parent_account": "212",
            "account_status": 777
          }, {
            "old_id": "213",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Cooke Bros. Funeral Chapel",
            "contact_person": "Marco",
            "email": "marco@cookebros,com",
            "phone": "(757) 380-0251",
            "fax": "",
            "address_1": "1601 27th Street",
            "address_2": "",
            "city": "Newport News ",
            "state": "VA",
            "zip": "23607",
            "web": "www.cookebros.com",
            "price_rounding": 0,
            "parent_account": "212",
            "account_status": 777
          }, {
            "old_id": "216",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "R.W. Baker Funeral Home & Crematory",
            "contact_person": "Blake Baker",
            "email": "rwbaker@yahoo.com",
            "phone": "(757) 539-4691",
            "fax": "",
            "address_1": "509 W. Washington Street",
            "address_2": "",
            "city": "Suffolk",
            "state": "VA",
            "zip": "23434",
            "web": "www.rwbakerfh.com",
            "price_rounding": 0,
            "parent_account": "216",
            "account_status": 777
          }, {
            "old_id": "217",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "R.W. Baker Funeral Home & Crematory",
            "contact_person": "Blake Baker",
            "email": "rwbaker@yahoo.com",
            "phone": "(757) 539-4691",
            "fax": "",
            "address_1": "509 W. Washington Street",
            "address_2": "",
            "city": "Suffolk",
            "state": "VA",
            "zip": "23434",
            "web": "www.rwbakerfh.com",
            "price_rounding": 0,
            "parent_account": "216",
            "account_status": 777
          }, {
            "old_id": "218",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Cox-Price",
            "contact_person": "Clay Cox",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "218",
            "account_status": 777
          }, {
            "old_id": "219",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Haywood Funeral Home",
            "contact_person": "Haywood Funeral Home",
            "email": "haywoodfh@aol.com",
            "phone": "919-832-2835",
            "fax": "919-828-4920",
            "address_1": "2415 S. Wilmington Street",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27603",
            "web": "www.haywoodfh.com",
            "price_rounding": 0,
            "parent_account": "219",
            "account_status": 777
          }, {
            "old_id": "220",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Matthews Funeral Service",
            "contact_person": "Mona Gore",
            "email": "matthewsfuneralservice@embarqmail.com",
            "phone": "(910) 285 - 3349",
            "fax": "",
            "address_1": "310 Elizabeth Street",
            "address_2": "",
            "city": "Wallace",
            "state": "NC",
            "zip": "28466",
            "web": "www.matthewsfuneralservice.com",
            "price_rounding": 0,
            "parent_account": "220",
            "account_status": 777
          }, {
            "old_id": "221",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Matthews Funeral Service",
            "contact_person": "Mona Gore",
            "email": "matthewsfuneralservice@embarq.com",
            "phone": "(910) 285 - 3349",
            "fax": "",
            "address_1": "310 Elizabeth Street",
            "address_2": "",
            "city": "Wallace",
            "state": "NC",
            "zip": "28466",
            "web": "www.matthewsfuneralservice.com",
            "price_rounding": 0,
            "parent_account": "220",
            "account_status": 777
          }, {
            "old_id": "222",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Jenkins Funeral Homes & Cremation Service",
            "contact_person": "Carl Jenkins",
            "email": "Carljenkins@jenkinsfuneralhome.net",
            "phone": "828-464-1555",
            "fax": "",
            "address_1": "4081 Startown Rd",
            "address_2": "",
            "city": "Newton",
            "state": "NC",
            "zip": "28658",
            "web": "Www.jenkinsfuneralhome.net",
            "price_rounding": 0,
            "parent_account": "222",
            "account_status": 777
          }, {
            "old_id": "223",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Michael A. Glenn",
            "contact_person": "Michael Glenn",
            "email": "michaelaglenn@bellsouth.net",
            "phone": "864-580-2012",
            "fax": "",
            "address_1": "1319 Union St",
            "address_2": "",
            "city": "Spartanburg",
            "state": "SC",
            "zip": "29302",
            "web": "www.michaelglennfh.com",
            "price_rounding": 0,
            "parent_account": "223",
            "account_status": 777
          }, {
            "old_id": "224",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Michael A. Glenn",
            "contact_person": "Michael Glenn",
            "email": "michaelaglenn@bellsouth.net",
            "phone": "864-580-2012",
            "fax": "",
            "address_1": "1319 Union St",
            "address_2": "",
            "city": "Spartanburg",
            "state": "SC",
            "zip": "29302",
            "web": "www.michaelglennfh.com",
            "price_rounding": 0,
            "parent_account": "223",
            "account_status": 777
          }, {
            "old_id": "225",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Albert A Glover Funeral Home",
            "contact_person": "Alaine",
            "email": "www.aagloverfh.com",
            "phone": "843-871-1528",
            "fax": "",
            "address_1": "113 Bryan Street",
            "address_2": "",
            "city": "Summerville",
            "state": "SC",
            "zip": "29484",
            "web": "www.aagloverfh.com",
            "price_rounding": 0,
            "parent_account": "225",
            "account_status": 777
          }, {
            "old_id": "226",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Ideal Funeral Parlor",
            "contact_person": "Douglas",
            "email": "info@idealfuneral.com",
            "phone": "864-662-3581",
            "fax": "",
            "address_1": "106 East Darlington St",
            "address_2": "",
            "city": "Florence",
            "state": "SC",
            "zip": "29506",
            "web": "www.idealfuneral.com",
            "price_rounding": 0,
            "parent_account": "226",
            "account_status": 777
          }, {
            "old_id": "229",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Crumpler-Honeycutt Funeral Home",
            "contact_person": "Crumpler Honeycutt",
            "email": "Crumplerhoneycutt@yahoo.com",
            "phone": "910-592-2066",
            "fax": "",
            "address_1": "118 Fayetteville st",
            "address_2": "",
            "city": "Clinton ",
            "state": "NC",
            "zip": "28328",
            "web": " www.crumpler-Honeycutt.com",
            "price_rounding": 0,
            "parent_account": "229",
            "account_status": 777
          }, {
            "old_id": "230",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bowman Funeral Home",
            "contact_person": "Mary Bowman",
            "email": "bd0ver@bellsouth.net",
            "phone": "843-346-2242",
            "fax": "",
            "address_1": "510 N. Brockington St",
            "address_2": "",
            "city": "Timmonsville",
            "state": "SC",
            "zip": "29161",
            "web": "",
            "price_rounding": 0,
            "parent_account": "230",
            "account_status": 777
          }, {
            "old_id": "231",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Bowman Funeral Home",
            "contact_person": "Mary Bowman",
            "email": "bd0ver@bellsouth.net",
            "phone": "843-346-2242",
            "fax": "",
            "address_1": "510 N. Brockington St",
            "address_2": "",
            "city": "Timmonsville",
            "state": "SC",
            "zip": "29161",
            "web": "",
            "price_rounding": 0,
            "parent_account": "230",
            "account_status": 777
          }, {
            "old_id": "232",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Callaham-Hicks",
            "contact_person": "Leonard Hicks",
            "email": "leonardhicks@bellsouth.net",
            "phone": "864-582-2588",
            "fax": "",
            "address_1": "228 North Dean St",
            "address_2": "",
            "city": "Spartanburg",
            "state": "SC",
            "zip": "29302",
            "web": "www.callahamhicks.com",
            "price_rounding": 0,
            "parent_account": "232",
            "account_status": 777
          }, {
            "old_id": "233",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Omega Funeral Service",
            "contact_person": "Omega",
            "email": "omegafuneral@gmail.com",
            "phone": "336-395-8326",
            "fax": "",
            "address_1": "2120 May Drive",
            "address_2": "",
            "city": "Burlington",
            "state": "NC",
            "zip": "27215",
            "web": "www.omegafsc.com",
            "price_rounding": "5",
            "parent_account": "233",
            "account_status": 777
          }, {
            "old_id": "234",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Omega Funeral Service",
            "contact_person": "Omega",
            "email": "omegafuneral@gmail.com",
            "phone": "336-395-8326",
            "fax": "",
            "address_1": "2120 May Drive",
            "address_2": "",
            "city": "Burlington",
            "state": "NC",
            "zip": "27215",
            "web": "omegafsc.com",
            "price_rounding": 0,
            "parent_account": "233",
            "account_status": 777
          }, {
            "old_id": "236",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "JOHNSON-HALLS FUNERAL HOME",
            "contact_person": "Johnson-Halls",
            "email": "jhfuneralhome@bellsouth.net",
            "phone": "843-884-4252",
            "fax": "",
            "address_1": "440 VENNING STREET",
            "address_2": "",
            "city": "MT. PLEASANT",
            "state": "S.C",
            "zip": "29464",
            "web": "",
            "price_rounding": 0,
            "parent_account": "236",
            "account_status": 777
          }, {
            "old_id": "237",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "JOHNSON-HALLS FUNERAL HOME",
            "contact_person": "Johnson-Halls",
            "email": "jhfuneralhome@bellsouth.net",
            "phone": "843-884-4252",
            "fax": "",
            "address_1": "440 VENNING STREET",
            "address_2": "",
            "city": "MT. PLEASANT",
            "state": "S.C",
            "zip": "29464",
            "web": "",
            "price_rounding": 0,
            "parent_account": "236",
            "account_status": 777
          }, {
            "old_id": "238",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Johnson-Halls Funeral Home",
            "contact_person": "sam halls",
            "email": "jhfuneralhome@bellsouth.net",
            "phone": "843-884-4252",
            "fax": "843-971-9809",
            "address_1": "440 venning st.",
            "address_2": "",
            "city": "Mount Pleasant",
            "state": "SC",
            "zip": "29464",
            "web": "johnsonhallsfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "238",
            "account_status": 777
          }, {
            "old_id": "239",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Johnson-Halls Funeral Home",
            "contact_person": "sam halls",
            "email": "jhfuneralhome@bellsouth.net",
            "phone": "843-884-4252",
            "fax": "",
            "address_1": "440 venning st.",
            "address_2": "",
            "city": "Mount Pleasant",
            "state": "Sou",
            "zip": "29464",
            "web": "jhfuneralhome@bellsouth.net",
            "price_rounding": 0,
            "parent_account": "238",
            "account_status": 777
          }, {
            "old_id": "240",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Alexander Funeral Home, Inc.",
            "contact_person": "Mr. Alfred L. Alexander",
            "email": "iave@aol.com",
            "phone": "704-333-1167",
            "fax": "",
            "address_1": "1424 Statesville Ave.",
            "address_2": "",
            "city": "Charlotte",
            "state": "NC",
            "zip": "28206",
            "web": "",
            "price_rounding": 0,
            "parent_account": "240",
            "account_status": 777
          }, {
            "old_id": "241",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Alexander Funeral Home, Inc.",
            "contact_person": "Mr. Alfred L. Alexander",
            "email": "iave@aol.com",
            "phone": "704-333-1167",
            "fax": "",
            "address_1": "1424 Statesville Ave.",
            "address_2": "",
            "city": "Charlotte",
            "state": "NC",
            "zip": "28206",
            "web": "",
            "price_rounding": 0,
            "parent_account": "240",
            "account_status": 777
          }, {
            "old_id": "242",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hines Funeral Service",
            "contact_person": "Nathan",
            "email": "chines53@embarqmail.com",
            "phone": "276-656-3833",
            "fax": "",
            "address_1": "903 Starling Avenue",
            "address_2": "",
            "city": "Martinsville",
            "state": "VA",
            "zip": "24112",
            "web": "",
            "price_rounding": 0,
            "parent_account": "242",
            "account_status": 777
          }, {
            "old_id": "243",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hines Funeral Service",
            "contact_person": "Nathan",
            "email": "chines53@embarqmail.com",
            "phone": "276-656-3833",
            "fax": "",
            "address_1": "903 Starling Avenue",
            "address_2": "",
            "city": "Martinsville",
            "state": "VA",
            "zip": "24112",
            "web": "",
            "price_rounding": 0,
            "parent_account": "242",
            "account_status": 777
          }, {
            "old_id": "245",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Abbeville-White Mortuary",
            "contact_person": "Kelcey Elmore",
            "email": "awmort62@yahoo.com",
            "phone": "864-366-4332",
            "fax": "",
            "address_1": "100 Whitehall St",
            "address_2": "",
            "city": "Abbeville",
            "state": "SC",
            "zip": "29628",
            "web": "www.abbevillewhitemortuaryinc.com",
            "price_rounding": 0,
            "parent_account": "245",
            "account_status": 777
          }, {
            "old_id": "246",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Angels Funeral Service",
            "contact_person": "Charles Molette",
            "email": "charlesmolettefuneraldirector@yahoo.com",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "246",
            "account_status": 777
          }, {
            "old_id": "247",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Angels Funeral Service",
            "contact_person": "Charles Molette",
            "email": "charlesmolettefuneraldirector@yahoo.com",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "246",
            "account_status": 777
          }, {
            "old_id": "248",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Parks Family Funeral Home",
            "contact_person": "JA Parks",
            "email": "parksfunfh@yahoo.com",
            "phone": "864-229-3206",
            "fax": "8649430024",
            "address_1": "514 Hackett Ave.",
            "address_2": "",
            "city": "Greenwood",
            "state": "SC",
            "zip": "29646",
            "web": "www.parksfunhm@yahoo.com",
            "price_rounding": 0,
            "parent_account": "248",
            "account_status": 777
          }, {
            "old_id": "249",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Parks Family Funeral Home",
            "contact_person": "JA Parks",
            "email": "parksfunfh@yahoo.com",
            "phone": "864-229-3206",
            "fax": "",
            "address_1": "514 Hackett Ave.",
            "address_2": "",
            "city": "Greenwood",
            "state": "SC",
            "zip": "29646",
            "web": "",
            "price_rounding": 0,
            "parent_account": "248",
            "account_status": 777
          }, {
            "old_id": "250",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Unity Funeral Services",
            "contact_person": "Unity Funeral Services",
            "email": "unityfs@embarqmail.com",
            "phone": "910-860-3900",
            "fax": "",
            "address_1": "594-106 South Reilly Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28314",
            "web": "www.UnityFuneralServices.com",
            "price_rounding": 0,
            "parent_account": "250",
            "account_status": 777
          }, {
            "old_id": "251",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Unity Funeral Services",
            "contact_person": "Unity Funeral Services",
            "email": "unityfs@embarqmail.com",
            "phone": "910-860-3900",
            "fax": "",
            "address_1": "594-106 South Reilly Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28314",
            "web": "www.UnityFuneralServices.com",
            "price_rounding": 0,
            "parent_account": "250",
            "account_status": 777
          }, {
            "old_id": "252",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Sample of William Toney",
            "contact_person": "sample toney",
            "email": "",
            "phone": "919-478-3164",
            "fax": "",
            "address_1": "516 S. Poplar St",
            "address_2": "",
            "city": "Spring Hope\/Zebulon",
            "state": "NC",
            "zip": "0",
            "web": "www.WilliamToneys.com",
            "price_rounding": 0,
            "parent_account": "252",
            "account_status": 777
          }, {
            "old_id": "256",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Scott f.h.",
            "contact_person": "Will Scott",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Chatham",
            "state": "Va",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "256",
            "account_status": 777
          }, {
            "old_id": "257",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Townes f.h.",
            "contact_person": "David Fuquay",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Danville",
            "state": "Va",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "257",
            "account_status": 777
          }, {
            "old_id": "258",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Townes F. H.",
            "contact_person": "David Fuquay",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "Danville",
            "state": "Va.",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "258",
            "account_status": 777
          }, {
            "old_id": "259",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Nesmith- Pinckney Funeral Home",
            "contact_person": "Nesmith-Pinckney Funeral Home",
            "email": "",
            "phone": "843-558-5327",
            "fax": "",
            "address_1": "81 Sams Cr.",
            "address_2": "",
            "city": "Hemingway",
            "state": "SC",
            "zip": "29554",
            "web": "",
            "price_rounding": 0,
            "parent_account": "259",
            "account_status": 777
          }, {
            "old_id": "260",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Nesmith- Pinckney Funeral Home",
            "contact_person": "Nesmith-Pinckney Funeral Home",
            "email": "",
            "phone": "843-558-5327",
            "fax": "",
            "address_1": "81 Sams Cr.",
            "address_2": "",
            "city": "Hemingway",
            "state": "SC",
            "zip": "29554",
            "web": "",
            "price_rounding": 0,
            "parent_account": "259",
            "account_status": 777
          }, {
            "old_id": "261",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Genesis Funeral Services",
            "contact_person": "Todd",
            "email": "genesisfuneral@yahoo.com",
            "phone": "704-487-0624",
            "fax": "",
            "address_1": "407 E. Grover Street",
            "address_2": "",
            "city": "Shelby",
            "state": "NC",
            "zip": "28150",
            "web": "",
            "price_rounding": 0,
            "parent_account": "261",
            "account_status": 777
          }, {
            "old_id": "262",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Genesis Funeral Services",
            "contact_person": "Todd",
            "email": "genesisfuneral@yahoo.com",
            "phone": "704-487-0624",
            "fax": "",
            "address_1": "407 E. Grover Street",
            "address_2": "",
            "city": "Shelby",
            "state": "NC",
            "zip": "28150",
            "web": "",
            "price_rounding": 0,
            "parent_account": "261",
            "account_status": 777
          }, {
            "old_id": "264",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "",
            "contact_person": "Walker Memorial",
            "email": "Walkerfh@embarqmail.com",
            "phone": "Walker Memorial Fune",
            "fax": "",
            "address_1": "163 Irene Roberts Rd",
            "address_2": "",
            "city": "Lillington ",
            "state": "NC",
            "zip": "27546",
            "web": "www.walkermemorialfh.com",
            "price_rounding": 0,
            "parent_account": "264",
            "account_status": 777
          }, {
            "old_id": "265",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Walker Memorial Funeral Home",
            "contact_person": "Walker Memorial",
            "email": "Walkerfh@embarqmail.com",
            "phone": "Walker Memorial Fune",
            "fax": "",
            "address_1": "163 Irene Roberts Rd",
            "address_2": "",
            "city": "Lillington ",
            "state": "NC",
            "zip": "27546",
            "web": "www.walkermemorialfh.com",
            "price_rounding": 0,
            "parent_account": "265",
            "account_status": 777
          }, {
            "old_id": "266",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Walker Memorial Funeral Home",
            "contact_person": "Walker Memorial",
            "email": "Walkerfh@embarqmail.com",
            "phone": "910-814-1200",
            "fax": "",
            "address_1": "163 Irene Roberts Rd",
            "address_2": "",
            "city": "Lillington ",
            "state": "NC",
            "zip": "27546",
            "web": "www.walkermemorialfh.com",
            "price_rounding": 0,
            "parent_account": "266",
            "account_status": 777
          }, {
            "old_id": "267",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Walker Memorial Funeral Home",
            "contact_person": "Walker Memorial",
            "email": "Walkerfh@embarqmail.com",
            "phone": "910-814-1200",
            "fax": "",
            "address_1": "163 Irene Roberts Rd",
            "address_2": "",
            "city": "Lillington ",
            "state": "NC",
            "zip": "27546",
            "web": "www.walkermemorialfh.com",
            "price_rounding": 0,
            "parent_account": "266",
            "account_status": 777
          }, {
            "old_id": "268",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Harris Funeral Home",
            "contact_person": "Brad Evans",
            "email": "Brad@harrisfuneral.com",
            "phone": "864-366-4027",
            "fax": "",
            "address_1": "302 North Main St.",
            "address_2": "",
            "city": "Abbeville",
            "state": "SC",
            "zip": "29620",
            "web": "www.harrisfuneral.com",
            "price_rounding": 0,
            "parent_account": "268",
            "account_status": 777
          }, {
            "old_id": "269",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Harris Funeral Home",
            "contact_person": "Brad Evans",
            "email": "Brad@harrisfuneral.com",
            "phone": "864-366-4027",
            "fax": "",
            "address_1": "302 North Main St.",
            "address_2": "",
            "city": "Abbeville",
            "state": "SC",
            "zip": "29620",
            "web": "www.harrisfuneral.com",
            "price_rounding": 0,
            "parent_account": "268",
            "account_status": 777
          }, {
            "old_id": "270",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Floyd Funeral Home Admin",
            "contact_person": "Kel Floyd",
            "email": "floydfuneral@ftc.i-net",
            "phone": "",
            "fax": "",
            "address_1": "136 East Main Street",
            "address_2": "",
            "city": "Olanta",
            "state": "SC",
            "zip": "29114",
            "web": "",
            "price_rounding": 0,
            "parent_account": "270",
            "account_status": 777
          }, {
            "old_id": "271",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Floyd Funeral Home",
            "contact_person": "Kel Floyd",
            "email": "floydfuneral@ftc.i-net",
            "phone": "",
            "fax": "",
            "address_1": "136 East Main Street",
            "address_2": "",
            "city": "Olanta",
            "state": "SC",
            "zip": "29114",
            "web": "",
            "price_rounding": 0,
            "parent_account": "270",
            "account_status": 777
          }, {
            "old_id": "272",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Baker Sample",
            "contact_person": "RW Baker sample",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "272",
            "account_status": 777
          }, {
            "old_id": "273",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "RW Baker Sample",
            "contact_person": "RW Baker Sample",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "273",
            "account_status": 777
          }, {
            "old_id": "274",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Rivers Community Funeral Home",
            "contact_person": "Kirk Rivers",
            "email": "kirk@riverscommunityfuneralhome.com",
            "phone": "252-338-5065",
            "fax": "252-338-5067",
            "address_1": "504 S Road Street",
            "address_2": "",
            "city": "Elizabeth City",
            "state": "NC ",
            "zip": "27909",
            "web": "www.riverscommunityfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "274",
            "account_status": 777
          }, {
            "old_id": "275",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Rivers Community Funeral Home",
            "contact_person": "Kirk Rivers",
            "email": "",
            "phone": "252-338-5065",
            "fax": "",
            "address_1": "504 S Road Street",
            "address_2": "",
            "city": "Elizabeth City",
            "state": "NC ",
            "zip": "27909",
            "web": "www.riverscommunityfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "274",
            "account_status": 777
          }, {
            "old_id": "276",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Folk Funeral Home",
            "contact_person": "Brett Williams",
            "email": "",
            "phone": "803-266-3434",
            "fax": "",
            "address_1": "170 Elko St.",
            "address_2": "",
            "city": "Williston",
            "state": "SC ",
            "zip": "29853",
            "web": "",
            "price_rounding": 0,
            "parent_account": "276",
            "account_status": 777
          }, {
            "old_id": "277",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Wright Funeral Home",
            "contact_person": "G.W. Wright, Jr.",
            "email": "wfhoxf@embarqmail.com",
            "phone": "919-693-8870",
            "fax": "",
            "address_1": "3724 Salem Road",
            "address_2": "",
            "city": "Oxford",
            "state": "NC",
            "zip": "27565",
            "web": "",
            "price_rounding": 0,
            "parent_account": "277",
            "account_status": 777
          }, {
            "old_id": "278",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Wright Funeral Home",
            "contact_person": "G.W. Wright, Jr.",
            "email": "wfhoxf@embarqmail.com",
            "phone": "919-693-8870",
            "fax": "",
            "address_1": "3724 Salem Road",
            "address_2": "",
            "city": "Oxford",
            "state": "NC",
            "zip": "27565",
            "web": "",
            "price_rounding": 0,
            "parent_account": "277",
            "account_status": 777
          }, {
            "old_id": "279",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joseph McMillian Funeral Home & Crematories",
            "contact_person": "Joe McMillian",
            "email": "joemc747@yahoo.com",
            "phone": "434-292-4343",
            "fax": "",
            "address_1": "1826 Cox Road, Bus. Hwy 460",
            "address_2": "",
            "city": "Blackstone",
            "state": "VA",
            "zip": "23824",
            "web": "www.mcmillianfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "279",
            "account_status": 777
          }, {
            "old_id": "280",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joseph McMillian Funeral Home & Crematories",
            "contact_person": "Joe McMillian",
            "email": "joemc747@yahoo.com",
            "phone": "434-292-4343",
            "fax": "",
            "address_1": "1826 Cox Road, Bus. Hwy 460",
            "address_2": "",
            "city": "Blackstone",
            "state": "VA",
            "zip": "23824",
            "web": "www.mcmillianfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "279",
            "account_status": 777
          }, {
            "old_id": "281",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "",
            "contact_person": "red",
            "email": "",
            "phone": "",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "281",
            "account_status": 777
          }, {
            "old_id": "282",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Sample",
            "contact_person": "Joe sample",
            "email": "",
            "phone": "9195551111",
            "fax": "",
            "address_1": "123 mainstream st",
            "address_2": "",
            "city": "Casket",
            "state": "NC",
            "zip": "27777",
            "web": "www.website.com",
            "price_rounding": 0,
            "parent_account": "282",
            "account_status": 777
          }, {
            "old_id": "283",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Horne Funeral Home & Crematory",
            "contact_person": "Brain Horne",
            "email": "",
            "phone": "(540) 382-2612",
            "fax": "",
            "address_1": "1300 N Franklin Street",
            "address_2": "",
            "city": "Christiansburg",
            "state": "Va",
            "zip": "24073",
            "web": "Www.hornefuneralservice.com",
            "price_rounding": 0,
            "parent_account": "283",
            "account_status": 777
          }, {
            "old_id": "284",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Renaissance Funeral Home",
            "contact_person": "Joseph Smolenski",
            "email": "joe3@rfhr.com",
            "phone": "919-866-1866",
            "fax": "",
            "address_1": "7615 Six Forks Road",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27615",
            "web": "www.raleighfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "284",
            "account_status": 777
          }, {
            "old_id": "285",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Renaissance Funeral Home",
            "contact_person": "Joseph Smolenski",
            "email": "joe3@rfhr.com",
            "phone": "919-866-1866",
            "fax": "",
            "address_1": "7615 Six Forks Road",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27615",
            "web": "www.raleighfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "284",
            "account_status": 777
          }, {
            "old_id": "286",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Gailes Funeral Home",
            "contact_person": "Clyde Foust",
            "email": "gaileswithcare@aol.com",
            "phone": "336-625-3595",
            "fax": "",
            "address_1": "1353 E. Salisbury St.",
            "address_2": "",
            "city": "Asheboro",
            "state": "NC",
            "zip": "27203",
            "web": "",
            "price_rounding": 0,
            "parent_account": "286",
            "account_status": 777
          }, {
            "old_id": "287",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Gailes Funeral Home",
            "contact_person": "Clyde Foust",
            "email": "gaileswithcare@aol.com",
            "phone": "336-625-3595",
            "fax": "",
            "address_1": "1353 E. Salisbury St.",
            "address_2": "",
            "city": "Asheboro",
            "state": "NC",
            "zip": "27203",
            "web": "",
            "price_rounding": 0,
            "parent_account": "286",
            "account_status": 777
          }, {
            "old_id": "288",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Powles Funeral Home",
            "contact_person": "Matt ",
            "email": "powlesfuneralhomeinc@gmail.com",
            "phone": "(704) 279-7241",
            "fax": "",
            "address_1": "913 West Main Street",
            "address_2": "",
            "city": "Rockwell",
            "state": "NC",
            "zip": "28138",
            "web": "www.powlesfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "288",
            "account_status": 777
          }, {
            "old_id": "289",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "J. B. Rhodes Funeral Home and Cremations, Inc.",
            "contact_person": "J. B. Rhodes ",
            "email": "jbrhodesfh1@aol.com",
            "phone": "919 - 735 - 2221",
            "fax": "",
            "address_1": "1701 Wayne Memorial Drive",
            "address_2": "",
            "city": "Goldsboro",
            "state": "N.C",
            "zip": "27534",
            "web": "www.jbrhodesfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "289",
            "account_status": 777
          }, {
            "old_id": "290",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "J. B. Rhodes Funeral Home and Cremations, Inc.",
            "contact_person": "J. B. Rhodes ",
            "email": "jbrhodesfh1@aol.com",
            "phone": "919 - 735 - 2221",
            "fax": "",
            "address_1": "1701 Wayne Memorial Drive",
            "address_2": "",
            "city": "Goldsboro",
            "state": "N.C",
            "zip": "27534",
            "web": "www.jbrhodesfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "289",
            "account_status": 777
          }, {
            "old_id": "291",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hornes Funeral Home",
            "contact_person": "Willie & Denise Hornes",
            "email": "hornesfuneralhome@centurylink.net",
            "phone": "252 - 753 -  2320",
            "fax": "",
            "address_1": "3865 S. Main Street",
            "address_2": "",
            "city": "Farmville",
            "state": "NC",
            "zip": "27828",
            "web": "www.hornesfh.com",
            "price_rounding": 0,
            "parent_account": "291",
            "account_status": 777
          }, {
            "old_id": "292",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hornes Funeral Home",
            "contact_person": "Willie & Denise Hornes",
            "email": "hornesfuneralhome@centurylink.net",
            "phone": "252 - 753 -  2320",
            "fax": "",
            "address_1": "3865 S. Main Street",
            "address_2": "",
            "city": "Farmville",
            "state": "NC",
            "zip": "27828",
            "web": "www.hornesfh.com",
            "price_rounding": 0,
            "parent_account": "291",
            "account_status": 777
          }, {
            "old_id": "293",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hudson Funeral & Cremation Services",
            "contact_person": "Hudson",
            "email": "",
            "phone": "919-596-8269",
            "fax": "",
            "address_1": "211 South Miami BLVD",
            "address_2": "",
            "city": "Durham",
            "state": "NC",
            "zip": "27703",
            "web": "www.HudsonFuneralHome.com",
            "price_rounding": 0,
            "parent_account": "293",
            "account_status": 777
          }, {
            "old_id": "294",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hudson Funeral & Cremation Services",
            "contact_person": "Hudson",
            "email": "",
            "phone": "919-596-8269",
            "fax": "",
            "address_1": "211 South Miami BLVD",
            "address_2": "",
            "city": "Durham",
            "state": "NC",
            "zip": "27703",
            "web": "www.HudsonFuneralHome.com",
            "price_rounding": 0,
            "parent_account": "293",
            "account_status": 777
          }, {
            "old_id": "295",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "McDougald Funeral Home",
            "contact_person": "Beacham McDougald",
            "email": "mcdougald@aol.com",
            "phone": "919-276-2200",
            "fax": "",
            "address_1": "305 E. Church St.",
            "address_2": "",
            "city": "Laurinburg",
            "state": "NC",
            "zip": "28352",
            "web": "www.McDouald.com",
            "price_rounding": 0,
            "parent_account": "295",
            "account_status": 777
          }, {
            "old_id": "296",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "McDougald Funeral Home",
            "contact_person": "Beacham McDougald",
            "email": "mcdougald@aol.com",
            "phone": "919-276-2200",
            "fax": "",
            "address_1": "305 E. Church St.",
            "address_2": "",
            "city": "Laurinburg",
            "state": "NC",
            "zip": "28352",
            "web": "www.McDouald.com",
            "price_rounding": 0,
            "parent_account": "295",
            "account_status": 777
          }, {
            "old_id": "297",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Oris P. Jones Funeral Establishment",
            "contact_person": "Mary Johnson - Fields",
            "email": "msmejohnson@msn.com",
            "phone": "(434) 447-7158",
            "fax": "",
            "address_1": "301 West Atlantic Street",
            "address_2": "",
            "city": "South Hill ",
            "state": "VA",
            "zip": "23970",
            "web": "www.johnsonservices.com",
            "price_rounding": 0,
            "parent_account": "297",
            "account_status": 777
          }, {
            "old_id": "298",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Oris P. Jones Funeral Establishment",
            "contact_person": "Mary Johnson - Fields",
            "email": "msmejohnson@msn.com",
            "phone": "(434) 447-7158",
            "fax": "",
            "address_1": "301 West Atlantic Street",
            "address_2": "",
            "city": "South Hill ",
            "state": "VA",
            "zip": "23970",
            "web": "www.johnsonservices.com",
            "price_rounding": 0,
            "parent_account": "297",
            "account_status": 777
          }, {
            "old_id": "299",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Cooke Bros. Funeral Chapel and Crematory",
            "contact_person": "Marco Stacy",
            "email": "marco@cookebros.com",
            "phone": "(757) 380-0251",
            "fax": "",
            "address_1": "1601 27th Street",
            "address_2": "",
            "city": "Newport News",
            "state": "VA",
            "zip": "23607",
            "web": "www.cookebros.com",
            "price_rounding": 0,
            "parent_account": "299",
            "account_status": 777
          }, {
            "old_id": "300",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Cooke Bros. Funeral Chapel and Crematory",
            "contact_person": "Marco Stacy",
            "email": "marco@cookebros.com",
            "phone": "(757) 380-0251",
            "fax": "",
            "address_1": "1601 27th Street",
            "address_2": "",
            "city": "Newport News",
            "state": "VA",
            "zip": "23607",
            "web": "www.cookebros.com",
            "price_rounding": 0,
            "parent_account": "299",
            "account_status": 777
          }, {
            "old_id": "303",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joyce-Brady Chapel",
            "contact_person": "JJ Moore",
            "email": "jjtheundertaker@gmail.com",
            "phone": "336-581-3505",
            "fax": "",
            "address_1": "3833 Chatham Street",
            "address_2": "",
            "city": "Bennett",
            "state": "NC",
            "zip": "27208",
            "web": "",
            "price_rounding": 0,
            "parent_account": "303",
            "account_status": 777
          }, {
            "old_id": "304",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joyce-Brady Chapel",
            "contact_person": "JJ Moore",
            "email": "jjtheundertaker@gmail.com",
            "phone": "336-581-3505",
            "fax": "",
            "address_1": "3833 Chatham Street",
            "address_2": "",
            "city": "Bennett",
            "state": "NC",
            "zip": "27208",
            "web": "",
            "price_rounding": 0,
            "parent_account": "303",
            "account_status": 777
          }, {
            "old_id": "305",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bower Funeral Chapels",
            "contact_person": "Dana Bowers Rygas",
            "email": "Drygas@comcast.net",
            "phone": "540-980-6160",
            "fax": "",
            "address_1": "1631 Bob White Blvd",
            "address_2": "",
            "city": "Pulaski",
            "state": "Vir",
            "zip": "24301",
            "web": "Www.bowerfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "305",
            "account_status": 777
          }, {
            "old_id": "306",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Haywood Funeral Home",
            "contact_person": "Karl Anderson",
            "email": "haywoodfh@aol.com",
            "phone": "919-832-2835",
            "fax": "",
            "address_1": "2415 Wilmington Street",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27603",
            "web": "",
            "price_rounding": 0,
            "parent_account": "306",
            "account_status": 777
          }, {
            "old_id": "307",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Haywood Funeral Home",
            "contact_person": "Karl Anderson",
            "email": "haywoodfh@aol.com",
            "phone": "919-832-2835",
            "fax": "",
            "address_1": "2415 Wilmington Street",
            "address_2": "",
            "city": "Raleigh",
            "state": "NC",
            "zip": "27603",
            "web": "",
            "price_rounding": 0,
            "parent_account": "306",
            "account_status": 777
          }, {
            "old_id": "308",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Smith FH Broadway",
            "contact_person": "April Collins",
            "email": "smithfuneralhomebroadway@windstream.net",
            "phone": "919-258-6711",
            "fax": "",
            "address_1": "201 First Street",
            "address_2": "",
            "city": "Broadway",
            "state": "NC",
            "zip": "27505",
            "web": "",
            "price_rounding": 0,
            "parent_account": "308",
            "account_status": 777
          }, {
            "old_id": "309",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Smith FH Broadway",
            "contact_person": "April Collins",
            "email": "smithfuneralhomebroadway@windstream.net",
            "phone": "919-258-6711",
            "fax": "",
            "address_1": "201 First Street",
            "address_2": "",
            "city": "Broadway",
            "state": "NC",
            "zip": "27505",
            "web": "",
            "price_rounding": 0,
            "parent_account": "308",
            "account_status": 777
          }, {
            "old_id": "310",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hale Funeral Home",
            "contact_person": "Chris Wilson",
            "email": "halefuneralhome@hotmail.com",
            "phone": "757 - 622 - 2321",
            "fax": "",
            "address_1": "2100 Ballentine Blvd",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.halefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "310",
            "account_status": 777
          }, {
            "old_id": "311",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hale Funeral Home",
            "contact_person": "Chris Wilson",
            "email": "halefuneralhome@hotmail.com",
            "phone": "757 - 622 - 2321",
            "fax": "",
            "address_1": "2100 Ballentine Blvd",
            "address_2": "",
            "city": "Norfolk",
            "state": "VA",
            "zip": "23504",
            "web": "www.halefuneralhome.com",
            "price_rounding": 0,
            "parent_account": "310",
            "account_status": 777
          }, {
            "old_id": "312",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bryant Grant Funeral Home",
            "contact_person": "Nick",
            "email": "nickcasketman@yahoo.com",
            "phone": "(828) 524-2411",
            "fax": "",
            "address_1": "105 W. Main Street",
            "address_2": "",
            "city": "Franklin",
            "state": "NC",
            "zip": "28734",
            "web": "bryantgrantfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "312",
            "account_status": 777
          }, {
            "old_id": "313",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Bryant Grant Funeral Home",
            "contact_person": "Nick",
            "email": "nickcasketman@yahoo.com",
            "phone": "(828) 524-2411",
            "fax": "",
            "address_1": "105 W. Main Street",
            "address_2": "",
            "city": "Franklin",
            "state": "NC",
            "zip": "28734",
            "web": "bryantgrantfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "312",
            "account_status": 777
          }, {
            "old_id": "314",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Pierce Funeral Home",
            "contact_person": "Ernie Myers",
            "email": "ernie@piercefh.com",
            "phone": "703-257-6028",
            "fax": "",
            "address_1": "9609 Center Street",
            "address_2": "",
            "city": "Manassas",
            "state": "VA",
            "zip": "20110",
            "web": "",
            "price_rounding": 0,
            "parent_account": "314",
            "account_status": 777
          }, {
            "old_id": "315",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Pierce Funeral Home",
            "contact_person": "Ernie Myers",
            "email": "ernie@piercefh.com",
            "phone": "703-257-6028",
            "fax": "",
            "address_1": "9609 Center Street",
            "address_2": "",
            "city": "Manassas",
            "state": "VA",
            "zip": "20110",
            "web": "",
            "price_rounding": 0,
            "parent_account": "314",
            "account_status": 777
          }, {
            "old_id": "316",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Harrelson Funeral Service",
            "contact_person": "Joe Harrelson",
            "email": "harrelsonfs@yahoo.com",
            "phone": "336-694-4114",
            "fax": "",
            "address_1": "143 3rd Avenue",
            "address_2": "",
            "city": "Yanceyville",
            "state": "NC",
            "zip": "27379",
            "web": "",
            "price_rounding": 0,
            "parent_account": "316",
            "account_status": 777
          }, {
            "old_id": "317",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Harrelson Funeral Service",
            "contact_person": "Joe Harrelson",
            "email": "harrelsonfs@yahoo.com",
            "phone": "336-694-4114",
            "fax": "",
            "address_1": "143 3rd Avenue",
            "address_2": "",
            "city": "Yanceyville",
            "state": "NC",
            "zip": "27379",
            "web": "",
            "price_rounding": 0,
            "parent_account": "316",
            "account_status": 777
          }, {
            "old_id": "318",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Sample",
            "contact_person": "Joe sample",
            "email": "",
            "phone": "9195551111",
            "fax": "",
            "address_1": "123 mainstream st",
            "address_2": "",
            "city": "Casket",
            "state": "NC",
            "zip": "27777",
            "web": "www.website.com",
            "price_rounding": "4",
            "parent_account": "282",
            "account_status": 777
          }, {
            "old_id": "319",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Tri-Cities Funeral Home",
            "contact_person": "James Guy",
            "email": "james@tricitiesfuneral.com",
            "phone": "804-621-0970",
            "fax": "",
            "address_1": "415 Halifax Street",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "23803",
            "web": "www.tricitiesfuneral.com",
            "price_rounding": 0,
            "parent_account": "319",
            "account_status": 777
          }, {
            "old_id": "320",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Tri-Cities Funeral Home",
            "contact_person": "James Guy",
            "email": "james@tricitiesfuneral.com",
            "phone": "804-621-0970",
            "fax": "",
            "address_1": "415 Halifax Street",
            "address_2": "",
            "city": "Petersburg",
            "state": "VA",
            "zip": "23803",
            "web": "www.tricitiesfuneral.com",
            "price_rounding": 0,
            "parent_account": "319",
            "account_status": 777
          }, {
            "old_id": "321",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bridges-Cameron Funeral Home",
            "contact_person": "Larry Cameron",
            "email": "bridgescameron@windstream.net",
            "phone": "919-774-1111",
            "fax": "",
            "address_1": "600 W. Main Street",
            "address_2": "",
            "city": "Sanford",
            "state": "NC",
            "zip": "27332",
            "web": "",
            "price_rounding": 0,
            "parent_account": "321",
            "account_status": 777
          }, {
            "old_id": "322",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Bridges-Cameron Funeral Home",
            "contact_person": "Larry Cameron",
            "email": "bridgescameron@windstream.net",
            "phone": "919-774-1111",
            "fax": "",
            "address_1": "600 W. Main Street",
            "address_2": "",
            "city": "Sanford",
            "state": "NC",
            "zip": "27332",
            "web": "",
            "price_rounding": 0,
            "parent_account": "321",
            "account_status": 777
          }, {
            "old_id": "323",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Triad Cremation Society",
            "contact_person": "Triad Cremation Society",
            "email": "",
            "phone": "336-275-1005",
            "fax": "",
            "address_1": "2110 Veasley St",
            "address_2": "",
            "city": "Greensboro",
            "state": "NC ",
            "zip": "27407",
            "web": "",
            "price_rounding": 0,
            "parent_account": "323",
            "account_status": 777
          }, {
            "old_id": "324",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Triad Cremation Society",
            "contact_person": "Triad Cremation Society",
            "email": "",
            "phone": "336-275-1005",
            "fax": "",
            "address_1": "2110 Veasley St",
            "address_2": "",
            "city": "Greensboro",
            "state": "NC ",
            "zip": "27407",
            "web": "",
            "price_rounding": 0,
            "parent_account": "323",
            "account_status": 777
          }, {
            "old_id": "325",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Hanes Funeral Services",
            "contact_person": "Bishop Hanes",
            "email": "fohrev@yahoo.com",
            "phone": "919-598-9968",
            "fax": "",
            "address_1": "460 South Driver Street",
            "address_2": "",
            "city": "Durham",
            "state": "NC",
            "zip": "27703",
            "web": "",
            "price_rounding": 0,
            "parent_account": "325",
            "account_status": 777
          }, {
            "old_id": "326",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Hanes Funeral Services",
            "contact_person": "Bishop Hanes",
            "email": "fohrev@yahoo.com",
            "phone": "919-598-9968",
            "fax": "",
            "address_1": "460 South Driver Street",
            "address_2": "",
            "city": "Durham",
            "state": "NC",
            "zip": "27703",
            "web": "",
            "price_rounding": 0,
            "parent_account": "325",
            "account_status": 777
          }, {
            "old_id": "327",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Gibbs Funeral Home",
            "contact_person": "Charles Gibbs",
            "email": "cgibbs@gibbsfuneral.com",
            "phone": "910-581-2904",
            "fax": "252-631-5112",
            "address_1": "412 Bern Street",
            "address_2": "",
            "city": "New Bern",
            "state": "NC",
            "zip": "28560",
            "web": "",
            "price_rounding": 0,
            "parent_account": "327",
            "account_status": 777
          }, {
            "old_id": "328",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Gibbs Funeral Home",
            "contact_person": "Charles Gibbs",
            "email": "cgibbs4695@aol.com",
            "phone": "910 - 581 - 2904",
            "fax": "",
            "address_1": "412 Bern Street",
            "address_2": "",
            "city": "New Bern",
            "state": "NC",
            "zip": "28560",
            "web": "",
            "price_rounding": 0,
            "parent_account": "327",
            "account_status": 777
          }, {
            "old_id": "329",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Freeman Funeral Service",
            "contact_person": "Glenda Freeman",
            "email": "freemanfuneralservices1@yahoo.com",
            "phone": "301-877-3735",
            "fax": "",
            "address_1": "7201 Old Alexanderia Ferry Road",
            "address_2": "",
            "city": "Clinton",
            "state": "MD",
            "zip": "20735",
            "web": "",
            "price_rounding": 0,
            "parent_account": "329",
            "account_status": 777
          }, {
            "old_id": "330",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Freeman Funeral Service",
            "contact_person": "Glenda Freeman",
            "email": "freemanfuneralservices1@yahoo.com",
            "phone": "301-877-3735",
            "fax": "",
            "address_1": "7201 Old Alexanderia Ferry Road",
            "address_2": "",
            "city": "Clinton",
            "state": "MD",
            "zip": "20735",
            "web": "",
            "price_rounding": 0,
            "parent_account": "329",
            "account_status": 777
          }, {
            "old_id": "331",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Palmer Memorial Chapel",
            "contact_person": "Lorin Perri Palmer",
            "email": "",
            "phone": "803-786-6300",
            "fax": "",
            "address_1": "1200 Fontaine Place",
            "address_2": "",
            "city": "Columbia",
            "state": "SC",
            "zip": "29202",
            "web": "www.palmerchapel.com",
            "price_rounding": 0,
            "parent_account": "331",
            "account_status": 777
          }, {
            "old_id": "332",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Palmer Memorial Chapel ",
            "contact_person": "Lorin Palmer",
            "email": "",
            "phone": "8037866300",
            "fax": "",
            "address_1": "1200 Fontaine Place",
            "address_2": "",
            "city": "Columbia",
            "state": "SC",
            "zip": "29202",
            "web": "",
            "price_rounding": 0,
            "parent_account": "332",
            "account_status": 777
          }, {
            "old_id": "333",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Zzz",
            "contact_person": "Zzz",
            "email": "",
            "phone": "Zzz",
            "fax": "",
            "address_1": "",
            "address_2": "",
            "city": "",
            "state": "",
            "zip": "0",
            "web": "Zzz",
            "price_rounding": 0,
            "parent_account": "333",
            "account_status": 777
          }, {
            "old_id": "334",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Colonial Funeral Home",
            "contact_person": "Carey",
            "email": "director@cfhsmithfield.com",
            "phone": "757-357-2979",
            "fax": "",
            "address_1": "14214 Carrollton Blvd.",
            "address_2": "",
            "city": "Carrollton",
            "state": "VA",
            "zip": "23314",
            "web": "",
            "price_rounding": 0,
            "parent_account": "334",
            "account_status": 777
          }, {
            "old_id": "335",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Colonial Funeral Home",
            "contact_person": "Carey",
            "email": "director@cfhsmithfield.com",
            "phone": "757-357-2979",
            "fax": "",
            "address_1": "14214 Carrollton Blvd.",
            "address_2": "",
            "city": "Carrollton",
            "state": "VA",
            "zip": "23314",
            "web": "",
            "price_rounding": 0,
            "parent_account": "334",
            "account_status": 777
          }, {
            "old_id": "336",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Colonial Funeral Home",
            "contact_person": "Carey Whitley",
            "email": "director@cfhsmithfield.com",
            "phone": "757-357-2979",
            "fax": "",
            "address_1": "14214 Carrollton Blvd.",
            "address_2": "",
            "city": "Carrollton",
            "state": "VA",
            "zip": "23314",
            "web": "colonialfuneralhomesmithfield.com",
            "price_rounding": 0,
            "parent_account": "336",
            "account_status": 777
          }, {
            "old_id": "337",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Colonial Funeral Home",
            "contact_person": "Carey Whitley",
            "email": "director@cfhsmithfield.com",
            "phone": "757-357-2979",
            "fax": "",
            "address_1": "14214 Carroliton Blvd.",
            "address_2": "",
            "city": "Carrollton",
            "state": "VA",
            "zip": "23314",
            "web": "colonialfuneralhomesmithfield.com",
            "price_rounding": 0,
            "parent_account": "336",
            "account_status": 777
          }, {
            "old_id": "338",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Donaldson Funeral Home & Cremation",
            "contact_person": "Lyle Donaldson",
            "email": "lyle@donaldsonfunerals.com",
            "phone": "919-542-3057",
            "fax": "",
            "address_1": "396 West Street",
            "address_2": "",
            "city": "Pittsboro",
            "state": "NC",
            "zip": "27312",
            "web": "www.donaldsonfunerals.com",
            "price_rounding": 0,
            "parent_account": "338",
            "account_status": 777
          }, {
            "old_id": "339",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Donaldson Funeral Home & Cremation",
            "contact_person": "Lyle Donaldson",
            "email": "lyle@donaldsonfunerals.com",
            "phone": "919-542-3057",
            "fax": "",
            "address_1": "396 West Street",
            "address_2": "",
            "city": "Pittsboro",
            "state": "NC",
            "zip": "27312",
            "web": "www.donaldsonfunerals.com",
            "price_rounding": 0,
            "parent_account": "338",
            "account_status": 777
          }, {
            "old_id": "340",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Covenant Funeral Home",
            "contact_person": "George Gilmore",
            "email": "gildeanbiz@yahoo.com",
            "phone": "910-354-8171 (Temp)",
            "fax": "",
            "address_1": "3735 Legion Road",
            "address_2": "",
            "city": "Hope Mills",
            "state": "NC",
            "zip": "28348",
            "web": "",
            "price_rounding": 0,
            "parent_account": "340",
            "account_status": 777
          }, {
            "old_id": "341",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Covenant Funeral Home",
            "contact_person": "George Gilmore",
            "email": "gildeanbiz@yahoo.com",
            "phone": "910-354-8171 (Tempor",
            "fax": "",
            "address_1": "3735 Legion Road",
            "address_2": "",
            "city": "Hope Mills",
            "state": "NC",
            "zip": "28348",
            "web": "",
            "price_rounding": 0,
            "parent_account": "340",
            "account_status": 777
          }, {
            "old_id": "342",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Joseph McMillian Funeral Home",
            "contact_person": "Joseph (Joe) McMcMillian",
            "email": "Mcmillianfuneralhomes@embarqmail.com",
            "phone": "434-292-4343",
            "fax": "",
            "address_1": "1826 Cox Road, Business Hwy. 460",
            "address_2": "",
            "city": "Blackstone",
            "state": "VA",
            "zip": "23824",
            "web": "www.mcmillianfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "342",
            "account_status": 777
          }, {
            "old_id": "343",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Joseph McMillian Funeral Home",
            "contact_person": "Joseph (Joe) McMcMillian",
            "email": "Joe@mcmillianfuneralhome.com",
            "phone": "434-292-4343",
            "fax": "",
            "address_1": "1826 Cox Road, Business Hwy. 460",
            "address_2": "",
            "city": "Blackstone",
            "state": "VA",
            "zip": "23824",
            "web": "www.mcmillianfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "342",
            "account_status": 777
          }, {
            "old_id": "344",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Steele-Bullock Funeral Home",
            "contact_person": "Paulette Strawbridge",
            "email": "paulettestrawbridge@gmail.com",
            "phone": "757-398-9100",
            "fax": "757-397-4481",
            "address_1": "3950 Turnpike Road ",
            "address_2": "",
            "city": "Portsmouth",
            "state": "VA",
            "zip": "23701",
            "web": "www.steelebullockfuneralhomeinc.com",
            "price_rounding": 0,
            "parent_account": "344",
            "account_status": 777
          }, {
            "old_id": "345",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Steele - Bullock Funeral Home",
            "contact_person": "Paulette Strawbridge",
            "email": "paulettestrawbridge@gmail.com",
            "phone": "757-398-9100",
            "fax": "",
            "address_1": "3950 Turnpike Road ",
            "address_2": "",
            "city": "Portsmouth",
            "state": "VA",
            "zip": "23701",
            "web": "www.steelebullockfuneralhomeinc.com",
            "price_rounding": 0,
            "parent_account": "344",
            "account_status": 777
          }, {
            "old_id": "346",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Fitchett-Mann Funeral Services, Inc.",
            "contact_person": "Danielle Fitchett-Mann",
            "email": "danit23320@aol.com",
            "phone": "757-494-1404",
            "fax": "",
            "address_1": "1146 Rodgers Street",
            "address_2": "",
            "city": "Chesapeake",
            "state": "VA",
            "zip": "23324",
            "web": "www.fitchettmannfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "346",
            "account_status": 777
          }, {
            "old_id": "347",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Fitchett-Mann Funeral Services, Inc.",
            "contact_person": "Danielle Fitchett-Mann",
            "email": "danit23320@aol.com",
            "phone": "757-494-1404",
            "fax": "",
            "address_1": "1146 Rodgers Street",
            "address_2": "",
            "city": "Chesapeake",
            "state": "VA",
            "zip": "23324",
            "web": "www.fitchettmannfuneralservices.com",
            "price_rounding": 0,
            "parent_account": "346",
            "account_status": 777
          }, {
            "old_id": "348",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Corprew Funeral Home",
            "contact_person": "Tracee N. Corprew-Hawk",
            "email": "corprewfuneralhome@gmail.com",
            "phone": "757-399-4661",
            "fax": "",
            "address_1": "1822 Portsmouth Blvd.",
            "address_2": "",
            "city": "Portsmouth ",
            "state": "VA",
            "zip": "23704",
            "web": "www.corprewfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "348",
            "account_status": 777
          }, {
            "old_id": "349",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Corprew Funeral Home",
            "contact_person": "Tracee N. Corprew-Hawk",
            "email": "corprewfuneralhome@gmail.com",
            "phone": "757-399-4661",
            "fax": "",
            "address_1": "1822 Portsmouth Blvd.",
            "address_2": "",
            "city": "Portsmouth ",
            "state": "VA",
            "zip": "23704",
            "web": "www.corprewfuneralhome.com",
            "price_rounding": 0,
            "parent_account": "348",
            "account_status": 777
          }, {
            "old_id": "350",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Patterson Memorial Funeral Home",
            "contact_person": "Sharon Patterson",
            "email": "patterfuneralhome@gmail.com",
            "phone": "910-822-8240",
            "fax": "",
            "address_1": "4621 Murphy Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28312",
            "web": "",
            "price_rounding": 0,
            "parent_account": "350",
            "account_status": 777
          }, {
            "old_id": "351",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Patterson Memorial Funeral Home",
            "contact_person": "Sharon Patterson",
            "email": "patterfuneralhome@gmail.com",
            "phone": "910-822-8240",
            "fax": "",
            "address_1": "4621 Murphy Road",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28312",
            "web": "",
            "price_rounding": 0,
            "parent_account": "350",
            "account_status": 777
          }, {
            "old_id": "352",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bennett & Barden Funeral Home",
            "contact_person": "Matthew (Matt) Bennett",
            "email": "bennettbarden@msn.com",
            "phone": "804-598-3270",
            "fax": "",
            "address_1": "3215 Anderson Highway",
            "address_2": "",
            "city": "Powhatan",
            "state": "VA",
            "zip": "23139",
            "web": "bennettbardenfh.com",
            "price_rounding": 0,
            "parent_account": "352",
            "account_status": 777
          }, {
            "old_id": "353",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Bennett & Barden Funeral Home",
            "contact_person": "Matthew (Matt) Bennett",
            "email": "bennettbarden@msn.com",
            "phone": "804-598-3270",
            "fax": "",
            "address_1": "3215 Anderson Highway",
            "address_2": "",
            "city": "Powhatan",
            "state": "VA",
            "zip": "23139",
            "web": "www.bennettbardenfh.com",
            "price_rounding": 0,
            "parent_account": "353",
            "account_status": 777
          }, {
            "old_id": "354",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Bennett & Barden Funeral Home",
            "contact_person": "Matthew (Matt) Bennett",
            "email": "bennettbarden@msn.com",
            "phone": "804-598-3270",
            "fax": "",
            "address_1": "3215 Anderson Highway",
            "address_2": "",
            "city": "Powhatan",
            "state": "VA",
            "zip": "23139",
            "web": "www.bennettbardenfh.com",
            "price_rounding": 0,
            "parent_account": "353",
            "account_status": 777
          }, {
            "old_id": "357",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Avinger Funeral Home",
            "contact_person": "Ward Avinger",
            "email": "avingerfh@embarqmail.com",
            "phone": "803-496-3434",
            "fax": "",
            "address_1": "2274 Eutaw Rd",
            "address_2": "PO Box 203",
            "city": "Holly Hill",
            "state": "SC",
            "zip": "29059",
            "web": "avingerfh.com",
            "price_rounding": 0,
            "parent_account": "357",
            "account_status": 777
          }, {
            "old_id": "358",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Avinger Funeral Home",
            "contact_person": "Ward Avinger",
            "email": "avingerfh@embarqmail.com",
            "phone": "803-496-3434",
            "fax": "",
            "address_1": "2274 Eutaw Rd",
            "address_2": "PO Box 203",
            "city": "Holly Hill",
            "state": "SC",
            "zip": "29059",
            "web": "avingerfh.com",
            "price_rounding": 0,
            "parent_account": "357",
            "account_status": 777
          }, {
            "old_id": "359",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Jones & Little Mortuary",
            "contact_person": "Ivy Little",
            "email": "hofflittle7@gmail.com",
            "phone": "910-944-2553",
            "fax": "",
            "address_1": "200 Benjamin Street",
            "address_2": "",
            "city": "Aberdeen",
            "state": "NC",
            "zip": "28315",
            "web": "",
            "price_rounding": 0,
            "parent_account": "359",
            "account_status": 777
          }, {
            "old_id": "360",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Jones & Little Mortuary",
            "contact_person": "Ivy Little",
            "email": "hofflittle7@gmail.com",
            "phone": "910-944-2553",
            "fax": "",
            "address_1": "200 Benjamin Street",
            "address_2": "",
            "city": "Aberdeen",
            "state": "NC",
            "zip": "28315",
            "web": "",
            "price_rounding": 0,
            "parent_account": "359",
            "account_status": 777
          }, {
            "old_id": "361",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Md",
            "contact_person": "Md",
            "email": "",
            "phone": "1",
            "fax": "",
            "address_1": "W",
            "address_2": "",
            "city": "D",
            "state": "",
            "zip": "0",
            "web": "",
            "price_rounding": 0,
            "parent_account": "361",
            "account_status": 777
          }, {
            "old_id": "366",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Sullivans Highland Funeral Service",
            "contact_person": "Bob Sullivan",
            "email": "sullivanshighland@yahoo.com",
            "phone": "910-484-8108",
            "fax": "",
            "address_1": "610 Ramsey Street",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28301",
            "web": "www.sullivanshighland.com",
            "price_rounding": "5",
            "parent_account": "366",
            "account_status": 777
          }, {
            "old_id": "367",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Sullivans Highland Funeral Service",
            "contact_person": "Bob Sullivan",
            "email": "sullivanshighland@yahoo.com",
            "phone": "910-484-8108",
            "fax": "",
            "address_1": "610 Ramsey Street",
            "address_2": "",
            "city": "Fayetteville",
            "state": "NC",
            "zip": "28301",
            "web": "www.sullivanshighland.com",
            "price_rounding": 0,
            "parent_account": "366",
            "account_status": 777
          }, {
            "old_id": "368",
            "username": "",
            "password": "",
            "account_type": "2",
            "fac_name": "Paradise Funeral Home",
            "contact_person": "Omar Pearsall",
            "email": "paztoro2530@gmail.com",
            "phone": "252-792-2001",
            "fax": "",
            "address_1": "811 East Blvd. Suite A2",
            "address_2": "",
            "city": "Williamston",
            "state": "NC",
            "zip": "27892",
            "web": "",
            "price_rounding": 0,
            "parent_account": "368",
            "account_status": 777
          }, {
            "old_id": "369",
            "username": "",
            "password": "",
            "account_type": "3",
            "fac_name": "Paradise Funeral Home",
            "contact_person": "Omar Pearsall",
            "email": "paztoro2530@gmail.com",
            "phone": "252-792-2001",
            "fax": "",
            "address_1": "811 East Blvd. Suite A2",
            "address_2": "",
            "city": "Williamston",
            "state": "NC",
            "zip": "27892",
            "web": "",
            "price_rounding": 0,
            "parent_account": "368",
            "account_status": 777
          }, {
            "old_id": "792",
            "username": "romypister33",
            "password": "sdfsfsdsdfsdfsdfsdf",
            "account_type": "4",
            "fac_name": "",
            "contact_person": "Romy Pister",
            "email": "romypister@dsk.com",
            "phone": "4837484848",
            "fax": "",
            "address_1": "33 Tremount Ave",
            "address_2": "Suite 229",
            "city": "Kansas City",
            "state": "Kansas",
            "zip": "38382",
            "web": "",
            "price_rounding": 0,
            "parent_account": "0",
            "account_status": 777
          }
        ];
        vm.insertOldUsers = function() {
            // loop through the old user objects array.
            vm.oldUsersArr.forEach(function(item) {
                function oldUserInsertFunc() {
                    var postInfo = {
                        obj: true,
                        uri: 'migrateoldusers',
                        oldUserObj: item,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    // console.log(postInfo);
                    // console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                }
                // Set a short Timeout for pagination elements
                function startOldUsersTimer() {
                    var oldUsersInserVarTimer = window.setTimeout(
                        oldUserInsertFunc, 1000
                    );
                }
                startOldUsersTimer();
            });
        };

        // ----------------------------------------------------------------------------
        vm.oldProductsArr = [

        ];
        vm.insertOldProducts = function() {
            var postInfo = {
                obj: true,
                uri: 'migrateoldproducts',
                oldUsersArr: vm.oldProductsArr,
            };
            console.log(postInfo);
            console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };
        // ----------------------------------------------------------------------------
        vm.oldImagesArr = [

        ];
        vm.insertOldImages = function() {
            var postInfo = {
                obj: true,
                uri: 'migrateoldimages',
                oldUsersArr: vm.oldImagesArr,
            };
            console.log(postInfo);
            console.log('postInfo');
            HttpService.postRequest(postInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-success';
                    } else {
                        vm.vars.siteMessage = data.data.message;
                        vm.vars.siteClass = 'alert alert-danger';
                    }
                });
        };
        // ----------------------------------------------------------------------------
        // ----------------------------------------------------------------------------
        // ----------------------------------------------------------------------------
        // ----------------------------------------------------------------------------
        // ----------------------------------------------------------------------------
        // console.log(vm);
})
// Filter / Cut and limit words/strings.
.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                        value = value.substr(0, lastspace);
                }
        }

        return value + (tail || ' …');
    };
});
