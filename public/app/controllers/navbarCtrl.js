angular.module('navbarCtrl', ['authService', 'httpService', 'formFactory'])
    .controller('navbarController', function (
        $rootScope,
        $scope,
        $state,
        $stateParams,
        $http,
        $location,
        $window,
        Auth,
        DropDown,
        HttpService,
        FormFieldValidationFactory) {

        // ----------------------------------------------------------------------------
        var vm = this;

        // reset rootScope search layout values and reset form
        vm.resetSearchLayout = function() {
            // $rootScope.searchLayout = 'float-layout-styles';
            // $rootScope.resetSearchTermsAndResults();
            // window.location.href('/');
            $(location).attr('href', '/');
        };

        // ----------------------------------------------------------------------------

        vm.vars = {
            // get info if a person is logged in
            loggedIn: Auth.isLoggedIn(),
            currentState: $state.$current.path[0].self.name,
            userId: $window.localStorage.getItem('userId'),
            firstname: $window.localStorage.getItem('firstname'),
            lastname: $window.localStorage.getItem('lastname'),
            userType: $window.localStorage.getItem('userType'),
            username: $window.localStorage.getItem('username'),
            user_email: $window.localStorage.getItem('email'),
            access_level: $window.localStorage.getItem('accesslevel'),
            user_type_1_active: false,
            user_type_2_3_active: false,
            user_type_4_active: false,
            user_type_5_active: false,
            check_user_type: function() {
                // console.log(this.access_level);
                // console.log('Type of user logged in above:');
                if (this.access_level === '1') {
                    this.user_type_1_active = true;
                } else if ((this.access_level === '2') || (this.access_level === '3')) {
                    this.user_type_2_3_active = true;
                } else if (this.access_level === '4') {
                    this.user_type_4_active = true;
                } else if (this.access_level === '5') {
                    this.user_type_5_active = true;
                }
            },
            navsUlActive: false,
            processing: false,
            contactFormActive: true,
            formActive: true,
            userTypeObj: {
                admin: 'admin',
                salesRep: 'salesRep',
                funeralHome: 'funeralHome'
            },
            taValidate: FormFieldValidationFactory,
            todaysFormattedDate: moment().format('L'),
            yesterdaysFormattedDate: moment().subtract(1, 'days').format('L'),
            currentYear: moment().format('YYYY')
        };

        // Global current year
        $rootScope.globalCurrentYear = moment().format('YYYY');

        // ----------------------------------------------------------------------------
        // console.log($state.$current.data);
        // console.log('Display $state');
        vm.currentHeaderPageTitle = $state.$current.data.pageH1Title;

        // ----------------------------------------------------------------------------
        vm.addEmailToMailingList = function(email) {
            console.log(email);
            console.log('email raw above:');
            if (email) {
                if ((vm.vars.taValidate.checkValidEmail(email.email_address)) &&
                    (vm.vars.taValidate.validateAnyValue(email.users_mailing_name))) {
                    var postInfo = {
                        obj: true,
                        uri: 'managemailinglist',
                        email: email.email_address,
                        users_mailing_name: email.users_mailing_name,
                        user_wants_subscription: true,
                        todaysFormattedDate: vm.vars.todaysFormattedDate
                    };
                    console.log(postInfo);
                    console.log('postInfo');
                    HttpService.postRequest(postInfo)
                        .then(function(data) {
                            if (data.data.success) {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-success';
                            } else {
                                vm.vars.siteMessage = data.data.message;
                                vm.vars.siteClass = 'alert alert-danger';
                            }
                        });
                } else {
                    vm.vars.siteMessage = 'Please enter a valid email address and fill out the form completely.';
                    vm.vars.siteClass = 'alert alert-danger';
                }
            } else {
                vm.vars.siteMessage = 'Please enter a valid email address and fill out the form completely.';
                vm.vars.siteClass = 'alert alert-danger';
            }
        };

        // ----------------------------------------------------------------------------
        vm.clearMainSearch = function() {
            location.reload();
        };
        vm.routeToHome = function() {
            $location.path('/');
        };

        // ----------------------------------------------------------------------------
        vm.activateDropdownNav = function() {
            // console.log('vm.vars.navsUlActive var:');
            // console.log(vm.vars.navsUlActive);
            if (vm.vars.navsUlActive) {
                vm.vars.navsUlActive = false;
            } else {
                vm.vars.navsUlActive = true;
            }
        };

        // ----------------------------------------------------------------------------
        // User navigations arrays
        vm.publicViewNavs = [
            {
                display: 'About Us',
                link: 'about-tacc',
                spanClass: 'glyphicon glyphicon-bookmark'
            },
            {
                display: 'Our Catalog',
                link: 'catalog',
                spanClass: 'glyphicon glyphicon-book'
            },
            {
                display: 'Mailing List',
                link: 'mailing-list',
                spanClass: 'glyphicon glyphicon-envelope'
            },
            {
                display: 'Contact Us',
                link: 'contact',
                spanClass: 'glyphicon glyphicon-log-in'
            },
            {
                display: 'Login',
                link: 'login',
                spanClass: 'glyphicon glyphicon-log-in'
            }
        ];
        vm.privateViewNavs = [
            {
                display: 'About Us',
                link: 'about-tacc',
                spanClass: 'glyphicon glyphicon-bookmark'
            },
            {
                display: 'Our Catalog',
                link: 'catalog',
                spanClass: 'glyphicon glyphicon-book'
            },
            {
                display: 'Mailing List',
                link: 'mailing-list',
                spanClass: 'glyphicon glyphicon-envelope'
            }
        ];
        vm.user1Array = [
            {
				        link: 'main-menu',
                display: 'Main Menu'
            },
            {
                link: 'add-item',
                display: 'Add Item'
            },
            {
                link: 'add-cat',
                display: 'Add Category'
            },
            {
                link: 'create-sales-rep',
                display: 'Create Sales Rep'
            },
            {
                link: 'create-funeral-home',
                display: 'Create Funeral home'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                link: 'remove-category',
                display: 'Remove A Category'
            },
            {
                link: 'manage-user-accounts',
                display: 'Manage User Accounts'
            },
            {
                link: 'search-products',
                display: 'Search Products'
            },
            {
                link: 'master-price-increase',
                display: 'Rate / Price Increase'
            },
            {
                link: 'login-reports',
                display: 'Login Reports'
            },
            {
                link: 'upload-media',
                display: 'Upload Media Library'
            },
            {
                link: 'change-admin-password',
                display: 'Change Administrator Password'
            },
            {
                link: 'list-user-credentials',
                display: 'View User Credentials'
            }
        ];
        vm.user2And3Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                display: 'Price Management',
                link: 'price-managment'
            },
            {
                link: 'search-products',
                display: 'Search Products'
            },
            {
                display: 'Public Account Setup',
                link: 'account-setup'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user4Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'search-products',
                display: 'Search Products'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];
        vm.user5Array = [
            {
                display: 'Main Menu',
                link: 'main-menu'
            },
            {
                link: 'search-products',
                display: 'Search Products'
            },
            {
                link: 'edit-master-info',
                display: 'Edit Master Information'
            },
            {
                display: 'Change Administrator Password',
                link: 'change-admin-password'
            }
        ];

        // ----------------------------------------------------------------------------
        // function to handle login form
        // vm.doLogin = function() {
        //     vm.processing = true;
        //
        //     // clear the error
        //     vm.error = '';
        //
        //     // call the Auth.login() function
        //     Auth.login(vm.loginData.username, vm.loginData.password)
        //         .then(function(data) {
        //             vm.processing = true;
        //
        //             // if a user successfully logs in, redirect to users page
        //             if (data.data.success) {
        //                 // remove search block if one is set
        //                 $window.localStorage.removeItem('userMustLogin');
        //                 // redirect user
        //                 $location.path('/users');
        //             } else {
        //                 vm.error = data.data.message;
        //             }
        //         });
        // };

        // function to handle logging out
        vm.doLogout = function() {
            Auth.logout();
            // reset all user info
            vm.user = {};
            // console.log('you are logged out now redirect.');
            $location.path('/login');
        };

        // ----------------------------------------------------------------------------
        // console.log(vm);
  })
  // Filter / Cut and limit words/strings.
  .filter('cut', function () {
      return function (value, wordwise, max, tail) {
          if (!value) return '';

          max = parseInt(max, 10);
          if (!max) return value;
          if (value.length <= max) return value;

          value = value.substr(0, max);
          if (wordwise) {
                  var lastspace = value.lastIndexOf(' ');
                  if (lastspace != -1) {
                          value = value.substr(0, lastspace);
                  }
          }

          return value + (tail || ' …');
      };
  });
