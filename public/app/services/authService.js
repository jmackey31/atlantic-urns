angular.module('authService', [])
// ============================================
// auth factory to login and get information
// inject $http for communicating with the API
// inject $q to return promise objects
// inject AuthToken to manage Tokens
// ============================================
    .factory('Auth', function(
        $http,
        $window,
        $q,
        AuthToken,
        AuthUserId,
        AuthUsername,
        AuthUserType,
        AuthEmail,
        AuthAccessLevel) {

        // create auth factory object
        var authFactory = {};

        // log a youthOrg in
        authFactory.login = function(username, password, formattedDate) {
            // return the promise object and it's data
            return $http.post('/api/authenticate', {
                username: username,
                password: password,
                formattedDate: formattedDate
            }).then(function(data) {
                AuthToken.setToken(data.data.token);
                AuthUserId.setUserId(data.data.user_id);
                AuthUsername.setUsername(data.data.username);
                AuthUserType.setUserType(data.data.userType);
                AuthEmail.setEmail(data.data.email);
                AuthAccessLevel.setAccessLevel(data.data.access_level);
                return data;
            });
        };

        // log a youthOrg out by clearing the Token
        authFactory.logout = function() {
            // clear the Token
            AuthToken.setToken();
            AuthUserId.setUserId();
            AuthUsername.setUsername();
            AuthUserType.setUserType();
            AuthEmail.setEmail();
            AuthAccessLevel.setAccessLevel();
        };

        // check if a user is logged in
        // checks if there is a local Token
        authFactory.isLoggedIn = function() {
          if (AuthToken.getToken()) {
              return true;
          } else {
              return false;
          }
        };

        // return auth factory object
        return authFactory;

    })

    // ============================================
    // factory for handling Tokens
    // inject $window to store Token client-side
    // ============================================
    .factory('AuthToken', function($window) {

        var authTokenFactory = {};

        // get the Token out of local storage
        authTokenFactory.getToken = function() {
          return $window.localStorage.getItem('Token');
        };

        // function set the Token or clear the Token
        // if a Token is passed, set the Token
        // if there is no Token, clear it from local storage
        authTokenFactory.setToken = function(Token) {
            if (Token) {
                $window.localStorage.setItem('Token', Token);
            } else {
                $window.localStorage.removeItem('Token');
            }
        };

        return authTokenFactory;

    })

    // ============================================
    // factory for handling userIds
    // inject $window to store userId client-side
    // ============================================
    .factory('AuthUserId', function($window) {

        var authUserIdFactory = {};

        // get the userId out of local storage
        authUserIdFactory.getUserId = function() {
            return $window.localStorage.getItem('userId');
        };

        // function set the userId or clear the userId
        // if a userId is passed, set the userId
        // if there is no userId, clear it from local storage
        authUserIdFactory.setUserId = function(userId) {
            if (userId) {
                $window.localStorage.setItem('userId', userId);
            } else {
                $window.localStorage.removeItem('userId');
            }
        };

        return authUserIdFactory;

    })

    // ============================================
    // factory for handling usernames
    // inject $window to store username client-side
    // ============================================
    .factory('AuthUsername', function($window) {

        var UsernameFactory = {};

        // get the username out of local storage
        UsernameFactory.getUsername = function() {
            return $window.localStorage.getItem('username');
        };

        // function set the username or clear the username
        // if a username is passed, set the username
        // if there is no username, clear it from local storage
        UsernameFactory.setUsername = function(username) {
            if (username) {
                $window.localStorage.setItem('username', username);
            } else {
                $window.localStorage.removeItem('username');
            }
        };

        return UsernameFactory;

    })

    // ============================================
    // factory for handling userTypes
    // inject $window to store userType client-side
    // ============================================
    .factory('AuthUserType', function($window) {

        var UserTypeFactory = {};

        // get the userType out of local storage
        UserTypeFactory.getUserType = function() {
            return $window.localStorage.getItem('userType');
        };

        // function set the userType or clear the userType
        // if a userType is passed, set the userType
        // if there is no userType, clear it from local storage
        UserTypeFactory.setUserType = function(userType) {
            if (userType) {
                $window.localStorage.setItem('userType', userType);
            } else {
                $window.localStorage.removeItem('userType');
            }
        };

        return UserTypeFactory;

    })

    // ============================================
    // factory for handling accesslevels
    // inject $window to store accesslevel client-side
    // ============================================
    .factory('AuthAccessLevel', function($window) {

        var authAccessFactory = {};

        // get the accesslevel out of local storage
        authAccessFactory.getAccessLevel = function() {
            return $window.localStorage.getItem('accesslevel');
        };

        // function set the accesslevel or clear the accesslevel
        // if a accesslevel is passed, set the accesslevel
        // if there is no accesslevel, clear it from local storage
        authAccessFactory.setAccessLevel = function(accesslevel) {
            if (accesslevel) {
                $window.localStorage.setItem('accesslevel', accesslevel);
            } else {
                $window.localStorage.removeItem('accesslevel');
            }
        };

        return authAccessFactory;

    })

    // ============================================
    // factory for handling email
    // inject $window to store email client-side
    // ============================================
    .factory('AuthEmail', function($window) {

        var EmailFactory = {};

        // get the email out of local storage
        EmailFactory.getEmail = function() {
            return $window.localStorage.getItem('email');
        };

        // function set the email or clear the email
        // if a email is passed, set the email
        // if there is no email, clear it from local storage
        EmailFactory.setEmail = function(email) {
            if (email) {
                $window.localStorage.setItem('email', email);
            } else {
                $window.localStorage.removeItem('email');
            }
        };

        return EmailFactory;

    })

    // ============================================
    // application configuration to integrate Token into requests
    // ============================================
    .factory('AuthInterceptor', function(AuthEmail, $q, $rootScope, $location, AuthToken, AuthUserId) {

        var interceptorFactory = {};

        // this will happen on all HTTP requests
        interceptorFactory.request = function(config) {

            // grab the Token
            var Token = AuthToken.getToken();

            // if the Token exists, add it to the header as x-access-token
            if (Token) {
                config.headers['x-access-token'] = Token;
            }

            return config;
        };

        // happens on response errors
        interceptorFactory.responseError = function(response) {
            // if our server returns a 403 forbidden response
            if (response.status == 403) {
                AuthToken.setToken();
                $location.path('/login');
            } else if (response.status === -1) {
                AuthToken.setToken();
                $location.url('/login');
            } else {
                return $q.reject({
                    message: 'Could not process this request.'
                });
                return response;
            }
        };

        return interceptorFactory;

    });
