angular.module('app.routes', ['ngRoute'])
    .config(function(
        $stateProvider,
        $urlRouterProvider,
        $locationProvider) {

        var headerContent = {
            controller: 'navbarController',
            controllerAs: 'nav',
            templateUrl: 'app/views/includes/header_include.html'
        };
        var adminheadercontent = {
            controller: 'navbarController',
            controllerAs: 'nav',
            templateUrl: 'app/views/includes/admin_header_include.html'
        };
        var profileHeaderContent = {
            controller: 'navbarController',
            controllerAs: 'nav',
            templateUrl: 'app/views/includes/profile_header_include.html'
        };
        var siteFooter = {
            controller: 'navbarController',
            controllerAs: 'nav',
            templateUrl: 'app/views/includes/footer-navbar-view.html'
        };

        $stateProvider
            // Home page state
            .state('home', {
                url: '/',
                data: {
                    pageTitle: 'Atlantic Urns - Wholesale Urn and Creamation Product Sales in North Carolina, South Carolina, and Virginia.',
                    metaVarDescription: 'Triangle Atlantic',
                    pageH1Title: 'Home'
                },
                views: {
                    'headercontent@': headerContent,
                    'slidercontent@': {
                        controller: 'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/includes/slider3-view.html'
                    },
                    'webcontent@': {
                        controller: 'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/welcome-view.html'
                    },
                    'sitefooter@': siteFooter
                }
            })
            .state('data-migrations', {
                url: '/data-migrations',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Main Menu'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/data-migrations-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            // About page state
            .state('about-tacc', {
                url: '/about-tacc',
                data: {
                    pageTitle: 'About Us | Triangle Atlantic',
                    metaVarDescription: 'About Us',
                    pageH1Title: 'About Us'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/about-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('catalog', {
                url: '/catalog',
                data: {
                    pageTitle: 'Our Catalog | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'Our Catalog'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/catalog-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('show-products/:category_code', {
                url: '/show-products/:category_code',
                data: {
                    pageTitle: 'Products | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'Products'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/show-products-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('show-admin-products/:category_code', {
                url: '/show-admin-products/:category_code',
                data: {
                    pageTitle: 'Products | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'Show Products'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/show-admin-products-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('all-products', {
                url: '/all-products',
                data: {
                    pageTitle: 'All Caskets | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'All Products'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/all-products-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('single-product/:product_code', {
                url: '/single-product/:product_code',
                data: {
                    pageTitle: 'Single Product | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'Single Product'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/single-product-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('single-admin-product/:product_code', {
                url: '/single-admin-product/:product_code',
                data: {
                    pageTitle: 'Single Admin Product | Triangle Atlantic',
                    metaVarDescription: 'Our Catalog',
                    pageH1Title: 'Single Product'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/single-admin-product-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('print-product-details', {
                url: '/print-product-details/:product_code',
                data: {
                    pageTitle: 'Print Product Details | Triangle Atlantic',
                    pageH1Title: 'Print Product Details'
                },
                views: {
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/print-product-details-view.html'
                    }
                  }
            })
            // General login state
            .state('login', {
                url: '/login',
                data: {
                    pageTitle: 'Login | Triangle Atlantic',
                    metaVarDescription: 'Triangle Atlantic Login',
                    pageH1Title: 'Login'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller: 'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/login-view.html'
                    },
                    'sitefooter@': siteFooter
                }
            })
            .state('contact', {
                url: '/contact',
                data: {
                    pageTitle: 'Contact TA | Triangle Atlantic',
                    metaVarDescription: 'Contact Triangle Atlantic.',
                    pageH1Title: 'Contact'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                    controller: 'mainController',
                    controllerAs: 'main',
                        templateUrl: 'app/views/contact-view.html'
                    },
                    'sitefooter@': siteFooter
                }
            })
            .state('mailing-list', {
                url: '/mailing-list',
                data: {
                    pageTitle: 'Mailing List | Triangle Atlantic',
                    metaVarDescription: 'Mailing List',
                    pageH1Title: 'Mailing List'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/mailing-list-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('search-products', {
                url: '/search-products',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Search Products'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/search-products-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('main-menu', {
                url: '/main-menu',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Main Menu'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/main-menu-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('admin-dashboard', {
                url: '/admin-dashboard',
                data: {
                    pageTitle: 'Dashboard | Triangle Atlantic',
                    metaVarDescription: 'Dashboard',
                    pageH1Title: 'Dashboard'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller: 'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/admin-dashboard-view.html'
                    },
                    'sitefooter@': siteFooter
                }
            })
            .state('price-managment', {
                url: '/price-managment',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Price Management'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/price-management-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('master-price-increase', {
                url: '/master-price-increase',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Rate / Price Increase'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/master-price-increase-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('retail-price-report', {
                url: '/retail-price-report',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Retail Price Report'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/retail-price-report-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('account-setup', {
                url: '/account-setup',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Account Setup'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/public-account-setup-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('add-item', {
                url: '/add-item',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Add Product / Item'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/add-item-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('add-cat', {
                url: '/add-cat',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Add Category'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/add-cat-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('create-sales-rep', {
                url: '/create-sales-rep',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Create Sales Rep'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/create-salesrep-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('edit-page-text', {
                url: '/edit-page-text',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Edit Page Text'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/edit-page-text-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('create-funeral-home', {
                url: '/create-funeral-home',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Create Funeral Home'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/create-funeral-home-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('edit-master-info', {
                url: '/edit-master-info',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Edit Master Info'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/edit-master-info-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('remove-category', {
                url: '/remove-category',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Remove Category'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/remove-category-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('manage-user-accounts', {
                url: '/manage-user-accounts',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Manage User Accounts'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/manage-user-accounts-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('login-reports', {
                url: '/login-reports',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Login Reports'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/login-reports-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('upload-media', {
                url: '/upload-media',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Upload Media'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/upload-media-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('change-admin-password', {
                url: '/change-admin-password',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'Change Admin Password'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/change-admin-password-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('list-user-credentials', {
                url: '/list-user-credentials',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'List User Credentials'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/list-users-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('view-login-history', {
                url: '/view-login-history/:user_id',
                data: {
                    pageTitle: 'Admin | Triangle Atlantic',
                    pageH1Title: 'View Login History'
                },
                views: {
                    'headercontent@': headerContent,
                    // 'adminheadercontent@': adminheadercontent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/view-login-history-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            })
            .state('mailing-list-emails', {
                url: '/mailing-list-emails',
                data: {
                    pageTitle: 'Mailing List Emails | Triangle Atlantic',
                    pageH1Title: 'Mailing List Emails'
                },
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        controller:   'mainController',
                        controllerAs: 'main',
                        templateUrl: 'app/views/admin/mailing-list-emails-view.html'
                    },
                    'sitefooter@': siteFooter
                  }
            });

        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    })
    .directive('updateTitle', ['$rootScope', '$timeout',
        function($rootScope, $timeout) {
            return {
                link: function(scope, element) {

                    var listener = function(event, toState) {

                        var title = 'Atlantic Urns - Wholesale Urn and Creamation Product Sales in North Carolina, South Carolina, and Virginia.';
                        if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

                        $timeout(function() {
                            element.text(title);
                        }, 0, false);
                    };

                    $rootScope.$on('$stateChangeSuccess', listener);
                }
            };
        }
    ]);
